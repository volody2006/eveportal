# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from services.models import TeamSpeakServer, ChannelTeamSpeak
from django.contrib import admin

class TeamSpeakServerAdmin(admin.ModelAdmin):
    list_display = ('name', 'server_ip', 'corporation', 'alliance' )
    # list_filter = ('is_npc', 'is_del', 'is_citizen', 'evewho', 'zkillboard')
    search_fields = ['server_ip', 'name',
                     # 'alliance__alliance_name', 'user__username',
                     # 'corporation__corporation_name',  'api__api_id',
                     # 'user__email', 'corporation__corporation_id',
                     # 'alliance__alliance_id'
                     ]
    raw_id_fields = ('created_user', 'alliance', 'corporation', 'managers')

admin.site.register(TeamSpeakServer, TeamSpeakServerAdmin)

class ChannelTeamSpeakAdmin(admin.ModelAdmin):
    list_display = ('name', 'server', 'corporation', 'parent' )
    # list_filter = ('is_npc', 'is_del', 'is_citizen', 'evewho', 'zkillboard')
    search_fields = ['server__name', 'name',
                     # 'alliance__alliance_name', 'user__username',
                     # 'corporation__corporation_name',  'api__api_id',
                     # 'user__email', 'corporation__corporation_id',
                     # 'alliance__alliance_id'
                     ]
    raw_id_fields = ('corporation', 'server',)

admin.site.register(ChannelTeamSpeak, ChannelTeamSpeakAdmin)