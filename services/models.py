# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils.translation import ugettext as _
from eveonline.models import Corporation, Alliance
from eveportal import settings


class TeamSpeakServer(models.Model):
    server_ip = models.GenericIPAddressField(default='127.0.0.1')
    name = models.CharField(max_length=254, default='')
    port = models.IntegerField(default=10011)
    serveradmin = models.CharField(max_length=254, default='serveradmin')
    password_serveradmin = models.CharField(max_length=254, default='')
    corporation = models.ForeignKey(Corporation, blank=True, null=True, on_delete=models.SET_NULL, related_name='ts_corp')
    alliance = models.ForeignKey(Alliance, blank=True, null=True, on_delete=models.SET_NULL, related_name='ts_alliance')
    managers = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='ts_managers')
    created_user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL, related_name='ts_created_user')

    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('TeamSpeak Server')
        verbose_name_plural = _('TeamSpeak Servers')


class ChannelTeamSpeak(models.Model):
    server = models.ForeignKey(TeamSpeakServer)
    name = models.CharField(max_length=245, default='')
    channel_id = models.IntegerField()

    corporation = models.ForeignKey(Corporation, blank=True, null=True, on_delete=models.SET_NULL, related_name='channel_corp')
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL, related_name='parent_channel')

    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)

    def __unicode__(self):
        return self.server + '-' + self.name

    class Meta:
        verbose_name = _('Channel TeamSpeak')
        verbose_name_plural = _('Channels TeamSpeak')
