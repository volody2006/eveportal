# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from services.teamspeak3_manager import Teamspeak3Manager
from django.contrib.auth.models import Group

# @login_required
# #@user_passes_test(service_blue_alliance_test)
# def activate_teamspeak3(request):
#     authinfo = AuthServicesInfoManager.get_auth_service_info(request.user)
#     character = EveManager.get_character_by_id(authinfo.main_char_id.character_id)
#     if check_if_user_has_permission(request.user, "alliance_member"):
#         result = Teamspeak3Manager.add_user(character.character_name, character.corporation_ticker)
#
#     elif check_if_user_has_permission(request.user, "blue_member"):
#         result = Teamspeak3Manager.add_blue_user(character.character_name, character.corporation_ticker)
#     else:
#         return HttpResponseRedirect("/dashboard")
#
#     # if its empty we failed
#     if result[0] is not "":
#         AuthServicesInfoManager.update_user_teamspeak3_info(result[0], result[1], request.user)
#         update_teamspeak3_groups(request.user)
#         return HttpResponseRedirect("/services/")
#     return HttpResponseRedirect("/dashboard")
#
#
# @login_required
# #@user_passes_test(service_blue_alliance_test)
# def deactivate_teamspeak3(request):
#     authinfo = AuthServicesInfoManager.get_auth_service_info(request.user)
#     result = Teamspeak3Manager.delete_user(authinfo.teamspeak3_uid)
#
#     remove_all_syncgroups_for_service(request.user, "teamspeak3")
#
#     # if false we failed
#     if result:
#         AuthServicesInfoManager.update_user_teamspeak3_info("", "", request.user)
#
#         return HttpResponseRedirect("/services/")
#     return HttpResponseRedirect("/")
#
#
# @login_required
# #@user_passes_test(service_blue_alliance_test)
# def reset_teamspeak3_perm(request):
#     authinfo = AuthServicesInfoManager.get_auth_service_info(request.user)
#     character = EveManager.get_character_by_id(authinfo.main_char_id.character_id)
#
#     Teamspeak3Manager.delete_user(authinfo.teamspeak3_uid)
#
#     remove_all_syncgroups_for_service(request.user, "teamspeak3")
#
#     if check_if_user_has_permission(request.user, "alliance_member"):
#         result = Teamspeak3Manager.generate_new_permissionkey(authinfo.teamspeak3_uid, character.character_name,
#                                                               character.corporation_ticker)
#
#
#     elif check_if_user_has_permission(request.user, "blue_member"):
#         result = Teamspeak3Manager.generate_new_blue_permissionkey(authinfo.teamspeak3_uid, character.character_name,
#                                                                    character.corporation_ticker)
#     else:
#         return HttpResponseRedirect("/services/")
#
#     # if blank we failed
#     if result != "":
#         AuthServicesInfoManager.update_user_teamspeak3_info(result[0], result[1], request.user)
#         update_teamspeak3_groups(request.user)
#         return HttpResponseRedirect("/services/")
#     return HttpResponseRedirect("/")