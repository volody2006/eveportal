# -*- coding: utf-8 -*-

import os.path
from django.core.management.base import BaseCommand, CommandError
from eveportal.settings import BASE_DIR
from my_sitemaps.sitemaps import GenericSitemap
from django.contrib.sites.models import Site
from django.template import loader
from django.utils.encoding import smart_str
from eveonline.models import Character

# from static_sitemaps.generator import SitemapGenerator

class Command(BaseCommand):
    help = """Generates the sitemaps for the site, pass in a output directory
    """

    def handle(self, *args, **options):
        # if len(args) != 1:
        #     raise CommandError('You need to specify a output directory')
        directory = os.path.join(BASE_DIR, 'file')

        # if not os.path.isdir(directory):
        #     raise CommandError('directory %s does not exist' % directory)
        #modify to meet your needs
        sitemap = GenericSitemap({'queryset': Character.objects.order_by('pk'), 'date_field':'update' })
        current_site = Site.objects.get_current()

        index_files = []
        paginator = sitemap.paginator
        for page_num in range(1, paginator.num_pages+1):
            filename = 'sitemap_%s.xml' % page_num
            file_path = os.path.join(directory,filename)
            index_files.append("http://%s/%s" % (current_site.domain, filename))
            print "Generating sitemap %s" % file_path
            with open(file_path, 'w') as site_mapfile:
                site_mapfile.write(smart_str(loader.render_to_string('sitemap.xml', {'urlset': sitemap.get_urls(page_num)})))
        sitemap_index = os.path.join(directory,'sitemap_index.xml')
        with open(sitemap_index, 'w') as site_index:
            print "Generating sitemap_index.xml %s" % sitemap_index
            site_index.write(loader.render_to_string('sitemap_index.xml', {'sitemaps': index_files}))
