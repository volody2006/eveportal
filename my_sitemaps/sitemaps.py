# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _

from django.apps import apps as django_apps
from django.contrib.sitemaps import Sitemap
from django.core.exceptions import ImproperlyConfigured
from eveonline.models import Character


class FlatPageSitemap(Sitemap):


    def items(self):
        if not django_apps.is_installed('django.contrib.sites'):
            raise ImproperlyConfigured("FlatPageSitemap requires django.contrib.sites, which isn't installed.")
        Site = django_apps.get_model('sites.Site')
        current_site = Site.objects.get_current()
        return current_site.flatpage_set.filter(registration_required=False)


class GenericSitemap(Sitemap):
    priority = None
    changefreq = None
    i18n = True
    limit = 24000
    # queryset = Character.objects.filter(is_citizen=True, is_del=False)

    def __init__(self, info_dict, priority=None, changefreq=None, i18n = True):
        self.queryset = info_dict['queryset']
        self.date_field = info_dict.get('date_field')
        self.priority = priority
        self.changefreq = changefreq
        self.i18n = i18n

    def items(self):
        # Make sure to return a clone; we don't want premature evaluation.
        return self.queryset.filter()

    def lastmod(self, item):
        if self.date_field is not None:
            return getattr(item, self.date_field)
        return None
