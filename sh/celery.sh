#!/usr/bin/env bash

ps ww | grep 'manage.py' | grep -v grep | awk '{print $1}' | xargs kill

#django-admin.py makemessages --all --ignore=env/* --ignore=.env/* --ignore=_*
# Компиляция файлов с сообщениями
#python manage.py compilemessages

python manage.py celeryd --verbosity=2 --loglevel=INFO --concurrency=2 -B -E &
python manage.py celeryd -Q celery_haystack --verbosity=2 --loglevel=INFO --concurrency=4 -E &
#python manage.py celerybeat --verbosity=2 --loglevel=INFO &
python manage.py celerycam --loglevel=INFO --logfile=logs/celerycam.log --maxrate=10&
#ps ww | grep 'redis-server' | grep -v grep | awk '{print $1}'

#python manage.py celeryd --verbosity=2 --loglevel=INFO --logfile=logs/celeryd.log --concurrency=8 &
#python manage.py celerybeat --verbosity=2 --loglevel=INFO --logfile=logs/celeryd.log &

#
#ps ww | grep 'gunicorn' | grep -v grep | awk '{print $1}' | xargs kill
#
#$ docker run -d -v "$PWD/config":/usr/share/elasticsearch/config elasticsearch
#$ docker run -d -v "$PWD/esdata":/usr/share/elasticsearch/data elasticsearch
#
#docker run  --name elastic -d -p 127.0.0.1:9200:9200 -p 127.0.0.1:9300:9300 -v /elastic:/data dockerfile/elasticsearch /elasticsearch/bin/elasticsearch -Des.config=/data/elasticsearch.yml
#
#/opt% docker run -d -v  /opt/elastic:/usr/share/elasticsearch/data -p 9200:9200 -p 9300:9300 elasticsearch
#
#$
#
#
#docker run --name  -d elastic -p 9200:9200 -p 9300:9300 -v /home/user/esdata:/usr/share/elasticsearch/data elasticsearch
#
#root@user-desktop:/usr/share/elasticsearch/bin# ./plugin install royrusso/elasticsearch-HQ
#./.env/bin/celery --verbosity=3 --loglevel=INFO --concurrency=4 -E
#
#docker run --rm --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management


### START LOCAL ###
#postgres latest
docker run --rm -e PGDATA=/var/lib/postgresql/data/pgdata -e POSTGRES_PASSWORD=postgres -v /home/user/docker/pgdata:/var/lib/postgresql/data/pgdata -p 5432:5432 postgres

#elasticsearch
docker run --rm -p 9200:9200 -p 9300:9300 -v /home/user/docker/elasticsearch/esdata:/usr/share/elasticsearch/data -v /home/user/docker/elasticsearch/plugins:/usr/share/elasticsearch/plugins/ elasticsearch

# rabbitmq
docker run --rm --hostname my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management

#redis
docker run --rm -p 6379:6379 redis

