# -*- coding: utf-8 -*-
# from datetime import datetime, time, timedelta
#
# from django.conf import settings
# from celery.task import periodic_task
# from django.contrib.auth.models import User
# from eveonline.models import EveCorporationInfo, EveApiKeyPair
# from history.managers import history_corp_alliance
#
# from models import SyncGroupCache
# from celery.schedules import crontab
# from services.managers.openfire_manager import OpenfireManager
# from services.managers.mumble_manager import MumbleManager
# from services.managers.phpbb3_manager import Phpbb3Manager
# from services.managers.ipboard_manager import IPBoardManager
# from services.managers.teamspeak3_manager import Teamspeak3Manager
# from authentication.models import AuthServicesInfo
# from eveonline.managers import EveManager
# from services.managers.eve_api_manager import EveApiManager
# from util.common_task import deactivate_services
# from wallet.models import CorpDivisions
#
#
# def update_jabber_groups(user):
#     syncgroups = SyncGroupCache.objects.filter(user=user)
#     authserviceinfo = AuthServicesInfo.objects.get(user=user)
#     groups = []
#
#     for syncgroup in syncgroups:
#         groups.append(str(syncgroup.groupname))
#
#     if len(groups) == 0:
#         groups.append('empty')
#
#     print groups
#
#     OpenfireManager.update_user_groups(authserviceinfo.jabber_username, authserviceinfo.jabber_password, groups)
#
#
# def update_mumble_groups(user):
#     syncgroups = SyncGroupCache.objects.filter(user=user)
#     authserviceinfo = AuthServicesInfo.objects.get(user=user)
#     groups = []
#     for syncgroup in syncgroups:
#         groups.append(str(syncgroup.groupname))
#
#     if len(groups) == 0:
#         groups.append('empty')
#
#     MumbleManager.update_groups(authserviceinfo.mumble_username, groups)
#
#
# def update_forum_groups(user):
#     syncgroups = SyncGroupCache.objects.filter(user=user)
#     authserviceinfo = AuthServicesInfo.objects.get(user=user)
#     groups = []
#     for syncgroup in syncgroups:
#         groups.append(str(syncgroup.groupname))
#
#     if len(groups) == 0:
#         groups.append('empty')
#
#     Phpbb3Manager.update_groups(authserviceinfo.forum_username, groups)
#
#
# def update_ipboard_groups(user):
#     syncgroups = SyncGroupCache.objects.filter(user=user)
#     authserviceinfo = AuthServicesInfo.objects.get(user=user)
#     groups = []
#     for syncgroup in syncgroups:
#         groups.append(str(syncgroup.groupname))
#
#     if len(groups) == 0:
#         groups.append('empty')
#
#     IPBoardManager.update_groups(authserviceinfo.ipboard_username, groups)
#
#
# def update_teamspeak3_groups(user):
#     syncgroups = SyncGroupCache.objects.filter(user=user)
#     authserviceinfo = AuthServicesInfo.objects.get(user=user)
#     groups = []
#     for syncgroup in syncgroups:
#         groups.append(str(syncgroup.groupname))
#
#     if len(groups) == 0:
#         groups.append('empty')
#
#     Teamspeak3Manager.update_groups(authserviceinfo.teamspeak3_uid, groups)
#
#
# def create_syncgroup_for_user(user, groupname, servicename):
#     synccache = SyncGroupCache()
#     synccache.groupname = groupname
#     synccache.user = user
#     synccache.servicename = servicename
#     synccache.save()
#
#
# def remove_all_syncgroups_for_service(user, servicename):
#     syncgroups = SyncGroupCache.objects.filter(user=user)
#     for syncgroup in syncgroups:
#         if syncgroup.servicename == servicename:
#             syncgroup.delete()
#
#
# def add_to_databases(user, groups, syncgroups):
#     authserviceinfo = None
#     try:
#         authserviceinfo = AuthServicesInfo.objects.get(user=user)
#     except:
#         pass
#
#     if authserviceinfo:
#         authserviceinfo = AuthServicesInfo.objects.get(user=user)
#
#         for group in groups:
#
#             if authserviceinfo.jabber_username and authserviceinfo.jabber_username != "":
#                 if syncgroups.filter(groupname=group.name).filter(servicename="openfire").exists() is not True:
#                     create_syncgroup_for_user(user, group.name, "openfire")
#                     update_jabber_groups(user)
#             if authserviceinfo.mumble_username and authserviceinfo.mumble_username != "":
#                 if syncgroups.filter(groupname=group.name).filter(servicename="mumble").exists() is not True:
#                     create_syncgroup_for_user(user, group.name, "mumble")
#                     update_mumble_groups(user)
#             if authserviceinfo.forum_username and authserviceinfo.forum_username != "":
#                 if syncgroups.filter(groupname=group.name).filter(servicename="phpbb3").exists() is not True:
#                     create_syncgroup_for_user(user, group.name, "phpbb3")
#                     update_forum_groups(user)
#             if authserviceinfo.ipboard_username and authserviceinfo.ipboard_username != "":
#                 if syncgroups.filter(groupname=group.name).filter(servicename="ipboard").exists() is not True:
#                     create_syncgroup_for_user(user, group.name, "ipboard")
#                     update_ipboard_groups(user)
#             if authserviceinfo.teamspeak3_uid and authserviceinfo.teamspeak3_uid != "":
#                 if syncgroups.filter(groupname=group.name).filter(servicename="teamspeak3").exists() is not True:
#                     create_syncgroup_for_user(user, group.name, "teamspeak3")
#                     update_teamspeak3_groups(user)
#
#
# def remove_from_databases(user, groups, syncgroups):
#     authserviceinfo = None
#     try:
#         authserviceinfo = AuthServicesInfo.objects.get(user=user)
#     except:
#         pass
#
#     if authserviceinfo:
#         update = False
#         for syncgroup in syncgroups:
#             group = groups.filter(name=syncgroup.groupname)
#
#             if not group:
#                 syncgroup.delete()
#                 update = True
#
#         if update:
#             if authserviceinfo.jabber_username and authserviceinfo.jabber_username != "":
#                 update_jabber_groups(user)
#             if authserviceinfo.mumble_username and authserviceinfo.mumble_username != "":
#                 update_mumble_groups(user)
#             if authserviceinfo.forum_username and authserviceinfo.forum_username != "":
#                 update_forum_groups(user)
#             if authserviceinfo.ipboard_username and authserviceinfo.ipboard_username != "":
#                 update_ipboard_groups(user)
#             if authserviceinfo.teamspeak3_uid and authserviceinfo.teamspeak3_uid != "":
#                 update_teamspeak3_groups(user)
#
#
# # Run every minute
# #@periodic_task(run_every=crontab(minute="*/1"))
# def run_databaseUpdate():
#     users = User.objects.all()
#     for user in users:
#         groups = user.groups.all()
#         syncgroups = SyncGroupCache.objects.filter(user=user)
#         add_to_databases(user, groups, syncgroups)
#         remove_from_databases(user, groups, syncgroups)
#       #  obj, create = AuthServicesInfo.objects.get_or_create(user = user)
#
