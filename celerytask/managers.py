# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.utils.translation import ugettext as _
from django.core.cache import caches
from eveportal import settings


CACHE = caches['redis-cache']

logger = logging.getLogger(__name__)

def run_task(key, delta):
    if delta is None:
        delta = 60*60 # Если время не указано, кешируем на час
    if delta > 1:
        delta = delta -1 # Кеш на 1 секунду меньше.
    if delta > 24*60*60:
        delta = 24*60*60-1 # Не даем кешировать больше суток
    if CACHE.get(key):
        logger.info('Ключ: %s закончится через: %s секунд', key, CACHE.ttl(key))
        if settings.DEBUG:
            return False
        # Ключ есть, возвращаем истину
        return True
    else:
        logger.info('Установили ключ: %s на %s секунд', key, delta)
        CACHE.set(key, key, delta)
        return False