# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evemail', '0009_auto_20160119_1456'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailmessages',
            name='content',
            field=models.TextField(null=True, blank=True),
        ),
    ]
