# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-30 21:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evemail', '0004_mailmessages_to_mailing_lists'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailinglists',
            name='character',
            field=models.ManyToManyField(blank=True, to='eveonline.Character'),
        ),
    ]
