# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-26 22:48
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0002_auto_20151224_1905'),
    ]

    operations = [
        migrations.CreateModel(
            name='MailingLists',
            fields=[
                ('list_id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=254)),
                ('character', models.ManyToManyField(to='eveonline.Character')),
            ],
        ),
        migrations.CreateModel(
            name='MailMessages',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message_id', models.BigIntegerField(blank=True, null=True)),
                ('sender_id', models.IntegerField(blank=True, null=True)),
                ('sent_date', models.DateTimeField(blank=True, null=True)),
                ('title', models.TextField(blank=True, null=True)),
                ('to_org_id', models.CharField(blank=True, max_length=254, null=True)),
                ('to_char_ids', models.CharField(blank=True, max_length=254, null=True)),
                ('to_list_ids', models.CharField(blank=True, max_length=254, null=True)),
                ('content', models.TextField(default=b'')),
                ('mailbodies', models.BooleanField(default=False)),
                ('is_read', models.BooleanField(default=False)),
                ('is_delete', models.BooleanField(default=False)),
                ('is_reply', models.BooleanField(default=False)),
                ('is_forward', models.BooleanField(default=False)),
                ('character', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='eveonline.Character')),
                ('to_char_id', models.ManyToManyField(related_name='_mailmessages_to_char_id_+', to='eveonline.Character')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Notifications',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notification_id', models.IntegerField()),
                ('sender_id', models.IntegerField()),
                ('sent_date', models.DateTimeField(blank=True, null=True)),
                ('read', models.BooleanField(default=False)),
                ('content', models.TextField(default=b'')),
                ('received', models.BooleanField(default=False)),
                ('character', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='eveonline.Character')),
            ],
        ),
        migrations.CreateModel(
            name='NotificationTypes',
            fields=[
                ('type_id', models.IntegerField(primary_key=True, serialize=False)),
                ('description', models.CharField(blank=True, max_length=254, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='notifications',
            name='type_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='evemail.NotificationTypes'),
        ),
        migrations.AddField(
            model_name='notifications',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
