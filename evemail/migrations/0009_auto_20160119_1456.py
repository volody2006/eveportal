# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evemail', '0008_auto_20160112_0603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailmessages',
            name='to_char_ids',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='mailmessages',
            name='to_list_ids',
            field=models.TextField(null=True, blank=True),
        ),
    ]
