# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('evemail', '0007_auto_20151230_2314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailmessages',
            name='to_char_id',
            field=models.ManyToManyField(related_name='+', to='eveonline.Character', blank=True),
        ),
    ]
