# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-30 20:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evemail', '0003_auto_20151230_1756'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailmessages',
            name='to_mailing_lists',
            field=models.ManyToManyField(blank=True, to='evemail.MailingLists'),
        ),
    ]
