# -*- coding: utf-8 -*-

from evemail.models import MailMessages, MailingLists, Notifications
from django.contrib import admin


class MailMessagesAdmin(admin.ModelAdmin):
    list_display = ('pk', 'message_id', 'sender_character',
                    'title', 'to_org_id', 'character',
                    'to_list_ids' )
    list_filter = ('is_read', 'is_delete', 'is_reply')
    raw_id_fields = ('to_char_id', 'user', 'character',
                     'sender_character', 'to_mailing_lists')

admin.site.register(MailMessages, MailMessagesAdmin)

class MailingListsAdmin(admin.ModelAdmin):
    list_display = ('list_id', 'name')
    # list_filter = ('is_read', 'is_delete', 'is_reply')
    raw_id_fields = ('characters',)

admin.site.register(MailingLists, MailingListsAdmin)

class NotificationsAdmin(admin.ModelAdmin):
    list_display = ('notification_id', 'type_id')
    # list_filter = ('is_read', 'is_delete', 'is_reply')
    raw_id_fields = ('character', 'user')

admin.site.register(Notifications, NotificationsAdmin)

