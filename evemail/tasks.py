# -*- coding: utf-8 -*-
import logging
from datetime import timedelta
from celery.task import periodic_task
from celerytask.managers import run_task
from django.contrib.auth.models import User
from evemail.managers import load_mail_messages, load_mail_messages_headers, load_notifications, load_notification_texts, \
    load_mailing_lists
from eveonline.models import  ApiKeyPair, Character
from eveportal import settings

logger = logging.getLogger(__name__)

@periodic_task(run_every = timedelta(minutes = 32))
def load_mail():
    name = ('%s:%s' % (__name__, 'load_mail'))
    delta = 32*60
    if not run_task(key=name, delta=delta):
        logger.info(u'Проверяем почту')
        chars = Character.objects.exclude(api=None)
        for char in chars:
            macka = 2048
            api = char.api_char_access(macka)
            if api:
                logger.info(u'Есть апи для загрузки заголовков писем')
                message_ids = load_mail_messages_headers(char.character_id, api.api_id, api.api_key)
                macka = 512
                api = char.api_char_access(macka)
                if message_ids and api:
                    logger.info(u'Есть апи для загрузки тел писем')
                    load_mail_messages(char.character_id, api.api_id, api.api_key, message_ids)


@periodic_task(run_every = timedelta(minutes = 32))
def load_notification():
    name = ('%s:%s' % (__name__, 'load_notification'))
    delta = 32*60
    if not run_task(key=name, delta=delta):
        chars = Character.objects.exclude(api=None)
        for char in chars:
            apis = ApiKeyPair.objects.filter(character = char)
            for api in apis:
                notification_ids = load_notifications(char.character_id, api.api_id, api.api_key)
                if notification_ids != False:
                    load_notification_texts(char.character_id, api.api_id, api.api_key, notification_ids)



@periodic_task(run_every = timedelta(hours = 7))
def run_load_mailing_lists():
    name = ('%s:%s' % (__name__, 'run_load_mailing_lists'))
    delta = 7*60*60
    if not run_task(key=name, delta=delta):
        logger.info(u'Загружаем списки рассылок')
        chars = Character.objects.exclude(api=None)
        for char in chars:
            macka = 1024
            api = char.api_char_access(macka)
            if api:
                logger.info(u'Есть апи для загрузки списков рассылки %s' % char)
                load_mailing_lists(char.character_id, api.api_id, api.api_key)