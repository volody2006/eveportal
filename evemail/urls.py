# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from evemail import views

urlpatterns = [
    #url(r'^/', 'evemail.views.evemail_test', name='evemail_test'),
    url(r'^$', views.evemail_all, name='evemail_all'),
                      # url(r'^/all/$', 'evemail.views.evemail_all', name='evemail_all'),
    url(r'^read/(?P<character_id>\d+)/(?P<id>\d+)/$', views.evemail_read, name='evemail_read'),
    url(r'^update/$', views.evemail_update, name='evemail_update')
                       # url('^write/$', 'evemail.views.evemail_write', name='evemail_write'),
                       # url('^write/(?P<recipient_id>\d+)/$', view='write', name='pm_write_user'),




]