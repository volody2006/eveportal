# -*- coding: utf-8 -*-
from django.utils.encoding import smart_unicode
from eveonline.models import Character
from django.db import models
from eveportal import settings


class MailingLists(models.Model):
    list_id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=254, blank=True, null=True)
    characters = models.ManyToManyField(Character, blank=True,)



class MailMessages(models.Model):
    message_id = models.BigIntegerField(blank=True, null=True)
    sender_character = models.ForeignKey(Character, related_name = 'sender_mail',
                               blank=True, null=True, on_delete=models.SET_NULL)
    sender_id = models.IntegerField(blank=True, null=True)
    sent_date = models.DateTimeField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    to_org_id = models.CharField(max_length=254, blank=True, null=True)
    to_char_ids = models.TextField(blank=True, null=True)
    to_list_ids = models.TextField(blank=True, null=True)
    to_mailing_lists = models.ManyToManyField(MailingLists, blank=True)
    content = models.TextField(blank=True, null=True)
    mailbodies = models.BooleanField(default=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    character = models.ForeignKey(Character)
    # На все ева письма по умолчанию, для остальных после прочтения.
    is_read = models.BooleanField(default=False)
    #ставим флаг, если юзер не хочет видеть это письмо.
    is_delete = models.BooleanField(default=False)
     # Какому юзеру ответили
    is_reply = models.BooleanField(default=False)
    # Какому юзеру переслали
    is_forward = models.BooleanField(default=False)
    to_char_id = models.ManyToManyField(Character, related_name='+', blank=True)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    class Meta:
        unique_together = (('message_id', 'user', 'character'),)

class NotificationTypes(models.Model):
    type_id = models.IntegerField(primary_key=True)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.description

class Notifications(models.Model):
    notification_id = models.IntegerField()
    type_id = models.ForeignKey(NotificationTypes)
    sender_id = models.IntegerField()
    sent_date = models.DateTimeField(blank=True, null=True)
    read = models.BooleanField(default=False)
    content = models.TextField(default='')
    received = models.BooleanField(default=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    character = models.ForeignKey(Character)

    class Meta:
        unique_together = (('notification_id', 'character'),)

# class MailMessageTest(models.Model):
#     message_id = models.BigIntegerField(blank=True, null=True)
#     sender_id = models.IntegerField(blank=True, null=True)
#     sent_date = models.DateTimeField(blank=True, null=True)
#     title = models.TextField(blank=True, null=True)
#     to_org_id = models.CharField(max_length=254, blank=True, null=True)
#     to_char_ids = models.CharField(max_length=254, blank=True, null=True)
#     to_list_ids = models.CharField(max_length=254, blank=True, null=True)
#     content = models.TextField(default='')
#     mailbodies = models.BooleanField(default=False)
#     user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
#     character = models.ForeignKey(Character)
#     На все ева письма по умолчанию, для остальных после прочтения.
    # is_read = models.BooleanField(default=False)
    # ставим флаг, если юзер не хочет видеть это письмо.
    # is_delete = models.BooleanField(default=False)
    #  Какому юзеру ответили
    # is_reply = models.BooleanField(default=False)
    # Какому юзеру переслали
    # is_forward = models.BooleanField(default=False)
    # content_type = models.ForeignKey(ContentType)
    # object_id = models.PositiveIntegerField()
    # to_content_object = GenericForeignKey('content_type', 'object_id')
#