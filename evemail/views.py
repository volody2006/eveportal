# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from evemail.managers import load_mail_messages_headers, load_mail_messages
from evemail.models import MailMessages
from django.http import request
from eveonline.models import Character


logger = logging.getLogger(__name__)


ALLOWED_TAGS = [
    'a',
    'abbr',
    'acronym',
    'b',
    'blockquote',
    'code',
    'em',
    'i',
    'li',
    'ol',
    'strong',
    'ul',
    'br'
]

ALLOWED_ATTRIBUTES = {
    'a': ['href', 'title'],
    'abbr': ['title'],
    'acronym': ['title'],
}

ALLOWED_STYLES = []


@login_required
def evemail_all(request):
    render_items ={}
    list = request.GET.get('list')
    full_list = [25, 100, 500]
    if list is None: list = 50
    messages = MailMessages.objects.filter(user=request.user,
                                           is_delete = False).order_by('-message_id')
    paginator = Paginator(messages, list)
    page = request.GET.get('page')
    try:
        messages = paginator.page(page)
    except PageNotAnInteger:
        messages = paginator.page(1)
    except EmptyPage:
        messages = paginator.page(paginator.num_pages)

    render_items= {
        'messages': messages,
    }



    return render_to_response('dashboard/evemail/mailbox.html', render_items, context_instance=RequestContext(request))


@login_required
def evemail_read(request, character_id, id):
    import bleach
    message = MailMessages.objects.get(user=request.user,
                                       pk=id,
                                       character=Character.objects.get(character_id=character_id)
                                       )
    message.is_read = True
    message.save()
    attrs = []
    tags = ['p', ]
    styles = ['font-weight', ]
    protocols=['http', 'https', 'mailto', 'showinfo', 'fitting']
    cleaned_text = bleach.clean(message.content, tags=ALLOWED_TAGS,
                                attributes=ALLOWED_ATTRIBUTES, styles=styles, strip=True,
                                protocols=protocols
                                )


    render_items= {
        'message': message,
        'content' : cleaned_text,
        'lists' : message.to_mailing_lists.all()
    }
  #  print message.to_mailing_lists.all()
    return render_to_response('dashboard/evemail/read-mail.html', render_items, context_instance=RequestContext(request))

# clean(text, tags=ALLOWED_TAGS, attributes=ALLOWED_ATTRIBUTES,
#           styles=ALLOWED_STYLES, strip=False, strip_comments=True)

@login_required
def evemail_update(request):
    chars = Character.objects.filter(user=request.user).exclude(api=None)
    for char in chars:
        macka = 2048
        api = char.api_char_access(macka)
        if api:
            logger.info(u'Есть апи для загрузки заголовков писем')
            message_ids = load_mail_messages_headers(char.character_id, api.api_id, api.api_key)
            macka = 512
            api = char.api_char_access(macka)
            if message_ids and api:
                logger.info(u'Есть апи для загрузки тел писем')
                load_mail_messages(char.character_id, api.api_id, api.api_key, message_ids)

    return HttpResponseRedirect(reverse("evemail_all"))