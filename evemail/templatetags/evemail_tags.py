# -*- coding: utf-8 -*-
from django.template import Library
from evemail.models import Notifications, MailMessages


register = Library()

@register.simple_tag(takes_context=True)
def notifications(context, *args, **kwargs):
    user= context['request'].user
#    for i in Notifications.objects.filter(user = user, read=False):
 #       print i.notification_id, i.sender_id
    return Notifications.objects.filter(user = user, read=False).count()


@register.simple_tag(takes_context=True)
def evemailcount(context, *args, **kwargs):
    try:
        user= context['request'].user
        return MailMessages.objects.filter(user = user, is_read=False).count()
    except:
        return None

@register.assignment_tag(takes_context=True)
def evemail_lists(context, *args, **kwargs):
    try:
        user= context['request'].user
        mail = MailMessages.objects.filter(user = user, is_read=False).order_by('-sent_date')[:15]
#    for i in Notifications.objects.filter(user = user, read=False):
 #       print i.notification_id, i.sender_id
        if mail.count() > 0:
            return mail
        return None
    except:
        return None

