# -*- coding: utf-8 -*-
import bleach
from django.template import Library
from evemail.models import Notifications, MailMessages
from eveonline.managers import EveManager
from eveonline.models import Character
from eveportal import settings

register = Library()

@register.simple_tag(takes_context=True)
def change_lang(context, lang=None, *args, **kwargs):
    """
    Get active page's url by a specified language
    Usage: {% change_lang 'en' %}
    """
    path = context['request'].path

    return '/{lang}/{path}'.format(lang=lang or settings.LANGUAGE_CODE,
                                   path=path[4:])



@register.simple_tag(takes_context=True)
def character(context, character_id, *args, **kwargs):

    try:
        return Character.objects.get(character_id=character_id)
    except:
        try:
            e = EveManager()
            character = e.update_or_create_character(character_id)
            return character
        except:
            return None




@register.simple_tag(takes_context=True)
def notifications(context, *args, **kwargs):
    user= context['request'].user
#    for i in Notifications.objects.filter(user = user, read=False):
 #       print i.notification_id, i.sender_id
    return Notifications.objects.filter(user = user, read=False).count()


@register.simple_tag(takes_context=True)
def evemail_not_read(context, *args, **kwargs):
    user= context['request'].user
#    for i in Notifications.objects.filter(user = user, read=False):
 #       print i.notification_id, i.sender_id
    return MailMessages.objects.filter(user = user, is_read=False).count()

@register.simple_tag(takes_context=True)
def evemail_lists(context, *args, **kwargs):
    user= context['request'].user
    mail = MailMessages.objects.filter(user = user, is_read=False).order_by('-sent_date')[:15]
#    for i in Notifications.objects.filter(user = user, read=False):
 #       print i.notification_id, i.sender_id
    return mail

ALLOWED_TAGS = [
    'a',
    'abbr',
    'acronym',
    'b',
    'blockquote',
    'code',
    'em',
    'i',
    'li',
    'ol',
    'strong',
    'ul',
    'br'
]

ALLOWED_ATTRIBUTES = {
    'a': ['href', 'title'],
    'abbr': ['title'],
    'acronym': ['title'],
}

ALLOWED_STYLES = ['font-weight',]

PROTOCOLS = ['http', 'https', 'mailto', 'showinfo', 'fitting']

@register.filter
def cleaned_text(value):

    try:
        # bleach.linkify(value)
        return bleach.clean(value,
                            tags=ALLOWED_TAGS,
                            attributes=ALLOWED_ATTRIBUTES,
                            styles=ALLOWED_STYLES,
                            strip=True,
                            protocols=PROTOCOLS)

    except:
        return value






# def index(request):
#     latest_question_list = Question.objects.order_by('-sent_date')[:5]
#     output = ', '.join([p.question_text for p in latest_question_list])
#     return HttpResponse(output)