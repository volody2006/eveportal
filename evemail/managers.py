# -*- coding: utf-8 -*-
import logging
from datetime import datetime

from cache import DJCache
from django.core.exceptions import MultipleObjectsReturned
from django.utils.timezone import make_aware
import evelink
from evelink.api import APICache
from evemail.models import MailingLists, MailMessages, NotificationTypes, Notifications
from evelink.char import Char
from eveonline.managers import EveManager
from eveonline.models import Character
from django.utils.translation import ugettext as _


logger = logging.getLogger(__name__)

def load_mailing_lists(character_id, api_id, api_key):
    character = Character.objects.get(character_id=character_id)
    api = (api_id, api_key)
    api = evelink.api.API(api_key=api)
    char_lists = Char(char_id = character_id, api=api)
    mailing_lists = char_lists.mailing_lists().result
    for mailing_list in mailing_lists:
        obj , creade = MailingLists.objects.get_or_create(
            list_id = mailing_list,
            defaults=dict(
                name = mailing_lists[mailing_list]
            )
        )
        obj.characters.add(character)


def load_mail_messages_headers(character_id, api_id, api_key):

    evemanager = EveManager()
    api = (api_id, api_key)
    api = evelink.api.API(api_key=api, cache=APICache())
    char = Char(char_id = character_id, api=api)
    try:
        messages = char.messages()
        logger.debug(messages)
    except:
        return False
    character = Character.objects.get(character_id = character_id)
    logger.info(u'Загружаем заголовки : %s' % character)
    user = character.user
    s = []
    for message in messages.result:

        if not Character.objects.filter(character_id=message['sender_id']).exists():
            evemanager.update_or_create_character(message['sender_id'])
        sender_character = Character.objects.get(character_id=message['sender_id'])

       # content_object = EveManager.char_or_corp_or_alliance_id(message['to']['org_id'])
        rsn = message['title']
        try:
            obj , creade = MailMessages.objects.get_or_create(
                message_id = message['id'],
                character = character,
                defaults=dict(
                    sender_character = sender_character,
                    sender_id = message['sender_id'],
                    sent_date =  make_aware(datetime.utcfromtimestamp(message['timestamp'])),
                    title = message['title'],
                    to_org_id = message['to']['org_id'],
                    to_char_ids = message['to']['char_ids'],
                    to_list_ids = message['to']['list_ids'],
                    user = user,
                )
            )
        except MultipleObjectsReturned:
            mail = MailMessages.objects.filter(message_id = message['id'], character = character)
            mail.delete()
            obj , creade = MailMessages.objects.get_or_create(
                message_id = message['id'],
                character = character,
                defaults=dict(
                    sender_character = sender_character,
                    sender_id = message['sender_id'],
                    sent_date =  make_aware(datetime.utcfromtimestamp(message['timestamp'])),
                    title = message['title'],
                    to_org_id = message['to']['org_id'],
                    to_char_ids = message['to']['char_ids'],
                    to_list_ids = message['to']['list_ids'],
                    user = user,
                )
            )
        if message['to']['char_ids'] is not None:
            for to_char_id in message['to']['char_ids']:
                obj.to_char_id.add(EveManager.get_char(to_char_id))

        if message['to']['list_ids'] is not None:
            for to_list_id in message['to']['list_ids']:
                obj.to_mailing_lists.add(EveManager.get_mail_list(to_list_id))

        s.append(message['id'])
    logger.debug(s)
    return s

#
# def char_mail():
#     all_mail = MailMessages.objects.all()
#     char_list = [x.sender_id for x in all_mail]
#     char_list = set(char_list)
#     char_list = [x for x in char_list]
#     print char_list
#     for char in char_list:
#        # print type(char)
#         print EveManager.character_info(char)


def load_mail_messages(character_id, api_id, api_key, message_ids):
    if message_ids == False:
        return False

    api = (api_id, api_key)
    api = evelink.api.API(api_key=api, cache=DJCache(cache_name='evelink'))
    char = Char(char_id = character_id, api=api)
    try:
        messages = char.message_bodies(message_ids = message_ids).result
        logger.debug(messages)
    except:
        return False
    character = Character.objects.get(character_id = character_id)
    logger.info(u'Загружаем тела писем : %s' % character)
    user = character.user
    for message_id in messages:
        logger.debug(messages[message_id])
        obj , creade = MailMessages.objects.update_or_create(
            message_id = message_id,
            user = user,
            character = character,
            defaults=dict(
                content = messages[message_id],
                mailbodies = True,
            )
        )


def load_notifications(character_id, api_id, api_key):
    api = (api_id, api_key)
    api = evelink.api.API(api_key=api, cache=DJCache(cache_name='evelink'))
    char = Char(char_id = character_id, api=api)
    try:
        notifications = char.notifications().result
    except:
        return False
    character = Character.objects.get(character_id = character_id)
    user = character.user
    s = []
    for notification in notifications:
        type_id , creade = NotificationTypes.objects.get_or_create(
            type_id = notifications[notification]['type_id'],
        )
        if int(notifications[notification]['read']) == 1:
            read = True
        else:
            read = False
        # try:
        obj , creade = Notifications.objects.update_or_create(
            notification_id = notifications[notification]['id'],
            character = character,
            defaults=dict(
                type_id = type_id,
                sent_date =  make_aware(datetime.utcfromtimestamp(notifications[notification]['timestamp'])),
                sender_id = notifications[notification]['sender_id'],
                read = read,
                user = user,
            )
        )
        s.append(notification)
        # except MultipleObjectsReturned:
        #     notification_var = Notifications.objects.filter(
        #             notification_id = notifications[notification]['id'],
        #             character = character)
        #     notification_var.delete()
        #     obj , creade = Notifications.objects.update_or_create(
        #         notification_id = notifications[notification]['id'],
        #         character = character,
        #         defaults=dict(
        #             type_id = type_id,
        #             sent_date =  make_aware(datetime.utcfromtimestamp(notifications[notification]['timestamp'])),
        #             sender_id = notifications[notification]['sender_id'],
        #             read = read,
        #             user = user,
        #         )
        #     )
        #     s.append(notification)
    return s

def load_notification_texts(character_id, api_id, api_key, notification_ids):
    api = (api_id, api_key)
    api = evelink.api.API(api_key=api, cache=DJCache(cache_name='evelink'))
    char = Char(char_id = character_id, api=api)
    try:
        notification_texts = char.notification_texts(notification_ids = notification_ids).result
        #print notification_texts
    except:
        return False
    character = Character.objects.get(character_id = character_id)
    user = character.user
    # print notification_texts
    for notification_id in notification_texts:
        obj , creade = Notifications.objects.update_or_create(
            notification_id = notification_id,
            user = user,
            character = character,
            defaults=dict(
                content = notification_texts[notification_id],
                received = True,
            )
        )
    return notification_texts
        # """
        # >>> from evemail.managers import *
        # >>> from eveonline.models import *
        # >>> char = Character.objects.get(character_id = 93840640)
        # >>> character_id = char.character_id
        # >>> api_id = '3110420'
        # >>> api_key = 'c4i4xUliSBS2pGcXWGNDXJJoaK3Ppqz90AJTNAM1ASIaghtVD29J5wRL5dYRNeTR'
        # """










notification_type = {
        1: _('Legacy'),
        2: 'Character deleted',
        3: 'Give medal to character',
        4: 'Alliance maintenance bill',
        5: 'Alliance war declared',
        6: 'Alliance war surrender',
        7: 'Alliance war retracted',
        8: 'Alliance war invalidated by Concord',
        9: 'Bill issued to a character',
        10: 'Bill issued to corporation or alliance',
        11: "Bill not paid because there's not enough ISK available",
        12: 'Bill, issued by a character, paid',
        13: 'Bill, issued by a corporation or alliance, paid',
        14: 'Bounty claimed',
        15: 'Clone activated',
        16: 'New corp member application',
        17: 'Corp application rejected',
        18: 'Corp application accepted',
        19: 'Corp tax rate changed',
        20: 'Corp news report, typically for shareholders',
        21: 'Player leaves corp',
        22: 'Corp news, new CEO',
        23: 'Corp dividend/liquidation, sent to shareholders',
        24: 'Corp dividend payout, sent to shareholders',
        25: 'Corp vote created',
        26: 'Corp CEO votes revoked during voting',
        27: 'Corp declares war',
        28: 'Corp war has started',
        29: 'Corp surrenders war',
        30: 'Corp retracts war',
        31: 'Corp war invalidated by Concord',
        32: 'Container password retrieval',
        33: 'Contraband or low standings cause an attack or items being confiscated',
        34: 'First ship insurance',
        35: 'Ship destroyed, insurance payed',
        36: 'Insurance contract invalidated/runs out',
        37: 'Sovereignty claim fails (alliance)',
        38: 'Sovereignty claim fails (corporation)',
        39: 'Sovereignty bill late (alliance)',
        40: 'Sovereignty bill late (corporation)',
        41: 'Sovereignty claim lost (alliance)',
        42: 'Sovereignty claim lost (corporation)',
        43: 'Sovereignty claim acquired (alliance)',
        44: 'Sovereignty claim acquired (corporation)',
        45: 'Alliance anchoring alert',
        46: 'Alliance structure turns vulnerable',
        47: 'Alliance structure turns invulnerable',
        48: 'Sovereignty disruptor anchored',
        49: 'Structure won/lost',
        50: 'Corp office lease expiration notice',
        51: 'Clone contract revoked by station manager',
        52: 'Corp member clones moved between stations',
        53: 'Clone contract revoked by station manager',
        54: 'Insurance contract expired',
        55: 'Insurance contract issued',
        56: 'Jump clone destroyed',
        57: 'Jump clone destroyed',
        58: 'Corporation joining factional warfare',
        59: 'Corporation leaving factional warfare',
        60: 'Corporation kicked from factional warfare on startup because of too low standing to the faction',
        61: 'Character kicked from factional warfare on startup because of too low standing to the faction',
        62: 'Corporation in factional warfare warned on startup because of too low standing to the faction',
        63: 'Character in factional warfare warned on startup because of too low standing to the faction',
        64: 'Character loses factional warfare rank',
        65: 'Character gains factional warfare rank',
        66: 'Agent has moved',
        67: 'Mass transaction reversal message',
        68: 'Reimbursement message',
        69: 'Agent locates a character',
        70: 'Research mission becomes available from an agent',
        71: 'Agent mission offer expires',
        72: 'Agent mission times out',
        73: 'Agent offers a storyline mission',
        74: 'Tutorial message sent on character creation',
        75: 'Tower alert',
        76: 'Tower resource alert',
        77: 'Station aggression message',
        78: 'Station state change message',
        79: 'Station conquered message',
        80: 'Station aggression message',
        81: 'Corporation requests joining factional warfare',
        82: 'Corporation requests leaving factional warfare',
        83: 'Corporation withdrawing a request to join factional warfare',
        84: 'Corporation withdrawing a request to leave factional warfare',
        85: 'Corporation liquidation',
        86: 'Territorial Claim Unit under attack',
        87: 'Sovereignty Blockade Unit under attack',
        88: 'Infrastructure Hub under attack',
        89: 'Contact add notification',
        90: 'Contact edit notification',
        91: 'Incursion Completed',
        92: 'Corp Kicked',
        93: 'Customs office has been attacked',
        94: 'Customs office has entered reinforced',
        95: 'Customs office has been transferred',
        96: 'FW Alliance Warning',
        97: 'FW Alliance Kick',
        98: 'AllWarCorpJoined Msg',
        99: 'Ally Joined Defender',
        100: 'Ally Has Joined a War Aggressor',
        101: 'Ally Joined War Ally',
        102: 'New war system: entity is offering assistance in a war.',
        103: 'War Surrender Offer',
        104: 'War Surrender Declined',
        105: 'FacWar LP Payout Kill',
        106: 'FacWar LP Payout Event',
        107: 'FacWar LP Disqualified Eventd',
        108: 'FacWar LP Disqualified Kill'
}

notification_type1 = {
                    1: _('Old Notifications'),
                    2: _('Member Biomassed'),
                    3: _('Medal Awarded'),
                    4: _('Alliance Maintenance Bill'),
                    5: _('Alliance War Declared'),
                    6: _('Alliance War Surrender'),
                    7: _('Alliance War Retracted'),
                    8: _('Alliance War Invalidated'),
                    9: _('Pilot Billed'),
                    10: _('Organization Billed'),
                    11: _('Insufficient Funds To Pay Bill'),
                    12: _('Bill Paid By Pilot'),
                    13: _('Bill Paid By Organization'),
                    14: _('Capsuleer Bounty Payment'),
                    15: _('Unknown'),
                    16: _('New Application To Join Corporation'),
                    17: _('Your Corporate Application Rejected'),
                    18: _('Your Corporate Application Accepted'),
                    19: _('Corporation Tax Change'),
                    20: _('Corporation News'),
                    21: _('Pilot Left Corporation'),
                    22: _('New Corporation CEO'),
                    23: _('Corporate Dividend Payout'),
                    25: _('Corporate Vote Notification'),
                    26: _('CEO Roles Revoked During Vote'),
                    27: _('Corporation War Declared'),
                    28: _('Corporation War Fighting'),
                    29: _('Corporation War Surrender'),
                    30: _('Corporation War Retracted'),
                    31: _('Corporation War Invalidated'),
                    32: _('Container Password'),
                    33: _('Customs Notification'),
                    34: _('Rookie Ship Replacement'),
                    35: _('Insurance Payment'),
                    36: _('Insurance Invalidated'),
                    37: _('Alliance Sovereignty Claim Failed'),
                    38: _('Corporate Sovereignty Claim Failed'),
                    39: _('Alliance Sovereignty Bill Due'),
                    40: _('Corporate Alliance Bill Due'),
                    41: _('Alliance Sovereignty Claim Lost'),
                    42: _('Corporate Sovereignty Claim Lost'),
                    43: _('Alliance Sovereignty Claim Acquired'),
                    44: _('Corporate Sovereignty Claim Acquired'),
                    45: _('Structure Anchoring'),
                    46: _('Sovereignty Structures Vulnerable'),
                    47: _('Sovereignty Structures Invulnerable'),
                    48: _('Sovereignty Blockade Unit Active'),
                    49: _('Structure Lost'),
                    50: _('Office Lease Expiration'),
                    51: _('Clone Contract Revoked 1'),
                    52: _('Clone Moved'),
                    53: _('Clone Contract Revoked 2'),
                    54: _('Insurance Expired'),
                    55: _('Insurance Issued'),
                    56: _('Jump Clone Deleted'),
                    57: _('Jump Clone Destruction'),
                    58: _('Corporation Has Joined Faction'),
                    59: _('Corporation Has Left Faction'),
                    60: _('Corporation Expelled From Faction'),
                    61: _('Pilot Expelled From Faction'),
                    62: _('Corporation Faction Standing Warning'),
                    63: _('Pilot Faction Standing Warning'),
                    64: _('Pilot Loses Faction Rank'),
                    65: _('Pilot Gains Faction Rank'),
                    66: _('Agent Moved Notice'),
                    67: _('Transaction Reversal'),
                    68: _('Reimbursement'),
                    69: _('Pilot Located'),
                    70: _('Research Mission Available'),
                    71: _('Mission Offer Expiration'),
                    72: _('Mission Failure'),
                    73: _('Special Mission Available'),
                    74: _('Tutorial Program'),
                    75: _('Tower Under Attack Alert'),
                    76: _('Tower Resource Alert'),
                    77: _('Station Under Attack'),
                    78: _('Station Changed'),
                    79: _('Station Conquered'),
                    80: _('Station Aggression'),
                    81: _('Corporation Joining Faction'),
                    82: _('Corporation Leaving Faction'),
                    83: _('Corporation Join Faction Withdrawn'),
                    84: _('Corporation Leave Faction Withdrawn'),
                    85: _('Corporate Liquidation Settlement'),
                    86: _('Sovereignty TCU Damage'),
                    87: _('Sovereignty SBU Damage'),
                    88: _('Sovereignty IHUB Damage'),
                    89: _('Added As Contact'),
                    90: _('Contact Level Modified'),
                    91: _('Incursion Completed'),
                    92: _('Kicked From Corporation'),
                    93: _('Orbital Structure Attacked'),
                    94: _('Orbital Structure Reinforced'),
                    95: _('Structure Ownership Transferred'),
                    96: _('Alliance Faction Standing Warning'),
                    97: _('Alliance Expelled From Faction'),
                    98: _('Corporation Joined Alliance At War'),
                    99: _('Defender Ally Joins War'),
                    100: _('Aggressor Ally Joins War'),
                    101: _('Corporation Joins War As Ally'),
                    102: _('War Ally Offer Received'),
                    103: _('Surrender Offer Received'),
                    104: _('Surrender Declined'),
                    105: _('Faction Kill Event LP'),
                    106: _('Faction Strategic Event LP'),
                    107: _('Strategic Event LP Disqualification'),
                    108: _('Kill Event LP Disqualification'),
                    109: _('War Ally Agreement Cancelled'),
                    110: _('War Ally Offer Declined'),
                    111: _('Bounty On You Claimed'),
                    112: _('Bounty Placed On You'),
                    113: _('Bounty Placed On Corporation'),
                    114: _('Bounty Placed On Alliance'),
                    115: _('Kill Right Available'),
                    116: _('Kill Right Available To All'),
                    117: _('Kill Right Earned'),
                    118: _('Kill Right Used'),
                    119: _('Kill Right Unavailable'),
                    120: _('Kill Right Unavailable To All'),
                    121: _('War Declaration'),
                    122: _('Surrender Offered'),
                    123: _('Surrender Accepted'),
                    124: _('War Made Mutual'),
                    125: _('War Retracted'),
                    126: _('You Offered War Ally'),
                    127: _('You Accepted War Ally'),
                    128: _('Mercenary Invitation Accepted'),
                    129: _('Mercenary Invitation Rejected'),
                    130: _('Mercenary Application Withdrawn'),
                    131: _('Mercenary Application Accepted'),
                    132: _('Corporation District Attacked'),
                    133: _('Friendly Fire Standings Loss'),
                    134: _('ESS Pool Taken'),
                    135: _('ESS Pool Shared'),
                    136: _('Unknown'),
                    137: _('Unknown'),
                    138: _('Clone Activation'),
                    139: _('You have been invited to join a Corporation'),
                    140: _('Kill report - Victim'),
                    141: _('Kill report - Final blow'),
                    142: _('Your Corporate Application Rejected'),
                    143: _('Corp Friendly-fire Enable-timer started'),
                    144: _('Corp Friendly-fire Disable-timer started'),
                    145: _('Corp Friendly-fire Enable-timer completed'),
                    146: _('Corp Friendly-fire Disable-timer completed'),
                    147: _('Sovereignty Structure Capture Started'),
                    148: _('Sovereignty Service Enabled'),
                    149: _('Sovereignty Service Disabled'),
                    150: _('Sovereignty Service Half Captured'),
                    151: _('Unknown'),
                    152: _('IHub Bill Expiring'),
                    160: _('Sovereignty Structures Reinforced'),
                    161: _('Command Nodes Decloaking'),
                    162: _('Sovereignty Structure Destroyed'),
                    163: _('Station Entered Freeport'),
                    164: _('IHub Destroyed - Bill'),
                    165: _('Alliance Capital Changed'),
}




def load_type():
    for i in notification_type1:
        print notification_type1[i]
        obj , creade = NotificationTypes.objects.update_or_create(
            type_id = i,
            defaults=dict(
                description = notification_type1[i]
            )
        )