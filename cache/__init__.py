# -*- coding: utf-8 -*-
import logging

from django.core.cache import caches
from django.utils.translation import ugettext as _
from evelink.api import APICache

logger = logging.getLogger(__name__)
class DJCache(APICache):
    """Minimal interface for caching API requests.

    This very basic implementation simply stores values in
    memory, with no other persistence. You can subclass it
    to define a more complex/featureful/persistent cache.
    """

    def __init__(self, cache_name=None):
        if cache_name:
            self.cache = caches[cache_name]
            logger.info('select cache %s' , cache_name)
        else:
            self.cache = caches['default']

    def get(self, key):
        """Return the value referred to by 'key' if it is cached.

        key:
            a string hash key
        """
        value = self.cache.get(key)
        logger.debug('Get cache key: %s', key)
        return value

    def put(self, key, value, duration):
        if duration > 24*60*60:
            duration = 24*60*60
        self.cache.set(key, value, duration)
        logger.debug('Put cache key: %s, timeout: %s', key, duration)