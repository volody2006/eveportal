# -*- coding: utf-8 -*-
import logging
from celery.task import periodic_task, task
from datetime import timedelta
from celerytask.managers import run_task
from eveonline.managers import EveApiManager
from eveonline.models import Character, ApiKeyPair, Corporation
from eveportal import settings
from wallet.managers import user_report_bounty, wallet_transactions_add, wallet_transactions_corp, get_corp_divisions
from wallet.managers import wallet_journal_corp, wallet_journal_add
from wallet.models import CorpDivisions


logger = logging.getLogger(__name__)

def UserReport():
    logger.info('Start UserReport')
        # list = [0, 1]
        # for i in list:
        #     user_report_bounty(type_period=i)



@periodic_task(run_every = timedelta(minutes = 31))
def updating_characters_wallets():
    name = ('%s:%s' % (__name__, 'updating_characters_wallets'))
    delta = 31*60
    if not run_task(key=name, delta=delta):
        characters = Character.objects.exclude(api = None)
        for character in characters:
            logger.info('updating_characters_wallets %s %s' % (character.character_name, character.character_id))
            updating_character_wallet_api.delay(character.pk)


@task
def updating_character_wallet_api(character_id):
    character = Character.objects.get(pk=character_id)
    name = ('%s:%s:%s' % (__name__, 'updating_character_wallet_api', character.character_id))
    delta = 31*60
    if not run_task(key=name, delta=delta):
        logger.info('updating_characters_wallets %s %s' % (character.character_name, character.character_id))
        character_id = character.character_id
        if EveApiManager.check_if_api_server_online():
            fromID = wallet_transactions_add(character_id=character_id)
            while fromID != False:
                logger.info('Спускаемся ниже по транзакциям')
                fromID=wallet_transactions_add(character_id=character_id, fromID=fromID)

            fromID = wallet_journal_add(character_id=character_id)
            while fromID != False:
                logger.info('Спускаемся ниже по журналу')
                fromID=wallet_journal_add(character_id=character_id, fromID=fromID)


@periodic_task(run_every = timedelta (minutes = 31))
#Обновляем корп счета на основании корпоративного ключа
def updade_corps_wallets():
    name = ('%s:%s' % (__name__, 'updade_corps_wallets'))
    delta = 31*60
    if not run_task(key=name, delta=delta):
        apipars = ApiKeyPair.objects.exclude(corporation = None)
        for api in apipars:
            updade_corp_wallet_api.delay(api.pk)


@task
def updade_corp_wallet_api(api_id):
    api = ApiKeyPair.objects.get(pk=api_id)
    name = ('%s:%s:%s' % (__name__, 'updade_corp_wallet_api', api.api_id))
    delta = 31*60
    if not run_task(key=name, delta=delta):
        logger.info('updade_corps_wallets %s' % api.corporation)
        api = (api.api_id, api.api_key)
        accounts = (1000, 1001, 1002, 1003, 1004, 1005, 1006)
        for account in accounts:
            fromID = wallet_transactions_corp(api=api, account=account, limit=2560, fromID = None)
            while fromID != False:
                logger.info('Спускаемся ниже по транзакциям')
                fromID = wallet_transactions_corp(api=api, account=account, limit=2560, fromID = fromID)

        for account in accounts:
            fromID = wallet_journal_corp(api=api, account=account, limit=2560, fromID = None)
            while fromID != False:
                logger.info('Спускаемся ниже по журналу')
                fromID = wallet_journal_corp(api=api, account=account, limit=2560, fromID = fromID)

@periodic_task(run_every = timedelta (days = 1))
# в этой функции вытаскиваем приватную информацию по корп ключу.
def update_corp_info_api():
    name = ('%s:%s' % (__name__, 'update_corp_info_api'))
    delta = 1*24*60*60-1
    if not run_task(key=name, delta=delta):
        logger.info('update_corp_info_api')
        if EveApiManager.check_if_api_server_online():
            corp_api = ApiKeyPair.objects.exclude(corporation = None)
            for api in corp_api:
                task_get_corp_divisions.delay(api.pk)


@task
def task_get_corp_divisions(api_id):
    api = ApiKeyPair.objects.get(pk=api_id)
    name = ('%s:%s:%s' % (__name__, 'task_get_corp_divisions', api.api_id))
    delta = 1*24*60*60-1
    if not run_task(key=name, delta=delta):
        get_corp_divisions(api)
