# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import resolve, reverse
from django.http import request
from django.template.context_processors import csrf
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.utils import translation
from django.utils.translation import get_language, activate
from eveonline.managers import EveManager
import models
from wallet.managers import *
from util.managers import month_period
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from django.http import HttpResponseRedirect

# Create your views here.

def test(reguest):
    print settings.STATICFILES_DIRS


    pass
    render_items ={}
    return render_to_response('dashboard/portal/dashboard.html', render_items, context_instance=RequestContext(reguest))


def corp_wallet_jornal_view(request, account= None):
    corporation_id= 98368686 #Holding 98368686 Stalin 98292612
    corporation = Corporation.objects.get(corporation_id=corporation_id)
    full_list = [25, 100, 500]
    type = request.GET.get('type')
    list = request.GET.get('list')
    if list is None: list = 25
    if type is None:
        wall_jormal = models.CorpJournal.objects.filter(corporation=corporation).order_by('-result_id')
    else:
        wall_jormal = models.CorpJournal.objects.filter(corporation=corporation, type_id=type).order_by('-result_id')

    if account:
        wall_jormal = wall_jormal.filter(account=account)
        type_list = [x.type_id for x in models.CorpJournal.objects.filter(corporation=corporation, account=account)]
    else:
        type_list = [x.type_id for x in models.CorpJournal.objects.filter(corporation=corporation)]

#    type_list = [x.type_id for x in models.CorpJournal.objects.filter(corporation=corporation)]
    type_list = set(type_list)
    type_list = [x for x in type_list]
    type_list.sort(key= lambda x: x.refTypeName)
    paginator = Paginator(wall_jormal, list)
    page = request.GET.get('page')
    accounts = [x.account for x in models.CorpJournal.objects.filter(corporation=corporation)]
    accounts = set(accounts)
    accounts = [x for x in accounts]
    accounts.sort()

    try:
        wall_jormal = paginator.page(page)
    except PageNotAnInteger:
        wall_jormal = paginator.page(1)
    except EmptyPage:
        wall_jormal = paginator.page(paginator.num_pages)
    try:
        type = EntryType.objects.get(refTypeID=type)
    except EntryType.DoesNotExist:
        type = None



    render_items = {'corp': corporation.corporation_name,
                    'accounts' : accounts,
                    'account' : account,
                    'wallet_list': wall_jormal,
                    'types': type_list,
                    'list': list,
                    'type': type,
                    'full_list': full_list}
    return render_to_response('dashboard/wallet/walletjornalcorp.html', render_items, context_instance=RequestContext(request))


@login_required
#@permission_required('auth.corp_stats')
def stat_wallet_view(request, character_id=None ):
    if EveManager.get_characters_by_owner_id(request.user.id) == None:
        return HttpResponseRedirect(reverse('auth_api_key_management'))
        #return HttpResponseRedirect("/api_key_management/")
    try:
        type_period = int(request.POST["type_period"])
        to_date = month_period(period=type_period)[1]
        from_date = month_period(period=type_period)[0]
     #   print type_period
    except:
        to_date = None
        from_date = None
 #   print type_period

    user = request.user
    characters = EveManager.get_characters_by_owner_id(request.user.id)
    if character_id is None:
        char_name = user
        result = sum_bounty_jornal(user=user, to_date=to_date, from_date=from_date)
    else:
        char = models.Character.objects.get(character_id=character_id)
        char_name = EveManager.get_character_by_id(character_id)
        result = sum_bounty_jornal(character_id=char, user=user, to_date=to_date, from_date=from_date)
    render_items = {'character': char_name,
                    'characters': characters,
                    'summa': result,
                    }

    return render_to_response('dashboard/wallet/stat_journal.html', render_items, context_instance=RequestContext(request))


@login_required
def wallet_jornal_char_view(request, character_id=None):

    if EveManager.get_characters_by_owner_id(request.user.id) == None:
        return HttpResponseRedirect(reverse('auth_api_key_management'))
    full_list = [25, 100, 500]
    characters = EveManager.get_characters_by_owner_id(request.user.id)
    type = request.GET.get('type')
    list = request.GET.get('list')
    if list is None: list = 25
    if character_id is None:
        char_name = EveManager.get_characters_by_owner_id(request.user.id)[0]
        char_id = models.Character.objects.get(character_name=char_name)
    else:
        char_id = models.Character.objects.get(character_id=character_id)
        char_name = EveManager.get_character_by_id(character_id)
    if type is None:
        wall_jormal = models.CharacterJournal.objects.filter(character=char_id, user=request.user).order_by('-result_id')
    else:
        wall_jormal = models.CharacterJournal.objects.filter(character=char_id, user=request.user, type_id=type).order_by('-result_id')

    type_list = [x.type_id for x in models.CharacterJournal.objects.filter(character=char_id, user=request.user)]
    type_list = set(type_list)
    type_list = [x for x in type_list]
    type_list.sort(key= lambda x: x.refTypeName)
    paginator = Paginator(wall_jormal, list)
    page = request.GET.get('page')
    try:
        wall_jormal = paginator.page(page)
    except PageNotAnInteger:
        wall_jormal = paginator.page(1)
    except EmptyPage:
        wall_jormal = paginator.page(paginator.num_pages)
    try:
        type = EntryType.objects.get(refTypeID=type)
    except EntryType.DoesNotExist:
        type = None
    render_items = {'character': char_name,
                    'characters': characters,
                    'wallet_list': wall_jormal,
                    'types': type_list,
                    'list': list,
                    'type': type,
                    'full_list': full_list}
    return render_to_response('dashboard/wallet/walletjournal.html', render_items, context_instance=RequestContext(request))


@login_required
def wallet_transaction_char_view(request, char_id=None):
    if char_id is None:
        char_name = EveManager.get_characters_by_owner_id(request.user.id)[0]
        char_id = models.Character.objects.get(character_name=char_name, user=request.user).id
    else:
        char_id = char_id
        char_name = EveManager.get_character_by_id(char_id)

    wall_jormal = models.CharacterTransactions.objects.filter(character=char_id, user=request.user).order_by('-result_id')
    render_items = {'character': char_name,
                    'wallet_list': wall_jormal}


    return render_to_response('dashboard/wallet/walletjournal.html', render_items, context_instance=RequestContext(request))


@login_required
def corp_stat_bounty(request, corporation_id=None):
    try:
        type_period = int(request.POST["type_period"])
        to_date = month_period(period=type_period)[1]
        from_date = month_period(period=type_period)[0]
    except:
        to_date = None
        from_date = None
    if corporation_id is None:
        corporation_id= 98292612
    corp_list = EveManager.get_all_corporation_info()
    alliance = EveManager.get_alliance_info_by_id(alliance_id = settings.ALLIANCE_ID)
    corp_list = corp_list.filter(alliance = alliance)
    result = []
    chars = EveManager.get_all_char_corp(corp_id=corporation_id)
    for char in chars:
        char_id = char.character_id
        corporation_id = char.corporation_id
        summa = sum_bounty_character_from_corp(character_id = char_id, corporation_id = corporation_id,
                                               to_date=to_date, from_date=from_date)
        if summa is False:
            continue
        result.append([char, summa])
    render_items = {'corps': corp_list,
                    'corp' : EveManager.get_corporation_info_by_id(corp_id=corporation_id),
                    'characters': chars,
                    'summa': result,
                    }
    print render_items
    return render_to_response('stat_bounty_corp.html', render_items, context_instance=RequestContext(request))

def bounty_system_stat(reguest, system_id=None):
    pass
    render_items = {}
    return render_to_response('dashboard/wallet/bounty_system_stat.html', render_items, context_instance=RequestContext(request))




@login_required
def test1(reguest, corporation_id=None):
    try:
        type_period = int(request.POST["type_period"])
        to_date = month_period(period=type_period)[1]
        from_date = month_period(period=type_period)[0]
     #   print type_period
    except:
        to_date = None
        from_date = None
    if corporation_id is None:
        corporation_id= 98292612
    corp_list = EveManager.get_all_corporation_info()
    alliance = EveManager.get_alliance_info_by_id(alliance_id = settings.ALLIANCE_ID)
    corp_list = corp_list.filter(alliance = alliance)
    result = []

    chars = EveManager.get_all_char_corp(corp_id=corporation_id)
    for char in chars:
        char_id = char.character_id
        summa = sum_bounty_character_from_corp(character_id = char_id, to_date=to_date, from_date=from_date)
        if summa is False:
            continue
        result.append([char, summa])
    print result

    render_items = {'corps': corp_list,
                    'corp' : EveManager.get_corporation_info_by_id(corp_id=corporation_id),
                    'characters': chars,
                    'summa': result,
                    }
    return render_to_response('dashboard/wallet/stat_bounty_corp.html', render_items, context_instance=RequestContext(reguest))

def change_lang1(context, lang=None, *args, **kwargs):
    """
    Get active page's url by a specified language
    Usage: {% change_lang 'en' %}
    """

    path = context['request'].path
    print next
    url_parts = resolve( path )

    url = path
    cur_language = get_language()
    try:
        activate(lang)
        url = reverse( url_parts.view_name, kwargs=url_parts.kwargs )
    finally:
        activate(cur_language)


    return "%s" % url

def lang(request, code):

   # code = request.POST["language"]
    next = request.META.get('HTTP_REFERER', '/')
    print request.META.get
    print ('next: %s' % next)
    response = HttpResponseRedirect(next)
    if code and translation.check_for_language(code):
        if hasattr(request, 'session'):
            request.session['django_language'] = code
        else:
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, code)
        translation.activate(code)
    print translation.activate(code)
    translation.activate('en')
    print code
    return response