# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('eve_db', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0002_auto_20151224_1905'),
    ]

    operations = [
        migrations.CreateModel(
            name='CharacterJournal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('result_id', models.BigIntegerField()),
                ('result_date', models.DateTimeField()),
                ('party_1_name', models.CharField(max_length=254)),
                ('party_1_id', models.BigIntegerField()),
                ('party_1_type', models.CharField(max_length=254)),
                ('party_2_name', models.CharField(max_length=254)),
                ('party_2_id', models.BigIntegerField()),
                ('party_2_type', models.CharField(max_length=254)),
                ('arg_id', models.BigIntegerField()),
                ('arg_name', models.CharField(max_length=254)),
                ('result_amount', models.DecimalField(max_digits=28, decimal_places=2)),
                ('result_balance', models.CharField(max_length=254)),
                ('result_reason', models.CharField(max_length=254)),
                ('tax_taxer_id', models.CharField(max_length=254)),
                ('tax_amount', models.CharField(max_length=254)),
                ('character', models.ForeignKey(to='eveonline.Character')),
            ],
            options={
                'ordering': ['result_id'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CharacterTransactions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transactionID', models.BigIntegerField()),
                ('transactionDateTime', models.DateTimeField()),
                ('journalTransactionID', models.BigIntegerField()),
                ('quantity', models.BigIntegerField(default=0)),
                ('typeID', models.IntegerField(default=0)),
                ('typeName', models.CharField(max_length=254)),
                ('price', models.FloatField(default=0.0)),
                ('clientID', models.IntegerField()),
                ('clientName', models.CharField(max_length=254)),
                ('stationID', models.BigIntegerField(default=0)),
                ('stationName', models.CharField(max_length=254)),
                ('transactionType', models.CharField(max_length=254)),
                ('transactionFor', models.CharField(max_length=254)),
                ('character', models.ForeignKey(to='eveonline.Character')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['transactionID'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CorpDivisions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('account_key', models.IntegerField()),
                ('description', models.CharField(max_length=254)),
                ('corporation', models.ForeignKey(to='eveonline.Corporation')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CorpJournal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('result_id', models.BigIntegerField()),
                ('result_date', models.DateTimeField()),
                ('party_1_name', models.CharField(max_length=254)),
                ('party_1_id', models.BigIntegerField()),
                ('party_1_type', models.CharField(max_length=254)),
                ('party_2_name', models.CharField(max_length=254)),
                ('party_2_id', models.BigIntegerField()),
                ('party_2_type', models.CharField(max_length=254)),
                ('arg_id', models.BigIntegerField()),
                ('arg_name', models.CharField(max_length=254)),
                ('result_amount', models.DecimalField(max_digits=28, decimal_places=2)),
                ('result_balance', models.CharField(max_length=254)),
                ('result_reason', models.CharField(max_length=254)),
                ('tax_taxer_id', models.CharField(max_length=254)),
                ('tax_amount', models.CharField(max_length=254)),
                ('account', models.IntegerField(null=True, blank=True)),
                ('corporation', models.ForeignKey(to='eveonline.Corporation')),
                ('divisions', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='wallet.CorpDivisions', null=True)),
            ],
            options={
                'ordering': ['result_id'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CorpTransactions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transactionID', models.BigIntegerField()),
                ('transactionDateTime', models.DateTimeField()),
                ('journalTransactionID', models.BigIntegerField()),
                ('quantity', models.BigIntegerField(default=0)),
                ('typeID', models.IntegerField(default=0)),
                ('typeName', models.CharField(max_length=254)),
                ('price', models.FloatField(default=0.0)),
                ('clientID', models.IntegerField()),
                ('clientName', models.CharField(max_length=254)),
                ('stationID', models.BigIntegerField(default=0)),
                ('stationName', models.CharField(max_length=254)),
                ('transactionType', models.CharField(max_length=254)),
                ('transactionFor', models.CharField(max_length=254)),
                ('account', models.IntegerField(null=True, blank=True)),
                ('corporation', models.ForeignKey(to='eveonline.Corporation')),
                ('divisions', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='wallet.CorpDivisions', null=True)),
            ],
            options={
                'ordering': ['transactionID'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EntryType',
            fields=[
                ('refTypeID', models.PositiveIntegerField(serialize=False, primary_key=True)),
                ('refTypeName', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='SystemsToRent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.DecimalField(null=True, max_digits=28, decimal_places=2, blank=True)),
                ('payment_period', models.CharField(max_length=254)),
                ('comment', models.TextField(null=True, blank=True)),
                ('start_rent', models.DateField(null=True, blank=True)),
                ('stop_rent', models.DateField(null=True, blank=True)),
                ('holder', models.ForeignKey(to='eveonline.Alliance')),
                ('system', models.ForeignKey(to='eve_db.MapSolarSystem')),
            ],
        ),
        migrations.CreateModel(
            name='UserReportBounty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('summa', models.DecimalField(verbose_name='\u0421\u0443\u043c\u043c\u0430 \u0437\u0430\u0440\u0430\u0431\u043e\u0442\u043a\u0430 (\u0431\u0430\u0443\u043d\u0442\u0438)', max_digits=28, decimal_places=2)),
                ('date_month', models.IntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u043c\u0435\u0441\u044f\u0446\u0430, \u0434\u043b\u044f \u043a\u043e\u0442\u043e\u0440\u043e\u0433\u043e \u0441\u0447\u0438\u0442\u0430\u043b\u0438 \u0441\u0443\u043c\u043c\u0443')),
                ('date_year', models.IntegerField(verbose_name='\u0413\u043e\u0434, \u0434\u043b\u044f \u043a\u043e\u0442\u043e\u0440\u043e\u0433\u043e \u0441\u0447\u0438\u0442\u0430\u043b\u0438 \u0441\u0443\u043c\u043c\u0443')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name='Last Update Date/Time')),
                ('amount_tax', models.DecimalField(default=0, verbose_name='\u0421\u0443\u043c\u043c\u0430 \u043d\u0430\u043b\u043e\u0433\u0430', max_digits=28, decimal_places=2)),
                ('character', models.ForeignKey(to='eveonline.Character')),
                ('corp', models.ForeignKey(verbose_name='\u0415\u0441\u043b\u0438 \u0441\u0447\u0438\u0442\u0430\u043b\u0438 \u043f\u043e \u043d\u0430\u043b\u043e\u0433\u0430\u043c \u043a\u043e\u0440\u043f\u043e\u0440\u0430\u0446\u0438\u0438, \u0442\u043e \u0443\u043a\u0430\u0437\u044b\u0432\u0430\u0435\u043c.', blank=True, to='eveonline.Corporation', null=True)),
                ('solar_system', models.ForeignKey(to='wallet.SystemsToRent')),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='corpjournal',
            name='type_id',
            field=models.ForeignKey(to='wallet.EntryType'),
        ),
        migrations.AddField(
            model_name='characterjournal',
            name='type_id',
            field=models.ForeignKey(to='wallet.EntryType'),
        ),
        migrations.AddField(
            model_name='characterjournal',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
