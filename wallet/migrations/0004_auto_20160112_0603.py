# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0003_auto_20160108_2325'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userreportbounty',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created Date/Time'),
        ),
        migrations.AlterField(
            model_name='userreportbounty',
            name='update',
            field=models.DateTimeField(auto_now=True, verbose_name='Last Update Date/Time'),
        ),
    ]
