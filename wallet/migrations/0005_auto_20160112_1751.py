# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0004_auto_20160112_0603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userreportbounty',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d \u0414\u0430\u0442\u0430/\u0412\u0440\u0435\u043c\u044f'),
        ),
        migrations.AlterField(
            model_name='userreportbounty',
            name='update',
            field=models.DateTimeField(auto_now=True, verbose_name='\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u0435\u0435 \u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0435 \u0414\u0430\u0442\u0430/\u0412\u0440\u0435\u043c\u044f'),
        ),
    ]
