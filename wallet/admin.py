from django.contrib import admin
# Register your models here.

from wallet.models import SystemsToRent, UserReportBounty, CorpDivisions, CorpJournal, EntryType, CharacterJournal, \
    CharacterTransactions, CorpTransactions


class SystemsToRentAdmin(admin.ModelAdmin):
    pass

class CorpDivisionsAdmin(admin.ModelAdmin):
    # list_filter = ['corporation' ]
    # search_fields = ['description',
    #                  'corporation__corporation_name']
    list_display = ('account_key','description', 'corporation')


class UserReportBountyAdmin(admin.ModelAdmin):
    list_filter = ['solar_system', 'date_year', 'date_month' ]
    search_fields = ['solar_system__system__solar_system_name',
                     'user__username', 'character__character_name', 'corp__corporation_name']
    list_display = ('character','user', 'solar_system',
                    'summa', 'amount_tax','date_year', 'date_month' ,'corp', 'created', 'update')


class EntryTypeAdmin(admin.ModelAdmin):

    list_display = ('refTypeID','refTypeName', )

class CharacterJournalAdmin(admin.ModelAdmin):

    # search_fields = ['description',
    #                  'corporation__corporation_name']
    list_display = ('result_id','character', )

class CharacterTransactionsAdmin(admin.ModelAdmin):
    # list_filter = ['corporation' ]
    # search_fields = ['description',
    #                  'corporation__corporation_name']
    list_display = ('transactionID','character', )



class CorpJournalAdmin(admin.ModelAdmin):
    # list_filter = ['corporation' ]
    # search_fields = ['description',
    #                  'corporation__corporation_name']
    raw_id_fields = ('corporation', 'divisions',)
    list_display = ('result_id','corporation', )


class CorpTransactionsAdmin(admin.ModelAdmin):
    # list_filter = ['corporation' ]
    # search_fields = ['description',
    #                  'corporation__corporation_name']
    list_display = ('transactionID', 'corporation', )




admin.site.register(UserReportBounty, UserReportBountyAdmin)
admin.site.register(SystemsToRent, SystemsToRentAdmin)
admin.site.register(EntryType, EntryTypeAdmin)
admin.site.register(CharacterJournal, CharacterJournalAdmin)
admin.site.register(CharacterTransactions, CharacterTransactionsAdmin)
admin.site.register(CorpDivisions, CorpDivisionsAdmin)
admin.site.register(CorpJournal, CorpJournalAdmin)
admin.site.register(CorpTransactions, CorpTransactionsAdmin)
#admin.site.register(CorpJournal)