# -*- coding: utf-8 -*-
import logging

from accounts import facade
from cache import DJCache
from django.db.models import *
from django.utils import timezone
from datetime import datetime, timedelta
from django.utils.timezone import make_aware

from accounts.models import Account, Transfer
from eve_db.models import MapSolarSystem
from eveonline.eve_api_manager import EveApiManager
from eveonline.managers import EveManager
from util.managers import _start_date, end_of_day, month_period
from wallet.models import CharacterJournal, CharacterTransactions, CorpJournal, CorpTransactions, EntryType, \
    SystemsToRent, UserReportBounty, CorpDivisions
from eveonline.models import ApiKeyPair, Character, Corporation
import evelink.api  # Raw API access
import evelink.char  # Wrapped API access for the /char/ API path
import evelink.eve
import evelink.corp
from django.core.exceptions import MultipleObjectsReturned
from decimal import *
from django.conf import settings

logger = logging.getLogger(__name__)


def proxy_wallet_evebank(obj):
    # Записываем транзакции в системе если, транзакции поступления
    #  в холдинговую корпорацию и переносим деньги на личные счета персов и корпорации
    if obj.corporation.corporation_id == 98368686 and obj.account == 1000 and obj.result_amount > 0:
        if obj.type_id.refTypeID in [37, 10]:
            logger.info(u'Проверяем зачисление в банк')
            source = Account.objects.get(name='Bank')
            destination = obj.corporation.get_account()
            amount = obj.result_amount
            merchant_reference = ('cj:%s:%s:%s' % (obj.result_id, obj.account, obj.pk))
            description = obj.result_reason
            if not Transfer.objects.filter(merchant_reference=merchant_reference).exists():
                logger.info(u'Зачисления нет, делаем. source %s, destination %s, amount %s, '
                            u'merchant_reference %s, description %s', source, destination, amount, merchant_reference,
                            description)
                parent = facade.transfer(source, destination, amount,
                                         parent=None, user=None, merchant_reference=merchant_reference,
                                         description=description)
                logger.info(u'Заносим новую транзакцию на счет банка'
                            u' %s' % parent)
                if Corporation.objects.filter(pk=obj.party_1_id).exists():
                    if not Transfer.objects.filter(parent=parent).exists():
                        transfer = facade.transfer(source=parent.destination,
                                                   destination=Corporation.objects.get(
                                                           pk=obj.party_1_id).get_account(),
                                                   amount=parent.amount,
                                                   parent=parent, user=None, merchant_reference=None,
                                                   description=description)
                        logger.info(u'Корпоративная транзакция %s' % transfer)
                if Character.objects.filter(pk=obj.party_1_id).exists():
                    if not Transfer.objects.filter(parent=parent).exists():
                        transfer = facade.transfer(source=parent.destination,
                                                   destination=Character.objects.get(
                                                           pk=obj.party_1_id).get_account(),
                                                   amount=parent.amount,
                                                   parent=parent, user=None, merchant_reference=None,
                                                   description=description)
                        logger.info(u'Персональная транзакция %s' % transfer)


def sum_bounty_jornal(user=None, character_id=None, from_date=None, to_date=None):
    # Статистика по системам
    if to_date is None:
        to_date = timezone.now()
    if from_date is None:
        from_date = make_aware(datetime.utcfromtimestamp(0))
    result = []
    if character_id is None:
        obj = CharacterJournal.objects.filter(type_id=85,
                                              user=user,
                                              result_date__range=(from_date, to_date))
    else:
        obj = CharacterJournal.objects.filter(type_id=85,
                                              user=user,
                                              character=character_id,
                                              result_date__range=(from_date, to_date))
    solar_system_list = [x.arg_name for x in obj]
    solar_system_list = set(solar_system_list)
    solar_system_list = [x for x in solar_system_list]
    solar_system_list.sort(key=lambda x: x)
    #   print solar_system_list # [u'5ED-4E', u'DN58-U', u'F39H-1']
    for system in solar_system_list:
        summa = obj.filter(arg_name=system).aggregate(Sum('result_amount'))['result_amount__sum']
        entry = [system, summa]
        result.append(entry)
    # print result #[[u'5ED-4E', 918012785], [u'DN58-U', 136603252], [u'F39H-1', 167114137]]
    return result


# ------------------------------------------------------------------------------
# Заносим типы транзакций в базу. Обязательно делать при установки новой системы
def update_RefTypes():
    logger.info("fetching /eve/RefTypes.xml.aspx...")
    typesApi = evelink.eve.EVE(api=evelink.api.API(cache=DJCache(cache_name='evelink')))
    typesApi = typesApi.reference_types()
    for type in typesApi.result:
        print type, typesApi.result[type]
        obj, created = EntryType.objects.get_or_create(
                refTypeID=type,
                defaults=dict(refTypeName=typesApi.result[type], )
        )



        #   LOG.info("transaction types updated")


def get_corp_divisions(api):
    corp_info = EveApiManager.get_corporation_information(corp_id=None,
                                                          # corp_info = EveApiManager.get_corporation_information(corp_id=api.corporation.corporation_id,
                                                          api_id=api.api_id,
                                                          api_key=api.api_key)
    if not corp_info:
        return
    wallets = corp_info['wallets']
    for wall in wallets:
        logger.info('%s %s' % (wall, wallets[wall]))
        obj, created = CorpDivisions.objects.update_or_create(
                corporation=Corporation.objects.get(pk=corp_info['id']),
                account_key=wall,
                defaults=dict(description=wallets[wall],
                              )
        )


def wallet_transactions_corp(api, account, limit=2560, fromID=None):
    api_obj = ApiKeyPair.objects.get(pk=api[0])
    try:
        api = evelink.api.API(api_key=api, cache=DJCache(cache_name='evelink'))
        corp = evelink.corp.Corp(api=api)
        corp_wals = corp.wallet_transactions(limit=limit, account=account)
        corp_info = evelink.corp.Corp(api=api).corporation_sheet()
    except:
        return False
    corp_id = corp_info.result['id']
    corporation = Corporation.objects.get(corporation_id=corp_id)
    if not CorpDivisions.objects.filter(corporation=corporation, account_key=account).exists():
        logger.debug('load corp divisions')
        get_corp_divisions(api_obj)

    if fromID is None:
        try:
            fromID = corp_wals.result[0]['id']
        except IndexError:
            return False
    i = 0
    for wall in corp_wals.result:
        result_id = wall['id']
        if fromID > result_id:
            fromID = result_id
        try:
            obj, created = CorpTransactions.objects.update_or_create(
                    transactionID=result_id,
                    account=account,
                    defaults=dict(transactionDateTime=make_aware(datetime.utcfromtimestamp(wall['timestamp'])),
                                  journalTransactionID=wall['journal_id'],
                                  quantity=wall['quantity'],
                                  typeID=wall['type']['id'],
                                  typeName=wall['type']['name'],
                                  price=wall['price'],
                                  clientID=wall['client']['id'],
                                  clientName=wall['client']['name'],
                                  stationID=wall['station']['id'],
                                  stationName=wall['station']['name'],
                                  transactionType=wall['action'],
                                  transactionFor=wall['for'],
                                  divisions=CorpDivisions.objects.get(corporation=corporation, account_key=account),
                                  corporation=corporation)
            )
        except MultipleObjectsReturned:
            logger.error("Error: %s:" % result_id)
            logger.info("ERROR: Corp_id: %s. corporation_id: %s. account: %s." % (corp_id, corporation, account))
            obj = CorpTransactions.objects.filter(transactionID=result_id, account=account)
            obj.delete()
            obj, created = CorpTransactions.objects.update_or_create(
                    transactionID=result_id,
                    account=account,
                    defaults=dict(transactionDateTime=make_aware(datetime.utcfromtimestamp(wall['timestamp'])),
                                  journalTransactionID=wall['journal_id'],
                                  quantity=wall['quantity'],
                                  typeID=wall['type']['id'],
                                  typeName=wall['type']['name'],
                                  price=wall['price'],
                                  clientID=wall['client']['id'],
                                  clientName=wall['client']['name'],
                                  stationID=wall['station']['id'],
                                  stationName=wall['station']['name'],
                                  transactionType=wall['action'],
                                  transactionFor=wall['for'],
                                  divisions=CorpDivisions.objects.get(corporation=corporation, account_key=account),
                                  corporation=corporation)
            )

        i = i + 1
    if i < 2560:
        return False
    return fromID


def wallet_journal_corp(api, account, limit=2560, fromID=None):
    try:
        api = evelink.api.API(api_key=api, cache=DJCache(cache_name='evelink'))
        corp = evelink.corp.Corp(api=api)
        corp_wals = corp.wallet_journal(limit=limit, account=account, before_id=fromID)
        corp_info = evelink.corp.Corp(api=api).corporation_sheet()
    except:
        return False
    corp_id = corp_info.result['id']
    corporation = Corporation.objects.get(corporation_id=corp_id)
    if fromID is None:
        try:
            fromID = corp_wals.result[0]['id']
        except IndexError:
            return False
    i = 0
    for wall in corp_wals.result:
        result_id = wall['id']
        if fromID > result_id:
            fromID = result_id
        rsn = wall['reason'].decode('unicode-escape')
        type_id = EntryType.objects.get(refTypeID=wall['type_id'])
        ol_tupe = type_id.refTypeID
        if ol_tupe == 85:
            rsn = rsn.replace(",", ", ")
        try:
            obj, created = CorpJournal.objects.update_or_create(
                    result_id=result_id, account=account,
                    defaults=dict(result_date=make_aware(datetime.utcfromtimestamp(wall['timestamp'])),
                                  type_id=type_id,
                                  party_1_name=wall['party_1']['name'],
                                  party_1_id=wall['party_1']['id'],
                                  party_1_type=wall['party_1']['type'],
                                  party_2_name=wall['party_2']['name'],
                                  party_2_id=wall['party_2']['id'],
                                  party_2_type=wall['party_2']['type'],
                                  arg_id=wall['arg']['id'],
                                  arg_name=wall['arg']['name'],
                                  result_amount=Decimal(wall['amount']),
                                  result_balance=wall['balance'],
                                  result_reason=rsn,
                                  tax_taxer_id=wall['tax']['taxer_id'],
                                  tax_amount=wall['tax']['amount'],
                                  divisions=CorpDivisions.objects.get(corporation=corporation, account_key=account),
                                  corporation=corporation)
            )
        except MultipleObjectsReturned:
            obj = CorpJournal.objects.filter(result_id=result_id, account=account)
            obj.delete()
            obj, created = CorpJournal.objects.update_or_create(
                    result_id=result_id, account=account,
                    defaults=dict(result_date=make_aware(datetime.utcfromtimestamp(wall['timestamp'])),
                                  type_id=type_id,
                                  party_1_name=wall['party_1']['name'],
                                  party_1_id=wall['party_1']['id'],
                                  party_1_type=wall['party_1']['type'],
                                  party_2_name=wall['party_2']['name'],
                                  party_2_id=wall['party_2']['id'],
                                  party_2_type=wall['party_2']['type'],
                                  arg_id=wall['arg']['id'],
                                  arg_name=wall['arg']['name'],
                                  result_amount=Decimal(wall['amount']),
                                  result_balance=wall['balance'],
                                  result_reason=rsn,
                                  tax_taxer_id=wall['tax']['taxer_id'],
                                  tax_amount=wall['tax']['amount'],
                                  divisions=CorpDivisions.objects.get(corporation=corporation, account_key=account),
                                  corporation=corporation)
            )
        proxy_wallet_evebank(obj)
        # Записываем транзакции в системе если, транзакции поступления
        #  в холдинговую корпорацию и переносим деньги на личные счета персов и корпорации
        # if corporation.corporation_id == 98368686 and account == 1000 and obj.result_amount > 0:
        #     if ol_tupe in [37, 10]:
        #         logger.info(u'Проверяем зачисление в банк')
        #         source = Account.objects.get(name='Bank')
        #         destination = corporation.get_account()
        #         amount = obj.result_amount
        #         merchant_reference = ('cj:%s:%s:%s' % (result_id, account, obj.pk))
        #         description = obj.result_reason
        #         if not Transfer.objects.filter(merchant_reference=merchant_reference).exists():
        #             logger.info(u'Зачисления нет, делаем')
        #             parent = facade.transfer(source, destination, amount,
        #                                      parent=None, user=None, merchant_reference=merchant_reference,
        #                                      description=description)
        #             logger.info(u'Заносим новую транзакцию на счет банка'
        #                         u' %s' % parent)
        #             if Corporation.objects.filter(pk=obj.party_1_id).exists():
        #                 if not Transfer.objects.filter(parent=parent).exists():
        #                     transfer = facade.transfer(source=parent.destination,
        #                                                destination=Corporation.objects.get(
        #                                                        pk=obj.party_1_id).get_account(),
        #                                                amount=parent.amount,
        #                                                parent=parent, user=None, merchant_reference=None,
        #                                                description=description)
        #                     logger.info(u'Корпоративная транзакция %s' % transfer)
        #             if Character.objects.filter(pk=obj.party_1_id).exists():
        #                 if not Transfer.objects.filter(parent=parent).exists():
        #                     transfer = facade.transfer(source=parent.destination,
        #                                                destination=Character.objects.get(
        #                                                        pk=obj.party_1_id).get_account(),
        #                                                amount=parent.amount,
        #                                                parent=parent, user=None, merchant_reference=None,
        #                                                description=description)
        #                     logger.info(u'Персональная транзакция %s' % transfer)

        i = i + 1
    if i < 2560:
        return False
    return fromID


# Добавлям транзакции в БД, возвращает минимальное значение для хождения в журнале
def wallet_transactions_add(character_id, limit=2560, fromID=None):
    macka = 4194304
    try:
        character = Character.objects.get(character_id=character_id)
    except Character.DoesNotExist:
        try:
            evemanager = EveManager()
            character = evemanager.update_or_create_character(character_id)
        except:
            return False
    user = character.user
    api = character.api_char_access(macka)
    if api:
        api_id = api.api_id
    else:
        return False
    api_key_pair = ApiKeyPair.objects.get(api_id=api_id)
    api_key = api_key_pair.api_key
    api = evelink.api.API(api_key=(api_id, api_key), cache=DJCache(cache_name='evelink'))
    char = evelink.char.Char(character_id, api=api)
    try:
        walls = char.wallet_transactions(limit=limit, before_id=fromID)
    except:
        logger.error('ERROR: Api key bad! User: %s, char: %s' % (user, character))
        return False
    if fromID is None:
        try:
            fromID = walls.result[0]['id']
        except IndexError:
            return False
    i = 0

    # print "############################ wallet_transactions_add ######################"
    for wall in walls.result:
        result_id = wall['id']
        if fromID > result_id:
            fromID = result_id
        try:
            obj, created = CharacterTransactions.objects.update_or_create(
                    transactionID=result_id,
                    character=character,
                    defaults=dict(transactionDateTime=make_aware(datetime.utcfromtimestamp(wall['timestamp'])),
                                  journalTransactionID=wall['journal_id'],
                                  quantity=wall['quantity'],
                                  typeID=wall['type']['id'],
                                  typeName=wall['type']['name'],
                                  price=wall['price'],
                                  clientID=wall['client']['id'],
                                  clientName=wall['client']['name'],
                                  stationID=wall['station']['id'],
                                  stationName=wall['station']['name'],
                                  transactionType=wall['action'],
                                  transactionFor=wall['for'],
                                  user=user,)
            )
        except MultipleObjectsReturned:
            temp = CharacterTransactions.objects.filter(transactionID=result_id)
            temp.delete()
            obj, created = CharacterTransactions.objects.update_or_create(
                    transactionID=result_id,
                    defaults=dict(transactionDateTime=make_aware(datetime.utcfromtimestamp(wall['timestamp'])),
                                  journalTransactionID=wall['journal_id'],
                                  quantity=wall['quantity'],
                                  typeID=wall['type']['id'],
                                  typeName=wall['type']['name'],
                                  price=wall['price'],
                                  clientID=wall['client']['id'],
                                  clientName=wall['client']['name'],
                                  stationID=wall['station']['id'],
                                  stationName=wall['station']['name'],
                                  transactionType=wall['action'],
                                  transactionFor=wall['for'],
                                  user=user,
                                  character=character)
            )
        i = i + 1

    if i < 2560:
        return False
    return fromID


def wallet_journal_add(character_id, limit=2560, fromID=None):
    macka = 2097152
    try:
        character = Character.objects.get(character_id=character_id)
    except Character.DoesNotExist:
        try:
            evemanager = EveManager()
            character = evemanager.update_or_create_character(character_id)
        except:
            return False

    user = character.user
    api = character.api_char_access(macka)
    if api:
        api_id = api.api_id
    else:
        return False
    api_key_pair = ApiKeyPair.objects.get(api_id=api_id)
    api_key = api_key_pair.api_key
    # eve = evelink.eve.EVE()
    api = evelink.api.API(api_key=(api_id, api_key), cache=DJCache(cache_name='evelink'))
    char = evelink.char.Char(character_id, api=api)
    try:
        walls = char.wallet_journal(limit=limit, before_id=fromID)
    except:
        logger.error('ERROR: Api key bad! User: %s, char: %s' % (user, character))
        return False
    if fromID is None:
        try:
            fromID = walls.result[0]['id']
        except IndexError:
            #     print u'Нет транзакций!'
            return False
    i = 0

    for wall in walls.result:
        # a = {'amount': -78900.0, 'balance': 216003653.75, 'party_2': {'type': 2, 'name': 'CONCORD', 'id': 1000125}, 'type_id': 97, 'reason': u'\u042d\u043a\u0441\u043f\u043e\u0440\u0442\u043d\u0430\u044f \u043f\u043e\u0448\u043b\u0438\u043d\u0430 \u043f\u043b\u0430\u043d\u0435\u0442\u044b Algogille IV', 'timestamp': 1442688734, 'tax': {'taxer_id': 0, 'amount': 0.0}, 'party_1': {'type': 1378, 'name': 'SergA Balan', 'id': 95624580}, 'arg': {'name': 'Algogille IV', 'id': 40314840}, 'id': 11676095208}

        result_id = wall['id']
        if fromID > result_id:
            fromID = result_id
        try:
            rsn = wall['reason'].decode('unicode-escape')
        except UnicodeEncodeError:
            rsn = wall['reason'].encode('utf-8')
            # print rsn
        type_id = EntryType.objects.get(refTypeID=wall['type_id'])
        ol_tupe = type_id.refTypeID
        if ol_tupe == 85:
            rsn = rsn.replace(",", ", ")
        try:
            obj, created = CharacterJournal.objects.update_or_create(
                    result_id=result_id,
                    defaults=dict(result_date=make_aware(datetime.utcfromtimestamp(wall['timestamp'])),
                                  type_id=type_id,
                                  party_1_name=wall['party_1']['name'],
                                  party_1_id=wall['party_1']['id'],
                                  party_1_type=wall['party_1']['type'],
                                  party_2_name=wall['party_2']['name'],
                                  party_2_id=wall['party_2']['id'],
                                  party_2_type=wall['party_2']['type'],
                                  arg_id=wall['arg']['id'],
                                  arg_name=wall['arg']['name'],
                                  result_amount=wall['amount'],
                                  result_balance=wall['balance'],
                                  result_reason=rsn,
                                  tax_taxer_id=wall['tax']['taxer_id'],
                                  tax_amount=wall['tax']['amount'],
                                  user=user,
                                  character=character)
            )
        except MultipleObjectsReturned:
            temp = CharacterJournal.objects.filter(result_id=result_id)
            temp.delete()
            obj, created = CharacterJournal.objects.update_or_create(
                    result_id=result_id,
                    defaults=dict(result_date=make_aware(datetime.utcfromtimestamp(wall['timestamp'])),
                                  type_id=type_id,
                                  party_1_name=wall['party_1']['name'],
                                  party_1_id=wall['party_1']['id'],
                                  party_1_type=wall['party_1']['type'],
                                  party_2_name=wall['party_2']['name'],
                                  party_2_id=wall['party_2']['id'],
                                  party_2_type=wall['party_2']['type'],
                                  arg_id=wall['arg']['id'],
                                  arg_name=wall['arg']['name'],
                                  result_amount=wall['amount'],
                                  result_balance=wall['balance'],
                                  result_reason=rsn,
                                  tax_taxer_id=wall['tax']['taxer_id'],
                                  tax_amount=wall['tax']['amount'],
                                  user=user,
                                  character=character)
            )

        i = i + 1

    if i < 2560:
        return False
    return fromID


def filter_type_corp_jornal(corporation_id, type_id=None, account=None, from_date=None, to_date=None):
    """

    :param type_id: Тип транзакции
    :param account: Номер счета
    :param corporation: Номер корпорации
    :param from_date: Начало даты, по умолчанию с самого начала
    :param to_date: До даты, по умолчанию, до текущей
    :return: Возвражает список объектов для дальнейшего анализа
    """
    if corporation_id is None:
        logger.error('ERROR: Corp_id cannot be blank!')
        return False
    else:
        corporation = Corporation.objects.get(corporation_id=corporation_id)
    if to_date is None:
        to_date = timezone.now()
    if from_date is None:
        from_date = datetime.utcfromtimestamp(0)
    if type_id is None:
        if account is None:
            obj = CorpJournal.objects.filter(corporation=corporation, result_date__range=(from_date, to_date))
        else:
            obj = CorpJournal.objects.filter(account=account,
                                             corporation=corporation,
                                             result_date__range=(from_date, to_date))
    elif account is None:
        obj = CorpJournal.objects.filter(type_id=type_id,
                                         corporation=corporation,
                                         result_date__range=(from_date, to_date))
    else:
        obj = CorpJournal.objects.filter(type_id=type_id,
                                         account=account,
                                         corporation=corporation,
                                         result_date__range=(from_date, to_date))
        # obj_new = CorpJournal.objects.filter(corporation=corporation,result_date__range=(from_date, to_date)).filter(Q(type_id=type_id) | Q(account=account))
    # summa = obj.filter(arg_name = system).aggregate(Sum('result_amount'))['result_amount__sum']
    #    print obj_new.count()

    return obj


def sum_bounty_character_from_corp(character_id, corporation_id, from_date=None, to_date=None):
    if to_date is None:
        to_date = timezone.now()
    if from_date is None:
        from_date = make_aware(datetime.utcfromtimestamp(0))
    result = []
    corporation = Corporation.objects.get(corporation_id=corporation_id)
    if not CorpJournal.objects.filter(corporation=corporation,
                                      type_id=85, party_2_id=character_id,
                                      result_date__range=(from_date, to_date)).exists():
        print False
        return False

    obj = CorpJournal.objects.filter(type_id=85,
                                     corporation=corporation,
                                     party_2_id=character_id,
                                     result_date__range=(from_date, to_date))
    solar_system_list = [x.arg_id for x in obj]
    solar_system_list = set(solar_system_list)
    solar_system_list = [x for x in solar_system_list]

    for system_id in solar_system_list:
        summa = obj.filter(arg_id=system_id).aggregate(Sum('result_amount'))['result_amount__sum']
        system = MapSolarSystem.objects.get(solar_system_id=system_id)
        entry = [system, summa]
        result.append(entry)
    summa = obj.aggregate(Sum('result_amount'))['result_amount__sum']
    result.append(['All', summa])
    logger.info(result)
    return result


def client_account_summa_corp(corporation_id, type_id=None, account=None, from_date=None, to_date=None):
    pass


def corp_report(corporation_id, type_id=None, account=None, from_date=None, to_date=None):
    period = 30
    end = timezone.now()
    start = end - timedelta(period)
    #   Запрос записи журнала в этот период
    journal_entries = filter_type_corp_jornal(corporation_id, type_id=type_id, account=1000, from_date=start,
                                              to_date=end)
    print ('Found transaction: %s' % journal_entries.count())
    # Получить агрегированный набор положительного дохода в этот период
    positivie_entries = journal_entries.filter(result_amount__gt=0)
    # Рассчитайте общую положительную прибыль в этот период
    income_total = positivie_entries.aggregate(Sum('result_amount'))['result_amount__sum']
    # Calculate the sum for each journal entry type and order by entry type
    # Вычислить сумму для каждого типа входа журнал и порядка по типу входа
    income_entries = _group_by_wallet_entry(positivie_entries)
    income_aggregated = []
    for item in income_entries:
        # Заполнения результаты с процентом для каждой записи
        item['percentage'] = item['result_amount'] / (income_total / 100)
        income_aggregated.append(item)
    # Получить агрегированный набор из negative_entries в этот период
    negative_entries = journal_entries.filter(result_amount__lt=0)
    # Рассчитайте общие расходы в этом periodamount__gt = 0
    expenditure_total = negative_entries.aggregate(Sum('result_amount'))['result_amount__sum']
    if expenditure_total == None: expenditure_total = 0.0
    # Рассчитайте сумму для каждого типа входа журнал и порядка по типу входа
    expenditure_entries = _group_by_wallet_entry(negative_entries)
    expenditure_aggregated = []
    for item in expenditure_entries:
        item['percentage'] = item['result_amount'] / (expenditure_total / 100)
        expenditure_aggregated.append(item)
    # Получите ежедневные суммы доходов и расходов
    income_time = _get_daily_sums(period, positivie_entries)
    expenditure_time = _get_daily_sums(period, negative_entries)
    # Рассчитайте денежный поток
    cashflow = income_total + expenditure_total

    # Создать данные отчета по умолчанию
    data = {
        'income_aggregated': income_aggregated,  # aggregated
        'income_total': income_total,  # total
        'income_time': income_time,  # all entries in month_period
        'expenditure_aggregated': expenditure_aggregated,  # aggregated
        'expenditure_total': expenditure_total,  # total
        'expenditure_time': expenditure_time,  # all entries in month_period
        'cashflow': cashflow,

    }

    return data


def _group_by_wallet_entry(query_set):
    return query_set.values('type_id_id').annotate(result_amount=Sum('result_amount')).order_by('type_id_id')


def _get_daily_sums(period, entries, step=1):
    """
    Returns both income and expenditure by type for each day in the provided period.
    Period is a number of days starting from utcnow back to utcnow - period.
    Step is the number of days to cummulate income and expenditure with default to 1.
    Daily sums are calculated for each day starting at 0:0:0 and ending with 23:59:59.
    """
    start_day = _start_date(period)
    result = []
    for day in range(0, period, step):
        start = start_day + timedelta(day)
        end = end_of_day(start)
        dated_query = entries.filter(result_date__range=(start, end))
        entry = dated_query.aggregate(Sum('result_amount'))['result_amount__sum']
        result_amount = 0 if entry == None else entry
        result.append({'date': start, 'result_amount': result_amount})
    return result


# Считаем заработок от баунти в арендованных системах.
def user_report_bounty(type_period):
    # type_period - Отступ от текущего месяца, 1- предыдущий, 0- текущий. -1 - следующий
    all_char = Character.objects.filter(alliance_id=settings.ALLIANCE_ID)
    solar_system_list = SystemsToRent.objects.all()
    to_date = month_period(period=type_period)[1]
    from_date = month_period(period=type_period)[0]
    date_month = to_date.month
    date_year = to_date.year
    #  print(to_date, from_date)
    for char in all_char:
        logger.info('user_report_bounty char: %s' % char.character_name)
        for solar_system in solar_system_list:
            summa_char_jourmal = CharacterJournal.objects.filter(arg_name=solar_system.system.solar_system_name,
                                                                 character=char,
                                                                 result_date__range=(from_date, to_date)
                                                                 ).aggregate(Sum('result_amount'))['result_amount__sum']

            summa_corp_jourmal = CorpJournal.objects.filter(arg_name=solar_system.system.solar_system_name,
                                                            party_2_name=char.character_name,
                                                            result_date__range=(from_date, to_date)
                                                            ).aggregate(Sum('result_amount'))['result_amount__sum']
            corp = Corporation.objects.get(pk=char.corporation_id)

            if summa_char_jourmal is not None:
                obj, created = UserReportBounty.objects.update_or_create(
                        character=char,
                        solar_system=solar_system,
                        date_month=date_month,
                        date_year=date_year,
                        corp=None,
                        defaults=dict(summa=summa_char_jourmal,
                                      user=char.user)
                )
            if summa_corp_jourmal is not None:
                summa_char_jourmal = summa_corp_jourmal / (corp.tax / 100)
                obj, created = UserReportBounty.objects.update_or_create(
                        character=char,
                        solar_system=solar_system,
                        date_month=date_month,
                        date_year=date_year,
                        corp=corp,
                        defaults=dict(summa=summa_char_jourmal,
                                      user=char.user)
                )
