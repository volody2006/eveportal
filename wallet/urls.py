# -*- coding: utf-8 -*-
from wallet import views

__author__ = 'user'

from django.conf.urls import url
# from wallet.views import *

urlpatterns = [

    url(r'^wallet/$', views.wallet_jornal_char_view, name='wallet_jo'),  #
    url(r'^wallet/journal/$', views.wallet_jornal_char_view, name='wallet_jo'),

    url(r'^wallet/journal/(?P<character_id>[^/]+)/$', views.wallet_jornal_char_view, name='wallet_jo'),
    url(r'^wallet/transactions/$', views.wallet_transaction_char_view, name='wallet_tr'),
    url(r'^wallet/transactions/(?P<character_id>[^/]+)/$', views.wallet_transaction_char_view,
        name='wallet_tr'),
    url(r'^wallet/statistic/$', views.stat_wallet_view, name='stat_wallet_view'),
    url(r'^wallet/statistic/(?P<character_id>[^/]+)/$', views.stat_wallet_view, name='stat_wallet_view'),
    url(r'^test/$', views.test, name='test'),

    # Corp view
    url(r'^wallet/corp/journal/$', views.corp_wallet_jornal_view, name='corp_wallet_jornal_view'),
    url(r'^wallet/corp/journal/(?P<account>[^/]+)/$', views.corp_wallet_jornal_view,
        name='corp_wallet_jornal_view'),
    url(r'^wallet/corp/stat/$', views.corp_stat_bounty, name='corp_stat_bounty'),
    url(r'^wallet/corp/stat/(?P<corporation_id>[^/]+)/$', views.corp_stat_bounty, name='corp_stat_bounty'),

    url(r'^(?P<lang>[a-zA-Z]{2})vall/$', views.change_lang1, name='change_lang1'),

]
