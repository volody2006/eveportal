# -*- coding: utf-8 -*-

__author__ = 'user'
from django import forms

class InputDate(forms.Form):
    from_date = forms.DateField()
    to_date = forms.DateField()
