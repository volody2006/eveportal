# -*- coding: utf-8 -*-
import logging
from django.utils.translation import ugettext_lazy as _
from django.db import models
from eveonline.models import Character, Corporation, Alliance

import datetime
from haystack import indexes
logger = logging.getLogger(__name__)

class TempParserZKill(models.Model):
    id = models.OneToOneField(Character, primary_key=True)
    zkillboard = models.BooleanField(default=False)
    #evewho = models.BooleanField(default=False)

class TempParserEveWho(models.Model):
    id = models.OneToOneField(Corporation, primary_key=True)
    evewho = models.BooleanField(default=False)

class TempParserEveWhoAli(models.Model):
    id = models.OneToOneField(Alliance, primary_key=True)
    evewho = models.BooleanField(default=False)


# for char in TempParserZKill.objects.all():
#     usr = char.id
#     usr.zkillboard = char.zkillboard
#     usr.save()




class BadCorp(models.Model):
    corp_id = models.BigIntegerField()
    error_text = models.TextField(default='')

    def __str__(self):
        return u'%s: %s' % (self.corp_id, self.error_text)
