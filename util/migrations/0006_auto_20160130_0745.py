# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('util', '0005_badcorp'),
    ]

    operations = [
        migrations.RenameField(
            model_name='badcorp',
            old_name='text',
            new_name='error_text',
        ),
    ]
