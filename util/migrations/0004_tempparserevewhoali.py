# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0005_auto_20160109_0132'),
        ('util', '0003_auto_20160113_1325'),
    ]

    operations = [
        migrations.CreateModel(
            name='TempParserEveWhoAli',
            fields=[
                ('id', models.OneToOneField(primary_key=True, serialize=False, to='eveonline.Alliance')),
                ('evewho', models.BooleanField(default=False)),
            ],
        ),
    ]
