# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0005_auto_20160109_0132'),
        ('util', '0002_tempparserzkill_evewho'),
    ]

    operations = [
        migrations.CreateModel(
            name='TempParserEveWho',
            fields=[
                ('id', models.OneToOneField(primary_key=True, serialize=False, to='eveonline.Corporation')),
                ('evewho', models.BooleanField(default=False)),
            ],
        ),
        migrations.RemoveField(
            model_name='tempparserzkill',
            name='evewho',
        ),
    ]
