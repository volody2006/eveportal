# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TempParserZKill',
            fields=[
                ('id', models.OneToOneField(primary_key=True, serialize=False, to='eveonline.Character')),
                ('zkillboard', models.BooleanField(default=False)),
            ],
        ),
    ]
