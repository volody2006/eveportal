# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('util', '0004_tempparserevewhoali'),
    ]

    operations = [
        migrations.CreateModel(
            name='BadCorp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('corp_id', models.BigIntegerField()),
                ('text', models.TextField(default=b'')),
            ],
        ),
    ]
