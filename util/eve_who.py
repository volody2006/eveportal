import json

from cache import DJCache
from eveonline.models import Corporation
import re
import logging
from time import sleep
from evelink import api
import six
import urllib2
log = logging.getLogger('evelink.thirdparty.eve_who')

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}


class FetchError(Exception):
    """Class for exceptions if fetch failed."""
    pass


class EVEWho(object):
    def __init__(self, url_fetch_func=None, cache=None, wait=True,
                 api_base='http://evewho.com/api.php'):
        super(EVEWho, self).__init__()

        self.api_base = api_base
        self.wait = wait

        if url_fetch_func is not None:
            self.url_fetch = url_fetch_func
        else:
            self.url_fetch = self._default_fetch_func

        cache = cache or DJCache(cache_name='evewho')
        if not isinstance(cache, api.APICache):
            raise ValueError("The provided cache must subclass from APICache.")
        self.cache = cache
        self.cachetime = 3600

    def _default_fetch_func(self, url):
        """Fetches a given URL using GET and returns the response."""
        req = urllib2.Request(url, headers=hdr)
        return urllib2.urlopen(req).read()
        # return six.moves.urllib.request.urlopen(url).read()

    def _cache_key(self, path, params):
        sorted_params = sorted(params.items())
        # Paradoxically, Shelve doesn't like integer keys.
        return str(hash((path, tuple(sorted_params))))

    def _get(self, ext_id, api_type, page=0):
        """Request page from EveWho api."""
        path = self.api_base
        params = {'id': ext_id,
                  'type': api_type,
                  'page': page}

        key = self._cache_key(path, params)
        cached_result = self.cache.get(key)
        if cached_result is not None:
            # Cached APIErrors should be re-raised
            if isinstance(cached_result, api.APIError):
                log.error("Raising cached error: %r" % cached_result)
                raise cached_result
                # Normal cached results get returned
            log.info("Cache hit, returning cached payload")
            return cached_result

        query = six.moves.urllib.parse.urlencode(params, True)
        url = '%s?%s' % (path, query)
        response = None

        regexp = re.compile("^hammering a website isn't very nice ya know.... please wait (\d+) seconds")
        i = 1
        hammering = True
        while hammering:
            response = self.url_fetch(url)
            hammering = regexp.findall(response)
            if hammering:
                if self.wait:
                    log.info("Fetch page waiting: %s (%s)" , url, response)
                    log.info('sleep %s sec' , (int(hammering[0])*i))
                    sleep(int(hammering[0])*i)
                    i = i + 4
                else:
                    log.error("Fetch page error: %s (%s)" , url, response)
                    raise FetchError(response)

        result = json.loads(response)
        self.cache.put(key, result, self.cachetime)
        return result

    def _member_list(self, ext_id, api_type):
        """Fetches member list for corporation or alliance.

        Valid api_types: 'corplist', 'allilist'.
        """
        if api_type not in ['corplist', 'allilist']:
            raise ValueError("not valid api type - valid api types: 'corplist' and 'allilist'.")

        member_count = 0
        page = 0
        members = []
        while page <= (member_count // 200):
            data = self._get(ext_id, api_type, page)
            try:
                info = data['info']
            except KeyError as e:
                log.warning('ERROR: KeyError: %s', e)
                # corp = Corporation.objects.get(corporation_id = ext_id)
                # corp.is_npc = True
                # corp.save()
                return members
            #print info
            if info:
                member_count = int(info['memberCount']) - 1    # workaround for numbers divisible by 200
            else:
                return members

            for member in data['characters']:
                try:
                    members.append({'name': str(member['name']),
                                'char_id': int(member['character_id']),
                                'corp_id': int(member['corporation_id']),
                                'alli_id': int(member['alliance_id'])})
                except UnicodeEncodeError:
                    members.append({'name': str(member['name'].encode("utf-8")),
                                'char_id': int(member['character_id']),
                                'corp_id': int(member['corporation_id']),
                                'alli_id': int(member['alliance_id'])})

            page += 1

        return members

    def corp_member_list(self, corp_id):
        """Fetch member list for a corporation.

        (Convenience wrapper for member_list.)
        """
        return self._member_list(corp_id, api_type='corplist')

    def alliance_member_list(self, alli_id):
        """Fetch member list for a alliance.

        (Convenience wrapper for member_list.)
        """
        return self._member_list(alli_id, api_type='allilist')
