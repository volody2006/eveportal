import json
import urllib2

from cache import DJCache
from eveonline.models import Corporation, Character
import re
import logging
from time import sleep
from evelink import api
import six

log = logging.getLogger(__name__)


class FetchError(Exception):
    """Class for exceptions if fetch failed."""
    pass

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}

class ZKillboardAPI(object):
    def __init__(self, url_fetch_func=None, cache=None, wait=True,
                 api_base='https://zkillboard.com/api'):
        super(ZKillboardAPI, self).__init__()

        self.api_base = api_base
        self.wait = wait

        if url_fetch_func is not None:
            self.url_fetch = url_fetch_func
        else:
            self.url_fetch = self._default_fetch_func

        cache = cache or DJCache(cache_name='zkillboard')
        if not isinstance(cache, api.APICache):
            raise ValueError("The provided cache must subclass from APICache.")
        self.cache = cache
        self.cachetime = 60

    # def _default_fetch_func(self, url):
    #     """Fetches a given URL using GET and returns the response."""
    #     return six.moves.urllib.request.urlopen(url).read()

    def _default_fetch_func(self, url):
        """Fetches a given URL using GET and returns the response."""
        req = urllib2.Request(url, headers=hdr)
        return urllib2.urlopen(req).read()


    def _cache_key(self, path, params):
        sorted_params = sorted(params.items())
        # Paradoxically, Shelve doesn't like integer keys.
        return str(hash((path, tuple(sorted_params))))

    def _get(self, ext_id, api_type):
        """Request page from EveWho api."""
        path = self.api_base
        params = {'id': ext_id,
                  'type': api_type,}

        key = self._cache_key(path, params)
        cached_result = self.cache.get(key)
        if cached_result is not None:
            # Cached APIErrors should be re-raised
            if isinstance(cached_result, api.APIError):
                log.error("Raising cached error: %r" % cached_result)
                raise cached_result
                # Normal cached results get returned
            log.debug("Cache hit, returning cached payload")
            return cached_result


        url = '%s/%s/%s' % (path, api_type, ext_id)
        # print url
        response = None

        response = self.url_fetch(url)


        result = json.loads(response)
        self.cache.put(key, result, self.cachetime)
        return result

    def _member_list(self, ext_id, api_type):
        """Fetches member list for corporation or alliance.

        Valid api_types: 'corplist', 'allilist'.
        """
        types = ['characterID', 'allilist']
        if api_type not in ['characterID', 'allilist']:
            raise ValueError("not valid api type - valid api types: 'characterID' and 'allilist'.")

        members = []
        data = self._get(ext_id, api_type)

        for kill in data:
         #   print kill
            for member in kill['attackers']:
                try:
                    members.append({'name': str(member['characterName']),
                                'char_id': int(member['characterID']),
                                'corp_id': int(member['corporationID']),
                                'alli_id': int(member['allianceID'])})
                except UnicodeEncodeError:
                    members.append({'name': str(member['characterName'].encode("utf-8")),
                                'char_id': int(member['characterID']),
                                'corp_id': int(member['corporationID']),
                                'alli_id': int(member['allianceID'])})
        return members

    def past_seconds(self, sec):
        data = self._get(sec, 'pastSeconds')
      #  print data
        members = []
        for kill in data:
         #   print kill
            for member in kill['attackers']:
                try:
                    members.append({'name': str(member['characterName']),
                                'char_id': int(member['characterID']),
                                'corp_id': int(member['corporationID']),
                                'alli_id': int(member['allianceID'])})
                except UnicodeEncodeError:
                    members.append({'name': str(member['characterName'].encode("utf-8")),
                                'char_id': int(member['characterID']),
                                'corp_id': int(member['corporationID']),
                                'alli_id': int(member['allianceID'])})




        return members



    def character_list (self, char_id):
        return self._member_list(char_id, api_type='characterID')

    def corp_member_list(self, corp_id):
        """Fetch member list for a corporation.

        (Convenience wrapper for member_list.)
        """
        return self._member_list(corp_id, api_type='corplist')

    def alliance_member_list(self, alli_id):
        """Fetch member list for a alliance.

        (Convenience wrapper for member_list.)
        """
        return self._member_list(alli_id, api_type='allilist')
