# -*- coding: utf-8 -*-
import calendar
from datetime import timedelta
from django.utils import timezone


def date_update(ts, period=1):
    _date = timezone.now() - timedelta(days=period)
    if _date > ts:
        return True
    else:
        return False


def date_update_timedelta(ts, **kwargs):
    _date = timezone.now() - timedelta(**kwargs)
    if _date > ts:
        return True
    else:
        return False


def _start_date(period, start=timezone.now()):
    """
    Returns a date with a timedelta days_from_now and time 0:0:0 to mark the start of the day.
    """
    return start_of_day(start) - timedelta(period)


def end_of_day(datetime):
    return datetime.replace(hour=23, minute=59, second=59)


def start_of_day(datetime):
    return datetime.replace(hour=0, minute=0, second=0)


def month_period(period=None):
    # TODO: Решить проблему с началом года и концом.
    # Пример. Если в начала января запросить предыдущий период, вылазит ошибка
    to_date = timezone.now()
    if period == 0:
        from_date = to_date.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        return from_date, to_date
    else:

        now_mon = to_date.month
        last_mon = now_mon - period
        year = to_date.year
        last_day = calendar.monthrange(year, last_mon)
        from_date = to_date.replace(month=last_mon, day=1, hour=0, minute=0, second=0, microsecond=0)
        to_date = to_date.replace(month=last_mon, day=last_day[1], hour=23, minute=59, second=59, microsecond=999999)
        return from_date, to_date


def date_end_month(to_date):
    month = to_date.month
    year = to_date.year
    last_day = calendar.monthrange(year, month)[1]
    return to_date.replace(month=month, day=last_day)

def date_start_of_month(datetime):
    return datetime.replace(day=1, hour=0, minute=0, second=0, microsecond=0)