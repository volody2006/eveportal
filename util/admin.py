from django.contrib import admin



# Register your models here.
from util.models import *


class BadCorpAdmin(admin.ModelAdmin):
    list_display = ('corp_id', 'error_text', )
    # list_filter = ('is_npc', 'is_del', 'is_citizen', 'evewho', 'zkillboard')
    search_fields = ['corp_id', 'error_text',]
    # raw_id_fields = ('api', 'alliance', 'corporation')

admin.site.register(BadCorp, BadCorpAdmin)
