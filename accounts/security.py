# -*- coding: utf-8 -*-
import logging

from accounts.models import IPAddressRecord
from eveonline.models import Character

logger = logging.getLogger('accounts')


def record_failed_request(request):
    record, __ = IPAddressRecord.objects.get_or_create(
            ip_address=request.META['REMOTE_ADDR'])
    record.increment_failures()


def record_successful_request(request):
    try:
        record, __ = IPAddressRecord.objects.get_or_create(
                ip_address=request.META['REMOTE_ADDR'])
    except IPAddressRecord.DoesNotExist:
        return
    record.reset()


def record_blocked_request(request):
    try:
        record, __ = IPAddressRecord.objects.get_or_create(
                ip_address=request.META['REMOTE_ADDR'])
    except IPAddressRecord.DoesNotExist:
        return
    record.increment_blocks()


def is_blocked(request):
    try:
        record = IPAddressRecord.objects.get(
                ip_address=request.META['REMOTE_ADDR'])
    except IPAddressRecord.DoesNotExist:
        record = IPAddressRecord(
                ip_address=request.META['REMOTE_ADDR'])
    return record.is_blocked()


def can_manage_corporate_account(user):
    if user.main_char:
        corporation = user.main_char.corporation
        if corporation.get_account():
            secondary_users = corporation.account.secondary_users.all()
            # Если юзер находится в примари или секондари, то разрешаем

            if user in secondary_users or user == corporation.account.primary_user:
                logger.info(u'Пользователь %s имеет разрешение на управление корп счетом %s', (user, corporation))
                return True
            # Если юзер имеет чара, который является сео данной корпорации, то разрешаем
            if corporation.ceo.user == user:
                logger.info(u'Пользователь %s имеет разрешение '
                            u'на управление корп счетом %s, он сео' % (user, corporation))
                return True

    return False
