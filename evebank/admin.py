# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _

from django.contrib import admin

from evebank.models import *


class ObligationsAdmin(admin.ModelAdmin):
    list_display = ('reference', 'source', 'destination',
                    'amount', 'contract', 'merchant_reference')
    list_filter = ('paid', 'removed', 'status' )
    search_fields = ['reference', 'description', 'user__username',
                     'merchant_reference', ]
    # raw_id_fields = ('api',)

admin.site.register(Obligations, ObligationsAdmin)