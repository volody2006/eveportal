# -*- coding: utf-8 -*-
from django.db import transaction
from django.utils import timezone

from accounts import exceptions
from django.db import models
# from oscar.core.loading import get_model
from accounts.models import Transfer, Account
import uuid
import hmac
from eveportal.settings import AUTH_USER_MODEL, SECRET_KEY
from rent.models import RentContract
from django.utils.translation import ugettext as _

# Account = get_model('accounts', 'Account')
# Transfer = get_model('accounts', 'Transfer')

class PostingManager(models.Manager):
    """
    Custom manager to provide a new 'create' method to create a new transfer.

    Apparently, finance people refer to "posting a transaction"; hence why this
    """

    def create(self, source, destination, amount, parent=None,
               user=None, merchant_reference=None, description=None):
        # Write out transfer (which involves multiple writes).  We use a
        # database transaction to ensure that all get written out correctly.
        self.verify_transfer(source, destination, amount, user)

        with transaction.atomic():

            transfer = self.get_queryset().create(
                source=source,
                destination=destination,
                amount=amount,
                parent=parent,
                user=user,
                merchant_reference=merchant_reference,
                description=description
            )

            # Create transaction records for audit trail
            transfer.transactions.create(
                account=source, amount=-amount)
            transfer.transactions.create(
                account=destination, amount=amount)
            # Update the cached balances on the accounts
            source.save()
            destination.save()
            return self._wrap(transfer)

    def _wrap(self, obj):
        # Dumb method that is here only so that it can be mocked to test the
        # transaction behaviour.
        return obj

    def verify_transfer(self, source, destination, amount, user=None):
        """
        Test whether the proposed transaction is permitted.  Raise an exception
        if not.
        """
        if amount <= 0:
            raise exceptions.InvalidAmount("Debits must use a positive amount")
        if not source.is_open():
            raise exceptions.ClosedAccount("Source account has been closed")
        if not source.can_be_authorised_by(user):
            raise exceptions.AccountException(
                "This user is not authorised to make transfers from "
                "this account")
        if not destination.is_open():
            raise exceptions.ClosedAccount(
                "Destination account has been closed")
        if not source.is_debit_permitted(amount):
            msg = "Unable to debit %.2f from account #%d:"
            raise exceptions.InsufficientFunds(
                msg % (amount, source.id))


class PTransfer(Transfer):
    objects = PostingManager()

    def _generate_reference(self):
        #obj = hmac.new(key=str.encode(settings.SECRET_KEY),
        #               msg=self.id)
        return uuid.uuid4()

    class Meta:
        proxy = True


class Obligations(models.Model):
    """
    A transfer of funds between two accounts.

    This object records the meta-data about the transfer such as a reference
    number for it and who was the authorisor.  The financial details are help
    within the transactions.  Each transfer links to TWO account transactions
    """
    STATUS =(
        (0, _('NEW')),
        (1, _('PAID')), # Оплачен
        (2, _('Late payment')),   # Просрочен
        (3, _('DELETE')),    # Удален

    )
    # We generate a reference for each transaction to avoid passing around
    # primary keys
    # Код счета (проверяется при поступлений денег),
    # При совпадение оплачивается автоматически
    reference = models.CharField(max_length=64, unique=True, null=True)
    # Кто оплачивает счет (отправитель)
    source = models.ForeignKey(Account,
                               related_name='source_obligations')
    # Кто получает деньги по счету (Получатель)
    destination = models.ForeignKey(Account,
                                    related_name='destination_obligations')
    # Сумма
    amount = models.DecimalField(decimal_places=2, max_digits=12)

    contract = models.ForeignKey(RentContract, blank=True, null=True, on_delete=models.SET_NULL,
                                 related_name='contract_obligations')
    # Если это периодический платеж, то указываем предыдущий
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL,
                               related_name='parent_obligation')
    # Если платеж был совершен, показываем транзакцию
    transfer = models.ForeignKey(Transfer, blank=True, null=True, on_delete=models.SET_NULL,
                                 related_name='completed_transactions')
    # Optional meta-data about transfer
    merchant_reference = models.CharField(max_length=128, blank=True, null=True)
    description = models.CharField(max_length=256, blank=True, null=True)

    # We record who the user was who authorised this transaction.  As
    # transactions should never be deleted, we allow this field to be null and
    # also record some audit information.
    user = models.ForeignKey(AUTH_USER_MODEL, related_name="obligations",
                             null=True, on_delete=models.SET_NULL)
    username = models.CharField(max_length=128)

    status = models.IntegerField(choices=STATUS, default=0,
                                 verbose_name=_('Obligations status'))
    paid = models.BooleanField(default=False)

    removed = models.BooleanField(default=False)

    date_created = models.DateTimeField(auto_now_add=True)
    date_payment = models.DateTimeField(blank=True, null=True)
    date_fact_payment = models.DateTimeField(blank=True, null=True)



    # Use a custom manager that extends the create method to also create the
    # account transactions.


    def __unicode__(self):
        return self.reference

    class Meta:
        ordering = ('-date_created',)

    def delete(self, *args, **kwargs):
        raise RuntimeError("Transfers cannot be deleted")

    def save(self, *args, **kwargs):
        # Store audit information about authorising user (if one is set)
        if self.user:
            self.username = self.user.username
        # We generate a transaction reference using the PK of the transfer so
        # we save the transfer first
        super(Obligations, self).save(*args, **kwargs)
        if not self.reference:
            self.reference = self._generate_reference()
            super(Obligations, self).save()

    def _generate_reference(self):
        obj = hmac.new(key=SECRET_KEY,
                       msg=unicode(self.id))
        return obj.hexdigest().upper()

    @property
    def authorisor_username(self):
        if self.user:
            return self.user.username
        return self.username

    def get_status(self):
        if self.paid:
            self.status = 1
            self.save()
            return self.status
        elif self.removed:
            self.status = 3
            self.save()
            return self.status
        elif self.date_payment:
            date = self.date_payment.replace(hour=23, minute=59)
            if date < timezone.now():
                self.status = 2
                self.save()
                return self.status
        return self.status


