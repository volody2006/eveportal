# -*- coding: utf-8 -*-
# from django.utils.translation import ugettext as _
import logging

from django.utils import timezone

from accounts import facade

from accounts.models import Account
from evebank.models import Obligations
from eveonline.models import Character
from util.managers import month_period

logger = logging.getLogger(__name__)

class UserBank:
    def __init__(self):
        pass

    def get_char_account(self, char_id, user):
        char = Character.objects.get(pk=char_id)
        name = 'Character'
        active_accounts = Account.active.filter(primary_user=user)

    def geterate_code_account(self, obj):
        name = obj.__class__.__name__
        if name == 'Character':
            name2 = obj


def get_merchant_reference(period):
    start_date = period[0].date()
    end_date = period[1].date()
    return str('%s - %s' % (start_date, end_date))


def create_obligations(source, destination, amount, user=None, date_payment=None, **kwargs):
    obligation = Obligations.objects.create()

    # reference = models.CharField(max_length=64, unique=True, null=True)
    # Кто оплачивает счет (отправитель)
    # source = models.ForeignKey(Account,
    #                            related_name='source_obligations')
    # Кто получает деньги по счету (Получатель)
    # destination = models.ForeignKey(Account,
    #                                 related_name='destination_obligations')
    # Сумма
    # amount = models.DecimalField(decimal_places=2, max_digits=12)


    # Optional meta-data about transfer
    # merchant_reference = models.CharField(max_length=128, null=True)
    # description = models.CharField(max_length=256, null=True)

    # We record who the user was who authorised this transaction.  As
    # transactions should never be deleted, we allow this field to be null and
    # also record some audit information.
    # user = models.ForeignKey(AUTH_USER_MODEL, related_name="obligations",
    #                          null=True, on_delete=models.SET_NULL)
    # username = models.CharField(max_length=128)
    #
    # paid = models.BooleanField(default=False)
    # removed = models.BooleanField(default=False)
    #
    # date_created = models.DateTimeField(auto_now_add=True)
    # date_payment = models.DateTimeField(null=True)
    # date_fact_payment = models.DateTimeField(null=True)

def pain_obligation(obligation):
    logger.info(u'Пытаемся оплатить %s' % obligation)
    if obligation.contract.status in [0, 2, 3]:
        obligation.removed = True
        obligation.save()
        obligation.get_status()
        return obligation
    if obligation.amount <= obligation.source.balance:
        # Денег хватает на оплату счета? Если да, проверям а выполнили мы оплату более ранних не просроченных счетов
        if obligation.parent:
            if obligation.parent.status == 0:
                logger.info('Мы нашли родительский не оплаченный счет %s, оплачиваем его.' % obligation.parent)
                pain_obligation(obligation)
            if obligation.parent.status != 1:
                logger.info('Родительский счет так и не оплачен %s, Ничего не делаем.' % obligation.parent)
                return obligation
        logger.info(u'Если денег на счету хватает, может сразу и оплатим? %s' % obligation)
        transfer = facade.transfer(source=obligation.source,
                                   destination=obligation.destination,
                                   amount=obligation.amount,
                                  # parent=obligation.parent,
                                   user=obligation.user,
                                   merchant_reference=obligation.merchant_reference,
                                   description=(
                                   'Automatic payment of invoice No. %s %s' % (obligation.pk, obligation)))
        obligation.paid = True
        obligation.status = 1
        obligation.date_fact_payment = timezone.now()
        obligation.transfer = transfer
        obligation.save()
    elif obligation.get_status() == 2:
        logger.info(u'Денег нет и статус просрочен. Закрываем контракт %s' % obligation.contract)
        contract = obligation.contract
        contract.close_now()
    return obligation


def permission_corp_account(user, corporation):
    qs = corporation.account.secondary_users.all()
    # Если юзер находится в примари или секондари, то разрешаем

    if user in qs or user == corporation.account.primary_user:
        logger.info(u'Даем пользователю %s разрешение на управление корп счетом %s', (user, corporation))
        return True
    # Если юзер имеет чара, который является сео данной корпорации, то разрешаем
    chars = Character.objects.filter(user=user)
    for char in chars:
        if char == corporation.ceo:
            logger.info(u'Даем пользователю %s разрешение '
                        u'на управление корп счетом %s, он сео %s' % (user, corporation, char))
            return True
    return False