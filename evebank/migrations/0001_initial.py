# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transfer',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('accounts.transfer',),
        ),
    ]
