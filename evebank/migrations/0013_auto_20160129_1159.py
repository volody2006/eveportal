# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0012_auto_20160122_2017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='obligations',
            name='status',
            field=models.IntegerField(default=0, verbose_name='Obligations status', choices=[(0, 'NEW'), (1, 'PAID'), (2, 'Late payment'), (3, 'DELETE')]),
        ),
    ]
