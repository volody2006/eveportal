# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0005_obligations_status'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='obligations',
            options={'ordering': ('date_created',)},
        ),
    ]
