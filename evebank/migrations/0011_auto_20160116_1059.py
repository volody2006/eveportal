# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0010_auto_20160116_1052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='obligations',
            name='parent',
            field=models.ForeignKey(related_name='parent_obligation', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='evebank.Obligations', null=True),
        ),
    ]
