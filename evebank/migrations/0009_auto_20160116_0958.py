# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0008_auto_20160112_1751'),
    ]

    operations = [
        migrations.AlterField(
            model_name='obligations',
            name='date_fact_payment',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='obligations',
            name='date_payment',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='obligations',
            name='description',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='obligations',
            name='merchant_reference',
            field=models.CharField(max_length=128, null=True, blank=True),
        ),
    ]
