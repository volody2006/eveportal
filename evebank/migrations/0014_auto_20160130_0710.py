# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0013_auto_20160129_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='obligations',
            name='status',
            field=models.IntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u043e\u0431\u044f\u0437\u0430\u0442\u0435\u043b\u044c\u0441\u0442\u0432\u0430', choices=[(0, '\u041d\u043e\u0432\u044b\u0435'), (1, '\u041e\u043f\u043b\u0430\u0447\u0435\u043d'), (2, '\u043f\u043b\u0430\u0442\u0435\u0436 \u043f\u0440\u043e\u0441\u0440\u043e\u0447\u0435\u043d'), (3, '\u0423\u0434\u0430\u043b\u0435\u043d\u043e')]),
        ),
    ]
