# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_core_accounts'),
        ('evebank', '0009_auto_20160116_0958'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Transfer',
        ),
        migrations.CreateModel(
            name='PTransfer',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('accounts.transfer',),
        ),
        migrations.AlterField(
            model_name='obligations',
            name='transfer',
            field=models.ForeignKey(related_name='completed_transactions', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounts.Transfer', null=True),
        ),
    ]
