# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0006_auto_20160111_1322'),
    ]

    operations = [
        migrations.AddField(
            model_name='obligations',
            name='parent',
            field=models.ForeignKey(related_name='parent_obligation', to='evebank.Obligations', null=True),
        ),
        migrations.AddField(
            model_name='obligations',
            name='transfer',
            field=models.ForeignKey(related_name='completed_transactions', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='evebank.Transfer', null=True),
        ),
    ]
