# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_core_accounts'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('evebank', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Obligations',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reference', models.CharField(max_length=64, unique=True, null=True)),
                ('amount', models.DecimalField(max_digits=12, decimal_places=2)),
                ('merchant_reference', models.CharField(max_length=128, null=True)),
                ('description', models.CharField(max_length=256, null=True)),
                ('username', models.CharField(max_length=128)),
                ('paid', models.BooleanField(default=False)),
                ('removed', models.BooleanField(default=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_payment', models.DateTimeField(null=True)),
                ('destination', models.ForeignKey(related_name='destination_obligations', to='accounts.Account')),
                ('source', models.ForeignKey(related_name='source_obligations', to='accounts.Account')),
                ('user', models.ForeignKey(related_name='obligations', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-date_created',),
            },
        ),
    ]
