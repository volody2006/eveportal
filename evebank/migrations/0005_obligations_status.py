# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0004_obligations_contract'),
    ]

    operations = [
        migrations.AddField(
            model_name='obligations',
            name='status',
            field=models.IntegerField(default=0, verbose_name='Obligations status', choices=[(0, 'NEW'), (1, 'PAID'), (2, 'Late payment'), (3, 'DELETE')]),
        ),
    ]
