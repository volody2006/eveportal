# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0007_auto_20160111_1802'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='obligations',
            options={'ordering': ('-date_created',)},
        ),
    ]
