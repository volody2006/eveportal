# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0004_auto_20160108_2325'),
        ('evebank', '0003_obligations_date_fact_payment'),
    ]

    operations = [
        migrations.AddField(
            model_name='obligations',
            name='contract',
            field=models.ForeignKey(related_name='contract_obligations', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='rent.RentContract', null=True),
        ),
    ]
