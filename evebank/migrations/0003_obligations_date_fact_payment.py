# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evebank', '0002_obligations'),
    ]

    operations = [
        migrations.AddField(
            model_name='obligations',
            name='date_fact_payment',
            field=models.DateTimeField(null=True),
        ),
    ]
