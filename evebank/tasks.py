# -*- coding: utf-8 -*-
from celerytask.managers import run_task
from django.utils import timezone
from accounts import facade
from django.utils.translation import ugettext as _
import logging

from eveportal import settings
from os import remove
from celery.schedules import crontab
from celery.task import task, periodic_task
from evebank.managers import get_merchant_reference, pain_obligation
from evebank.models import Obligations
from eveonline.models import Alliance, SystemSovereignty, Corporation
from eveportal.settings import EVEBANK_CORP
from rent.models import RentContract
from util.managers import date_end_month, date_start_of_month, month_period

logger = logging.getLogger(__name__)


@periodic_task(run_every=crontab(minute=19))
def task_obligations():
    name = ('%s:%s' % (__name__, 'task_obligations'))
    delta = 19*60
    if not run_task(key=name, delta=delta):
        # Здесь пытаемся оплатить существующие счета
        obligations = Obligations.objects.filter(status=0)
        for obligation in obligations:
           # print (obligation.date_created, obligation.pk)
            pain_obligation(obligation)


@periodic_task(run_every=crontab(minute=59))
def new_obligation():
    name = ('%s:%s' % (__name__, 'new_obligation'))
    delta = 59*60
    if not run_task(key=name, delta=delta):
        # Создаем новый счет на будующий месяц
        rent_cotracts = RentContract.objects.filter(status=5)
        for rent_cotract in rent_cotracts:
            if Obligations.objects.filter(contract=rent_cotract).exists():
                parent = Obligations.objects.filter(contract=rent_cotract)[0]
            else:
                parent = None
            if rent_cotract.created.date().month == timezone.now().date().month:
                # Если контракт заключен в этом месяце, выписываем счет на будующий месяц
                # с учетом кооректировки платежа. Счет выписан при заключение контракта
                parent = Obligations.objects.filter(contract=rent_cotract)[0]
                period = month_period(-1)
                logger.info(u'Проверяем выписан ли счет за период c %s по %s' % period)
                if not Obligations.objects.filter(merchant_reference=get_merchant_reference(period)).exists():
                    logger.info(u'Счет не выписан, надо выписать')
                    date_created = rent_cotract.created.date()
                    end_date_created = date_end_month(date_created)
                    prepaid_period = end_date_created - date_created
                    #print end_date_created
                    #print (rent_cotract.price, end_date_created.day, prepaid_period.days)
                    funds_expended = rent_cotract.price / (end_date_created.day) * prepaid_period.days
                    # the overpayment for the last month
                    summa = rent_cotract.price - funds_expended
                    amount = rent_cotract.price - summa
                    obligation = Obligations.objects.create(source=rent_cotract.renter_corp.get_account(),
                                                            destination=Corporation.objects.get(pk=EVEBANK_CORP).get_account(),
                                                            amount=amount,
                                                            user=rent_cotract.user,
                                                            merchant_reference=get_merchant_reference(period),
                                                            username=rent_cotract.user.username,
                                                            contract=rent_cotract,
                                                            parent = parent,
                                                            date_payment=period[0])
                    logger.info(u'Создали счет %s, на сумму %s' % (obligation, obligation.amount))
                    # Если денег на счету хватает, может сразу и оплатим?
                    pain_obligation(obligation)
            else:
                period = month_period(-1)
                logger.info(u'Проверяем выписан ли счет за период %s' % period)
                if not Obligations.objects.filter(merchant_reference=get_merchant_reference(period)).exists():
                    logger.info(u'Счет не выписан, надо выписать')
                    obligation = Obligations.objects.create(source=rent_cotract.renter_corp.get_account(),
                                                            destination=Corporation.objects.get(pk=EVEBANK_CORP).get_account(),
                                                            amount=rent_cotract.price,
                                                            user=rent_cotract.user,
                                                            merchant_reference=get_merchant_reference(period),
                                                            username=rent_cotract.user.username,
                                                            contract=rent_cotract,
                                                            parent = parent,
                                                            date_payment=(period[1]-timezone.timedelta(days=7)))
                    logger.info(u'Создали счет %s, на сумму %s' % (obligation, obligation.amount))
                    # Если денег на счету хватает, может сразу и оплатим?
                    pain_obligation(obligation)