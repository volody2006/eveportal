# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _

from modeltranslation.translator import translator, TranslationOptions
from .models import Section, Post, PPost


class SectionTranslationOptions(TranslationOptions):
    """
    Класс настроек интернационализации полей модели Page.
    """

    fields = ('name',)

translator.register(Section, SectionTranslationOptions)

class PostTranslationOptions(TranslationOptions):
    """
    Класс настроек интернационализации полей модели Page.
    """

    fields = ('title', 'description' ,)

translator.register(Post, PostTranslationOptions)

class PPostTranslationOptions(TranslationOptions):
    """
    Класс настроек интернационализации полей модели Page.
    """

    fields = ('title', 'description' ,)

translator.register(PPost, PPostTranslationOptions)

