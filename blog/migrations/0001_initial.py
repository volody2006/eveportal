# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FeedHit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('request_data', models.TextField(verbose_name='Request data')),
                ('created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created')),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_path', models.ImageField(upload_to=b'images/%Y/%m/%d')),
                ('url', models.CharField(max_length=150, verbose_name='Url', blank=True)),
                ('timestamp', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Timestamp', editable=False)),
            ],
            options={
                'verbose_name': 'Image',
                'verbose_name_plural': 'Images',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=90, verbose_name='Title')),
                ('title_ru', models.CharField(max_length=90, null=True, verbose_name='Title')),
                ('title_en', models.CharField(max_length=90, null=True, verbose_name='Title')),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('slug_ru', models.SlugField(unique=True, null=True, verbose_name='Slug')),
                ('slug_en', models.SlugField(unique=True, null=True, verbose_name='Slug')),
                ('markup', models.CharField(max_length=25, verbose_name='Markup', choices=[('markdown', 'Markdown')])),
                ('teaser_html', models.TextField(editable=False)),
                ('content_html', models.TextField(editable=False)),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('description_ru', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('description_en', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('tweet_text', models.CharField(verbose_name='Tweet text', max_length=140, editable=False)),
                ('created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created', editable=False)),
                ('updated', models.DateTimeField(verbose_name='Updated', null=True, editable=False, blank=True)),
                ('published', models.DateTimeField(verbose_name='Published', null=True, editable=False, blank=True)),
                ('state', models.IntegerField(default=1, verbose_name='State', choices=[(1, 'Draft'), (2, b'Published')])),
                ('secret_key', models.CharField(help_text='allows url for sharing unpublished posts to unauthenticated users', unique=True, max_length=8, verbose_name='Secret key', blank=True)),
                ('view_count', models.IntegerField(default=0, verbose_name='View count', editable=False)),
                ('author', models.ForeignKey(related_name='posts', verbose_name='Author', to=settings.AUTH_USER_MODEL)),
                ('primary_image', models.ForeignKey(related_name='+', verbose_name='Primary Image', blank=True, to='blog.Image', null=True)),
            ],
            options={
                'ordering': ('-published',),
                'get_latest_by': 'published',
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
            },
        ),
        migrations.CreateModel(
            name='ReviewComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('review_text', models.TextField(verbose_name='Review text')),
                ('timestamp', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Timestamp')),
                ('addressed', models.BooleanField(default=False, verbose_name='Addressed')),
                ('post', models.ForeignKey(related_name='review_comments', verbose_name='Post', to='blog.Post')),
            ],
        ),
        migrations.CreateModel(
            name='Revision',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=90, verbose_name='Title')),
                ('teaser', models.TextField(verbose_name='Teaser')),
                ('content', models.TextField(verbose_name='Content')),
                ('updated', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Updated')),
                ('published', models.DateTimeField(null=True, verbose_name='Published', blank=True)),
                ('view_count', models.IntegerField(default=0, verbose_name='View count', editable=False)),
                ('author', models.ForeignKey(related_name='revisions', verbose_name='Author', to=settings.AUTH_USER_MODEL)),
                ('post', models.ForeignKey(related_name='revisions', verbose_name='Post', to='blog.Post')),
            ],
            options={
                'verbose_name': 'Revision',
                'verbose_name_plural': 'Revisions',
            },
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=150)),
                ('name_ru', models.CharField(max_length=150, unique=True, null=True)),
                ('name_en', models.CharField(max_length=150, unique=True, null=True)),
                ('slug', models.SlugField(unique=True)),
                ('enabled', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'Section',
                'verbose_name_plural': 'Sections',
            },
        ),
        migrations.AddField(
            model_name='post',
            name='section',
            field=models.ForeignKey(to='blog.Section'),
        ),
        migrations.AddField(
            model_name='image',
            name='post',
            field=models.ForeignKey(related_name='images', verbose_name='Post', to='blog.Post'),
        ),
    ]
