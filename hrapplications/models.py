from django.db import models
from eveonline.models import Corporation, ApiKeyPair
from eveonline.models import Character
from users.models import CustomUser


class HRApplication(models.Model):
    character = models.ForeignKey(Character, related_name="hr_character")
    api = models.ForeignKey(ApiKeyPair)
    is_a_spi = models.CharField(max_length=254, default="")
    about = models.TextField(default="")
    extra = models.TextField(default="")

    corp = models.ForeignKey(Corporation)
    user = models.ForeignKey(CustomUser, blank=True, null=True)

    approved_denied = models.NullBooleanField(blank=True, null=True)
    reviewer_user = models.ForeignKey(CustomUser, blank=True, null=True, related_name="review_user")
    reviewer_character = models.ForeignKey(Character, blank=True, null=True, related_name="hr_reviewer_character")
    reviewer_inprogress_character = models.ForeignKey(Character, blank=True, null=True,
                                                      related_name="inprogress_character")
    is_activ = models.BooleanField(default=True)

    def __str__(self):
        return str(self.character) + " - Application"

