# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _




from django.conf.urls import url
from . import views

urlpatterns = [
                        # HR Application Management
                       url(r'^hr_application_management/', views.hr_application_management_view,
                             name="auth_hrapplications_view"),
                       url(r'^hr_application_create/', views.hr_application_create_view,
                             name="auth_hrapplication_create_view"),
                       url(r'^hr_application_remove/(\w+)', views.hr_application_remove,
                             name="auth_hrapplication_remove"),
                       url(r'^hr_application_view/(\w+)', views.hr_application_view,
                             name="auth_hrapplication_view"),
                       url(r'^hr_application_personal_view/(\w+)', views.hr_application_personal_view,
                             name="auth_hrapplication_personal_view"),
                       url(r'^hr_application_personal_removal/(\w+)',
                             views.hr_application_personal_removal,
                             name="auth_hrapplication_personal_removal"),
                       url(r'^hr_application_approve/(\w+)', views.hr_application_approve,
                             name="auth_hrapplication_approve"),
                       url(r'^hr_application_reject/(\w+)', views.hr_application_reject,
                             name="auth_hrapplication_reject"),
                       url(r'^hr_application_search/', views.hr_application_search,
                             name="auth_hrapplication_search"),
                       url(r'^hr_mark_in_progress/(\w+)', views.hr_application_mark_in_progress,
                             name="auth_hrapplication_mark_in_progress"),


]

# Страница подачи заявки

# Страница пока заявок
# Страница рассмотрения заявки
# страница поиска
