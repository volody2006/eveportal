# -*- coding: utf-8 -*-
from django.core.exceptions import MultipleObjectsReturned
from django.core.urlresolvers import reverse
from django.test import Client
from django.test.testcases import TestCase
from django.utils.datetime_safe import datetime
from djcelery.utils import make_aware
from eveonline.managers import EveManager
from eveonline.models import Character, Corporation, Alliance, CharacterCorpHistory, ApiKeyPair
from hrapplications.models import HRApplication
import mock
from test.test_base import APITestCase
from test.util import TestBase
from users.models import CustomUser
from mixer.backend.django import mixer

class TestHR_no_fixture(TestCase):

    def setUp(self):

        super(TestHR_no_fixture, self).setUp()
        self.url = reverse('auth_hrapplications_view')
        self.user1 = CustomUser.objects.create_user(username='user', password='user', email='user@user.com')
        self.user = mixer.blend(CustomUser)
        self.moderator = CustomUser.objects.create_superuser(username='moder', password='moder', email='m@m.ru')
        self.bad_user = CustomUser.objects.create_user(username='rediska', email='rediska@dofigaumniy.ru', password='rediska')
        mixer.blend(Alliance, alliance_id = 99005103, alliance_name = 'Independent Union')


    def login_user(self):
        self.client.logout()
        self.client.login(username='user', password='user')
        self.url = reverse('auth_hrapplications_view')


    def became_anonymous(self):
        """
        Выполняет действия от имени анонимного пользователя
        """
        self.client.logout()

    # def login_as_moderator(self):
    #     """
    #     Меняет текущего пользователя на модератора
    #     """
    #     self.client.logout()
    #     self.client.login(username='moder', password='moder')

    def login_as_bad_user(self):
        """
        Меняет текущего пользователя на "не владельца объявления"
        """
        self.client.logout()
        self.client.login(username='rediska', password='rediska')

    def test_hr_application_create_view_no_fix(self):
        self.login_user()
        page = self.client.get(self.url)
        self.assertEquals(200, page.status_code)
        #self.assertTemplateUsed(page, 'dashboard/hrapplications/hrcreateapplication.html')
        from hrapplications.forms import HRApplicationForm
        form = HRApplicationForm()


class TestHR(TestBase):



    def setUp(self):

        super(TestHR, self).setUp()
        self.url = reverse('auth_hrapplication_create_view')
        self.corps = mixer.cycle(5).blend(Corporation, alliance=self.alliance )



    def test_hr_application_create_view_good(self):
        data_api = {'api_id':  '4641111',
                    'api_key': 'bmVVdUoBtamumPM6g9AevKDGDnxblLZI98O4ZqBI5ObdVuAxJZGhLtXvP5epAAI9'}
        for id in  self.get_api_info(data_api['api_id'])['characters']:
            mixer.blend(Character, pk = id, character_name = self.get_api_info(data_api['api_id'])['characters'][id]['name'])

        with mock.patch(
                        'eveonline.eve_api_manager.EveApiManager.check_api_is_valid',
                         return_value = self.check_api_is_valid(data_api['api_id'])
        ),   mock.patch('eveonline.eve_api_manager.EveApiManager.check_api_is_type_account',
                         return_value = self.check_api_is_type_account(data_api['api_id'])
        ),   mock.patch('eveonline.eve_api_manager.EveApiManager.get_characters_from_api',
                         return_value = self.get_characters_from_api(data_api['api_id'])
        ),   mock.patch('eveonline.eve_api_manager.EveApiManager.check_api_is_full',
                         return_value = True
        ),   mock.patch('eveonline.eve_api_manager.EveApiManager.get_api_info',
                         return_value = self.get_api_info(data_api['api_id'])
        ):

            page = self.client.get(self.url)

            self.assertEquals(200, page.status_code)
            self.assertTemplateUsed(page, 'dashboard/hrapplications/hrcreateapplication.html')
            self.assertIn('form', page.context)

            # И засылаем форму
            data = {}
            data['character_name'] = 'Lili Cadelanne'
            data['full_api_id'] = data_api['api_id']
            data['full_api_key'] = data_api['api_key']
            data['corp'] = int(self.corps[0].pk)
            data['is_a_spi'] = 'Yes'
            data['about'] = 'Test about'
            data['extra'] = 'Test extra'

            from hrapplications.forms import HRApplicationForm
            form = HRApplicationForm(data)

            self.assertTrue(form.is_valid())

            page = self.client.post(self.url, data)

            hr1 = HRApplication.objects.get(api = data['full_api_id'])
            self.assertEquals(hr1.user, self.user)
            self.assertEqual(str(hr1), data['character_name'] + " - Application")

            # Чары загрузились?
            char = Character.objects.get(character_name = data['character_name'])
            self.assertEquals(char.character_name, data['character_name'])

            # Чары юзеру присвоились?
            self.assertEquals(char.user, self.user)

            user = CustomUser.objects.get(pk = self.user.pk)
            self.assertEquals(user.main_char, char)

    def test_hr_api_key_key_belongs_to_another_user(self):
        #Сделать тест, заявки при вводе апи другого юзера должны
        # отклонятся с уведомлением юзера.

        pass
