from django.contrib import admin

from models import HRApplication



class HRApplicationAdmin(admin.ModelAdmin):
    # list_display = ('alliance', 'period', )
    # list_filter = ('period', 'alliance', )
    pass

admin.site.register(HRApplication, HRApplicationAdmin)