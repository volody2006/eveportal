# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponseRedirect
from django.utils import timezone
from eveonline.models import Corporation, ApiKeyPair, CharacterCorpHistory
from eveonline.models import Character


from django.conf import settings
from eveonline.managers import EveManager
from django.utils.translation import ugettext as _
from hrapplications.forms import HRApplicationSearchForm, HRApplicationForm
from hrapplications.models import HRApplication


@login_required
def hr_application_management_view(request):
    # Для супер узера все заявки, для сео корпорации только его
    personal_app = None
    corp_applications = None

    if request.user.is_superuser:
        corp_applications = HRApplication.objects.all()
    else:
        # Get the corp the member is in

        if request.user.main_char != None:
            main_alliance_id = request.user.main_char.alliance
            if main_alliance_id == settings.ALLIANCE_ID:
                main_char = request.user.main_char
                corp = Corporation.objects.get(corporation=main_char.corporation)
                corp_applications = HRApplication.objects.filter(corp=corp).filter(approved_denied=None)
            else:
                corp_applications = None
            pass
    context = {'personal_apps': HRApplication.objects.all().filter(user=request.user),
               'applications': corp_applications,
               'search_form': HRApplicationSearchForm()}

    return render_to_response('dashboard/hrapplications/hrapplicationmanagement.html',
                              context, context_instance=RequestContext(request))


@login_required
def hr_application_create_view(request):
    success = False

    if request.method == 'POST':
        form = HRApplicationForm(request.POST)
        if form.is_valid():
            evemanager = EveManager()
            evemanager.create_api_keypair(form.cleaned_data['full_api_id'],
                                          form.cleaned_data['full_api_key'],
                                          request.user)
            application = HRApplication()
            application.user = request.user
            application.character = Character.objects.get(character_name= form.cleaned_data['character_name'])
            application.api = ApiKeyPair.objects.get(api_id = form.cleaned_data['full_api_id'])
            application.corp = Corporation.objects.get(corporation_id=form.cleaned_data['corp'].pk)
            application.is_a_spi = form.cleaned_data['is_a_spi']
            application.about = form.cleaned_data['about']
            application.extra = form.cleaned_data['extra']
            application.save()
            success = True
            if request.user.main_char == None:
                request.user.main_char = application.character
                request.user.save()

    else:
        form = HRApplicationForm()

    context = {'form': form, 'success': success}
    return render_to_response('dashboard/hrapplications/hrcreateapplication.html',
                              context, context_instance=RequestContext(request))


@login_required
def hr_application_personal_view(request, app_id):
    if HRApplication.objects.filter(id=app_id).exists():
        application = HRApplication.objects.get(id=app_id)
        if application.user != request.user:
            application = HRApplication()
    else:
        application = HRApplication()
    corphistorys= []
    corphistorys_obj = CharacterCorpHistory.objects.filter(character=application.character).order_by('-start_date')
    try:

        last_date = corphistorys_obj[0].start_date

        for corphistory in corphistorys_obj:

            if corphistory.start_date < last_date:
                end_date = last_date
                last_date = corphistory.start_date
                delta = end_date - corphistory.start_date
            else:
                end_date = None
                delta =  timezone.now() - corphistory.start_date


            corphistorys.append({
                'corp': corphistory.corporation,
                'start_date': corphistory.start_date,
                'end_date' : end_date,
                'delta' : delta
            })
    except IndexError:
        corphistorys= []


    context = {'application': application,
               'corphistory': corphistorys}
    template_name = 'dashboard/hrapplications/char_hr_application_view.html'
    _context_instance_undefined = RequestContext(request)
    return render_to_response(template_name=template_name, context=context,
                       context_instance=_context_instance_undefined)
    # return render_to_response('dashboard/hrapplications/hrapplicationview.html',
    #                           context, context_instance=RequestContext(request))


@login_required
def hr_application_personal_removal(request, app_id):
    if HRApplication.objects.filter(id=app_id).exists():
        application = HRApplication.objects.get(id=app_id)
        if application.user == request.user:
            application.delete()

    return HttpResponseRedirect(reverse("auth_hrapplications_view"))


@login_required
@permission_required('users.human_resources')
def hr_application_view(request, app_id):

    if HRApplication.objects.filter(id=app_id).exists():
        application = HRApplication.objects.get(id=app_id)

    else:
        application = HRApplication()


    context = {'application': application, }

    return render_to_response('dashboard/hrapplications/hrapplicationview.html',
                              context, context_instance=RequestContext(request))


@login_required
@permission_required('users.human_resources')
def hr_application_remove(request, app_id):
    if HRApplication.objects.filter(id=app_id).exists():
        application = HRApplication.objects.get(id=app_id)
        if application:
            application.delete()

    return HttpResponseRedirect(reverse('auth_hrapplications_view'))


@login_required
@permission_required('users.human_resources')
def hr_application_approve(request, app_id):
    if HRApplication.objects.filter(id=app_id).exists():
        application = HRApplication.objects.get(id=app_id)
        application.approved_denied = True
        application.reviewer_user = request.user
        application.reviewer_character = request.user.main_char
        application.save()

    return HttpResponseRedirect(reverse('auth_hrapplications_view'))


@login_required
@permission_required('users.human_resources')
def hr_application_reject(request, app_id):
    if HRApplication.objects.filter(id=app_id).exists():
        application = HRApplication.objects.get(id=app_id)
        application.approved_denied = False
        application.reviewer_user = request.user
        application.reviewer_character = request.user.main_char
        application.save()

    return HttpResponseRedirect(reverse('auth_hrapplications_view'))


@login_required
@permission_required('users.human_resources')
def hr_application_search(request):
    if request.method == 'POST' and request.POST['search_string']:
        # print request.POST
        form = HRApplicationSearchForm(request.POST)
        if form.is_valid():
            # Really dumb search and only checks character name
            # This can be improved but it does the job for now
            searchstring = form.cleaned_data['search_string']
            applications = []
            results = HRApplication.objects.filter(character_name__icontains=searchstring)
            if results:
                for application in results:
                #if searchstring in application.character_name:
                    applications.append(application)
            else:
                return HttpResponseRedirect(reverse('auth_hrapplications_view'))

            context = {'applications': applications, 'search_form': HRApplicationSearchForm()}

            return render_to_response('dashboard/hrapplications/hrapplicationsearchview.html',
                                      context, context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect(reverse('auth_hrapplications_view'))


@login_required
@permission_required('users.human_resources')
def hr_application_mark_in_progress(request, app_id):
    # print reverse("auth_hrapplication_view", args=[app_id,])
    if HRApplication.objects.filter(id=app_id).exists():
        application = HRApplication.objects.get(id=app_id)
        application.reviewer_inprogress_character = request.user.main_char
        application.save()

    return HttpResponseRedirect(reverse("auth_hrapplication_view", args=[app_id,]))
