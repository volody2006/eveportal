# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HRApplication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_a_spi', models.CharField(default=b'', max_length=254)),
                ('about', models.TextField(default=b'')),
                ('extra', models.TextField(default=b'')),
                ('approved_denied', models.NullBooleanField()),
                ('is_activ', models.BooleanField(default=True)),
                ('api', models.ForeignKey(to='eveonline.ApiKeyPair')),
                ('character', models.ForeignKey(related_name='hr_character', to='eveonline.Character')),
                ('corp', models.ForeignKey(to='eveonline.Corporation')),
                ('reviewer_character', models.ForeignKey(related_name='hr_reviewer_character', blank=True, to='eveonline.Character', null=True)),
                ('reviewer_inprogress_character', models.ForeignKey(related_name='inprogress_character', blank=True, to='eveonline.Character', null=True)),
            ],
        ),
    ]
