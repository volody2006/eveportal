# -*- coding: utf-8 -*-
from django import forms

from eveonline.eve_api_manager import EveApiManager
from eveonline.managers import EveManager
from eveonline.models import Corporation, Alliance
from django.utils.translation import ugettext as _
from eveportal import settings


class HRApplicationForm(forms.Form):
    allchoices = []
    try:
        alliance = Alliance.objects.get(pk=settings.ALLIANCE_ID)
    except:
        # Если начальная установка при миграции выеживается, как починить, пока не знаю
        try:
            evemanager = EveManager()
            alliance = evemanager.update_or_create_alliance(settings.ALLIANCE_ID)
        except:
            alliance = None


    character_name = forms.CharField(max_length=254, required=True, label=_("Main Character Name"))
    full_api_id = forms.CharField(max_length=254, required=True, label=_("API ID"))
    full_api_key = forms.CharField(max_length=254, required=True, label=_("API Verification Code"))

    corp = forms.ModelChoiceField(queryset=Corporation.objects.filter(alliance = alliance), required=True, label=_("Corporation"))

    is_a_spi = forms.ChoiceField(choices=[('Yes', _('Yes')), ('No', _('No'))], required=True, label=_('Are you a spy?'))
    about = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '3'}), required=False, label=_("About You"))
    extra = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '3'}), required=False, label=_("Extra Application Info"))

    def clean(self):

        try:
            # Проверяем, что это ключ, а не набор случайных данных
            if not EveApiManager.check_api_is_valid(self.cleaned_data['full_api_id'],
                                                    self.cleaned_data['full_api_key']):

                raise forms.ValidationError(_(u'API key is invalid. Please make another.'))
        except:
            raise forms.ValidationError(_(u'API key is invalid. Please make another.'))

        # Проверяем тип ключа
        if not EveApiManager.check_api_is_type_account(self.cleaned_data['full_api_id'],
                                                       self.cleaned_data['full_api_key']):
            raise forms.ValidationError(_(u'API not of type account'))

        # Проверяем, что ключ имеет допустимую маску
        if not EveApiManager.check_api_is_full(self.cleaned_data['full_api_id'],
                                               self.cleaned_data['full_api_key']):
            raise forms.ValidationError(_(u'API supplied is not a full api key'))
        chars = EveApiManager.get_characters_from_api(api_id = self.cleaned_data['full_api_id'],
                                                      api_key = self.cleaned_data['full_api_key'])

        valid_name = False
        for char in chars:
            if self.cleaned_data['character_name'] == chars[char]['name']:
                valid_name = True
               # print True
        if not valid_name:
            raise forms.ValidationError(_(u'Enter the character for that provide API key. Register writing is important.'))
        # raise forms.ValidationError(_(u'test'))
        return self.cleaned_data

class HRApplicationNotUserForm(HRApplicationForm):
    email = forms.EmailField(required=True, label=_('email'))


class HRApplicationSearchForm(forms.Form):
    search_string = forms.CharField(max_length=254, required=True, label=_("Search String"))
