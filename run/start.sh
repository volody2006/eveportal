#!/usr/bin/env bash
python manage.py celeryd --verbosity=2 --loglevel=INFO &
python manage.py celerybeat --verbosity=2 --loglevel=INFO &
