"""
By importing all of these sub-modules, the models package is transparently
accessible by the rest of the project. This makes it act just as if it were
one monolithic models.py.
"""
from eve_db.models.system import *
from eve_db.models.inventory import *
from eve_db.models.map import *
from eve_db.models.chr import *
from eve_db.models.npc import *
from eve_db.models.station import *
from eve_db.models.planet import *