# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0010_auto_20160119_1456'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pricelistfile',
            name='file',
            field=models.FileField(upload_to=b'/home/user/program/MyProject/eveportal/media', null=True, verbose_name='\u0444\u0430\u0439\u043b', blank=True),
        ),
        migrations.AlterField(
            model_name='pricelistfile',
            name='title',
            field=models.CharField(help_text='Optional title to display. If not supplied, the filename will be used.', max_length=255, null=True, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='rentcontract',
            name='status',
            field=models.IntegerField(default=0, verbose_name='rent status', choices=[(0, 'unknown'), (1, '\u0423\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d'), (2, '\u041e\u0442\u043a\u043b\u043e\u043d\u0438\u0442\u044c'), (3, '\u0417\u0430\u043a\u0440\u044b\u0442\u043e'), (4, 'New'), (5, '\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0439')]),
        ),
    ]
