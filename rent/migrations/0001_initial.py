# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PriceListFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url_price', models.URLField(help_text='\u0430\u0434\u0440\u0435\u0441, \u043f\u043e \u043a\u043e\u0442\u043e\u0440\u043e\u043c\u0443 \u0434\u043e\u0441\u0442\u0443\u043f\u0435\u043d \u0444\u0430\u0439\u043b \u043f\u0440\u0430\u0439\u0441\u043b\u0438\u0441\u0442\u0430 \u0432 \u0444\u043e\u0440\u043c\u0430\u0442\u0435 YML', max_length=1024, verbose_name=b'URL')),
                ('run_auto', models.BooleanField(default=False)),
                ('all_free', models.BooleanField(default=False, help_text='\u0418\u0441\u0442\u0438\u043d\u0430, \u0435\u0441\u043b\u0438 \u0432 \u0444\u0430\u0439\u043b\u0435 \u0442\u043e\u043b\u044c\u043a\u043e \u0434\u043e\u0441\u0442\u0443\u043f\u043d\u044b\u0435 \u043f\u0440\u0435\u0434\u043b\u043e\u0436\u0435\u043d\u0438\u044f.')),
                ('row_name', models.IntegerField(default=1)),
                ('colum_system', models.CharField(default=b'', max_length=255, blank=True)),
                ('colum_price', models.CharField(default=b'', max_length=255, blank=True)),
                ('colum_status', models.CharField(default=b'', max_length=255, blank=True)),
                ('colum_status_free', models.CharField(default=b'', max_length=255, blank=True)),
                ('coefficient', models.IntegerField(default=1, help_text='\u041c\u043d\u043e\u0436\u0438\u0442\u0435\u043b\u044c \u0446\u0435\u043d\u044b')),
                ('md5_checksum', models.CharField(default=b'', help_text='\u043a\u043e\u043d\u0442\u0440\u043e\u043b\u044c\u043d\u0430\u044f \u0441\u0443\u043c\u043c\u0430 \u0441\u043a\u0430\u0447\u0430\u043d\u043d\u043e\u0433\u043e \u0444\u0430\u0439\u043b\u0430. \u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u0434\u043b\u044f \u0442\u043e\u0433\u043e, \u0447\u0442\u043e\u0431\u044b \u043d\u0435 \u0437\u0430\u043f\u0443\u0441\u043a\u0430\u0442\u044c \u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0443 \u043e\u0434\u0438\u043d\u0430\u043a\u043e\u0432\u044b\u0445 \u043f\u0440\u0430\u0439\u0441\u043b\u0438\u0441\u0442\u043e\u0432', max_length=255, editable=False, verbose_name=b'MD5')),
                ('file', models.FileField(upload_to=b'/home/user/program/MyProject/eveportal/media', null=True, verbose_name='file', blank=True)),
                ('title', models.CharField(help_text='Optional title to display. If not supplied, the filename will be used.', max_length=255, null=True, verbose_name='title', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='RentContract',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.BigIntegerField(default=0)),
                ('valid_ceo', models.BooleanField(default=False, verbose_name='the application approved by the CEO of the Corporation')),
                ('status', models.IntegerField(default=0, verbose_name='rent status', choices=[(0, 'unknown'), (1, 'Approved'), (2, 'Reject'), (3, 'Close'), (4, 'New'), (5, 'Active')])),
                ('start_date', models.DateTimeField(null=True, verbose_name='The beginning of the contract', blank=True)),
                ('end_date', models.DateTimeField(null=True, verbose_name='The end of the contract', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('character', models.ForeignKey(blank=True, to='eveonline.Character', null=True)),
                ('rent_manager_character', models.ForeignKey(related_name='rent_contract_character', blank=True, to='eveonline.Character', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='RentPriceBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_base_price', models.BigIntegerField(default=0)),
                ('period', models.IntegerField(default=30, verbose_name='Type period', choices=[(1, 'day'), (7, 'week'), (30, 'montn'), (365, 'year')])),
                ('margin_for_security_level', models.BigIntegerField(default=0)),
                ('margin_for_deadlock', models.BigIntegerField(default=0)),
                ('margin_for_station', models.BigIntegerField(default=0)),
                ('margin_for_belts', models.BigIntegerField(default=0)),
                ('margin_for_icebelts', models.BigIntegerField(default=0)),
                ('margin_for_jump_bridge', models.BigIntegerField(default=0)),
                ('multiplier', models.IntegerField(default=1, verbose_name='prices are in', choices=[(1, 'ISK'), (1000, 'thousand ISK'), (1000000, 'million ISK'), (1000000000, 'billion ISK')])),
                ('alliance', models.ForeignKey(related_name='rent_price_base_alliance', to='eveonline.Alliance')),
            ],
        ),
        migrations.CreateModel(
            name='RentSolarSystem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rent_price', models.BigIntegerField(default=0)),
                ('free', models.BooleanField(default=False)),
                ('temp_status', models.TextField(default=b'')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('alliance', models.ForeignKey(related_name='rent_system_alliance', blank=True, to='eveonline.Alliance', null=True)),
                ('renter_alliance', models.ForeignKey(related_name='rent_system_renter_alliance', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Alliance', null=True)),
                ('renter_corp', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Corporation', null=True)),
                ('system', models.ForeignKey(blank=True, to='eveonline.SystemSovereignty', null=True)),
            ],
        ),
    ]
