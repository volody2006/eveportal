# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0008_auto_20160113_1323'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pricelistfile',
            name='file',
            field=models.FileField(upload_to=b'/home/volody/program/MyProject/eveportal/media', null=True, verbose_name='file', blank=True),
        ),
        migrations.AlterField(
            model_name='rentcontract',
            name='status',
            field=models.IntegerField(default=0, verbose_name='rent status', choices=[(0, 'unknown'), (1, 'Approved'), (2, 'Reject'), (3, 'Closed'), (4, 'New'), (5, 'Active')]),
        ),
    ]
