# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0002_auto_20151224_1905'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pricelistfile',
            name='title',
            field=models.CharField(help_text='Optional title to display. If not supplied, the filename will be used.', max_length=255, null=True, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
        ),
    ]
