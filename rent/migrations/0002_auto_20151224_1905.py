# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0002_auto_20151224_1905'),
        ('rent', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='rentcontract',
            name='rent_manager_user',
            field=models.ForeignKey(related_name='rent_contract_user', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='rentcontract',
            name='rent_system',
            field=models.ForeignKey(to='rent.RentSolarSystem'),
        ),
        migrations.AddField(
            model_name='rentcontract',
            name='renter_alliance',
            field=models.ForeignKey(related_name='rent_contract_alliance', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Alliance', null=True),
        ),
        migrations.AddField(
            model_name='rentcontract',
            name='renter_corp',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Corporation', null=True),
        ),
        migrations.AddField(
            model_name='rentcontract',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='pricelistfile',
            name='alliance',
            field=models.ForeignKey(related_name='rent_price_list_alliance', to='eveonline.Alliance'),
        ),
    ]
