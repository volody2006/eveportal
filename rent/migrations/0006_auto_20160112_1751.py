# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0005_auto_20160112_0603'),
    ]

    operations = [
        migrations.AddField(
            model_name='rentsolarsystem',
            name='free_contract',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='pricelistfile',
            name='file',
            field=models.FileField(upload_to=b'/home/user/program/MyProject/eveportal/media', null=True, verbose_name='file', blank=True),
        ),
        migrations.AlterField(
            model_name='rentcontract',
            name='status',
            field=models.IntegerField(default=0, verbose_name='rent status', choices=[(0, 'unknown'), (1, '\u0423\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d'), (2, '\u041e\u0442\u043a\u043b\u043e\u043d\u0438\u0442\u044c'), (3, '\u0417\u0430\u043a\u0440\u044b\u0442\u044c'), (4, 'New'), (5, 'Active')]),
        ),
    ]
