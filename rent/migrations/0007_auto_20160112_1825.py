# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0006_auto_20160112_1751'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rentcontract',
            options={'ordering': ('created',)},
        ),
    ]
