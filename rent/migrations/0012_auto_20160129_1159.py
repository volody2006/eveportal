# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0011_auto_20160122_2017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pricelistfile',
            name='file',
            field=models.FileField(upload_to=b'/home/user/program/MyProject/eveportal/media', null=True, verbose_name='file', blank=True),
        ),
        migrations.AlterField(
            model_name='pricelistfile',
            name='title',
            field=models.CharField(help_text='Optional title to display. If not supplied, the filename will be used.', max_length=255, null=True, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
        ),
        migrations.AlterField(
            model_name='rentcontract',
            name='status',
            field=models.IntegerField(default=0, verbose_name='rent status', choices=[(0, 'unknown'), (1, 'Approved'), (2, 'Reject'), (3, 'Closed'), (4, 'New'), (5, '\u0414\u0435\u0439\u0441\u0442\u0432\u0438\u0435')]),
        ),
    ]
