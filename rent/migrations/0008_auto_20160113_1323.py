# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rent', '0007_auto_20160112_1825'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rentcontract',
            options={'ordering': ('-created',)},
        ),
        migrations.AlterField(
            model_name='rentcontract',
            name='status',
            field=models.IntegerField(default=0, verbose_name='rent status', choices=[(0, 'unknown'), (1, '\u0423\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d'), (2, '\u041e\u0442\u043a\u043b\u043e\u043d\u0438\u0442\u044c'), (3, 'Closed'), (4, 'New'), (5, 'Active')]),
        ),
    ]
