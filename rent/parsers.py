# -*- coding: utf-8 -*-

import logging
from eve_db.models import MapSolarSystem
from eveonline.managers import EveManager
from eveonline.models import SystemSovereignty, Alliance, ApiKeyPair
from rent.models import RentSolarSystem
import csv

from users.models import CustomUser

logger = logging.getLogger(__name__)

# Solar Citizens- System Rental Prices
url = 'https://docs.google.com/spreadsheets/d/1g7aANUvDEnmsWATXDWdjgnxbRYNcmT85wFoswaf7wsE/export?format=csv&id=1g7aANUvDEnmsWATXDWdjgnxbRYNcmT85wFoswaf7wsE&gid=0'
#url = 'https://docs.google.com/spreadsheets/d/1g7aANUvDEnmsWATXDWdjgnxbRYNcmT85wFoswaf7wsE/export?format=tsv&id=1g7aANUvDEnmsWATXDWdjgnxbRYNcmT85wFoswaf7wsE&gid=0'

def parse_cvs_file(file, col= 1):
    data=csv.reader(open(file))
    i = 1
    fields = data.next()
    while i < col:
        fields = data.next()
        i=i+1
    list = []
    for row in data:
        # Zip together the field names and values
        items = zip(fields, row)
        item = {}
        # Add the value to our dictionary
        for (name, value) in items:
            item[name] = value.strip()
        list.append(item)
    return list

def inp_user():
    file = 'auth_user.csv'
    spisok = parse_cvs_file(file)
    for i in spisok:
        print i
        try:
            user, cread = CustomUser.objects.update_or_create(
                pk = int(i['id']),
                defaults=dict(
                    username = i['username'],
                    email = i['email'],
                    is_staff = False,
                    is_active = True,
                    password = i['password'],
                    is_superuser = False
                )
            )
        except:
            user = CustomUser.objects.create_user(username=i['username'], pk=int(i['id']),
                                                  email = ('%s-%s' % (i['id'], i['email'] )
                                                           )
                                                  )
            user, cread = CustomUser.objects.update_or_create(
                pk = int(i['id']),
                defaults=dict(
                    username = i['username'],
                #    email = i['email'],
                    is_staff = False,
                    is_active = True,
                    password = i['password'],
                    is_superuser = False
                )
            )
        print user
def inp_api():
    file = 'eveonline_eveapikeypair.csv'
    evemanager = EveManager()
    spisok = parse_cvs_file(file)
    for i in spisok:
        print i
        api_id = i['api_id']
        api_key = i['api_key']
        user = CustomUser.objects.get(pk = int(i['user_id']))
        evemanager.create_api_keypair(api_id, api_key, user)

def update_rent(file, price, status, system, status_free, alliance_id, kkk=1, **kwargs):

    for i in parse_cvs_file(file, **kwargs):
        print i
        try:

            system_map = MapSolarSystem.objects.get(name= i[system])
            free = False
            if i[status] == status_free:
                try:
                    if int(alliance_id) == int(SystemSovereignty.objects.get(pk=system_map.pk).alliance.alliance_id):
                        print SystemSovereignty.objects.get(pk=system_map.pk).alliance
                        free = True
                except:
                    pass


            rent_system , creade = RentSolarSystem.objects.update_or_create(system=SystemSovereignty.objects.get(pk=system_map.pk),
                                                                            alliance = Alliance.objects.get(alliance_id=alliance_id),
                                                                            defaults=dict(
                                                                                rent_price = float(i[price].replace(',', '.'))*kkk,
                                                                                temp_status = i[status],
                                                                                free =free
                                                                            ))

        except:
            pass

def update_rent_all(file, price, system, alliance_id, kkk=1, **kwargs):
    RentSolarSystem.objects.filter(alliance = Alliance.objects.get(alliance_id=alliance_id),
                                   renter_corp = None).delete()
    for i in parse_cvs_file(file, **kwargs):
        print i
        try:

            system_map = MapSolarSystem.objects.get(name= i[system])
            free = False

            if int(alliance_id) == int(SystemSovereignty.objects.get(pk=system_map.pk).alliance.alliance_id):
                print SystemSovereignty.objects.get(pk=system_map.pk).alliance
                free = True


            rent_system , creade = RentSolarSystem.objects.update_or_create(system=SystemSovereignty.objects.get(pk=system_map.pk),
                                                                            alliance = Alliance.objects.get(alliance_id=alliance_id),
                                                                            defaults=dict(
                                                                                rent_price = float(i[price].replace(',', '.'))*kkk,
                                                                                free = free
                                                                            ))

        except:
            pass


def solar():
    file = 'Solar Citizens- System Rental Prices - All.csv'
    price ='price'
    status = 'status'
    system = 'system'
    alliance_id=1208295500
    status_free='\xd1\x81\xd0\xb2\xd0\xbe\xd0\xb1\xd0\xbe\xd0\xb4\xd0\xbd\xd0\xb0'
    kkk=1000*1000*1000
    update_rent(file, price, status, system, status_free, alliance_id, kkk)


def triumvirate():
    url = 'https://docs.google.com/spreadsheets/d/19f3LblRmK9MYyuPNoomZg62s7s5qEz7yq21dB-T4X5A/export?format=csv&id=19f3LblRmK9MYyuPNoomZg62s7s5qEz7yq21dB-T4X5A&gid=1813038360'
    file = 'Cohortes Triarii Rental Portal - Etherium Reach.csv'
    price ='Total'
    status = 'Available'
    system = 'SolarSystem'
    update_rent(file, price, status, system, status_free='Yes', alliance_id=933731581, kkk=1000*1000*1000)
    url = 'https://docs.google.com/spreadsheets/d/19f3LblRmK9MYyuPNoomZg62s7s5qEz7yq21dB-T4X5A/export?format=csv&id=19f3LblRmK9MYyuPNoomZg62s7s5qEz7yq21dB-T4X5A&gid=1289581964'
    file = 'Cohortes Triarii Rental Portal - The Spire.csv'
    update_rent(file, price, status, system, status_free='YES', alliance_id=933731581, kkk=1000*1000*1000)

def bot():
    url = 'https://docs.google.com/spreadsheets/d/1zT9j1cEKkrf81uF1BbyUceiH8daK9GF10XJWUCpDA_M/export?format=csv&id=1zT9j1cEKkrf81uF1BbyUceiH8daK9GF10XJWUCpDA_M&gid=0'
    file = 'BOT RENTALS - Sheet1.csv'
    price ='PRICE BEFORE SOV FEES'
    status = 'AVAILABILITY'
    system = 'SYSTEM'
    update_rent(file, price, status, system, status_free='YES', alliance_id=99003006, kkk=1000*1000*1000)


def soviet_union():
    url = ['Soviet-Union RENT LIST - Feythabolis.csv',
           'https://docs.google.com/spreadsheets/d/1mTX1lzQsI5JdeebH3Tz7JVdWCR_3cdhyx3l55sOAnGU/export?format=csv&id=1mTX1lzQsI5JdeebH3Tz7JVdWCR_3cdhyx3l55sOAnGU&gid=1371565863',
           ]
    files = ['Soviet-Union RENT LIST - Feythabolis.csv',
             'Soviet-Union RENT LIST - Impass.csv',
             'Soviet-Union RENT LIST - Esoteria.csv',
            ]
    price ='PRICE IN BILLIONS'
    status = 'AVAILABLE'
    system = 'System'
    alliance_id=99004779
    status_free='YES'
    kkk=1000*1000*1000
    for file in files:
        update_rent(file, price, status, system, status_free, alliance_id, kkk)


def imfamous():
    url ='https://docs.google.com/spreadsheets/d/1B7A0LX8WsZAjhr2l86l3OxmH9D99gPIjaKqgO3aX5fY/edit#gid=0'
    url ='https://docs.google.com/spreadsheets/d/1B7A0LX8WsZAjhr2l86l3OxmH9D99gPIjaKqgO3aX5fY/export?format=csv&id=1B7A0LX8WsZAjhr2l86l3OxmH9D99gPIjaKqgO3aX5fY&gid=0'
    file = 'Partner systems - Sheet1.csv'
    price ='Membership Fee'
    system = 'System'
    update_rent_all(file, price, system, alliance_id=99005677, kkk=1000*1000*1000, col = 5)

def ocer():
    url = 'https://docs.google.com/spreadsheets/d/1ed58LvbVECrFD_qGkrJQPGgdhIUZm68BiHy4JnF5RuQ/edit#gid=48904413'
    ali = 'http://evemaps.dotlan.net/alliance/Kids_With_Guns_Alliance'