# -*- coding: utf-8 -*-
from rent.models import *
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

# class BookAdmin(admin.ModelAdmin):
#     list_filter = (
#         ('author', admin.RelatedOnlyFieldListFilter),
#     )
class RentPriceBaseAdmin(admin.ModelAdmin):
    list_display = ('alliance', 'period', )
    list_filter = ('period', 'alliance', )


admin.site.register(RentPriceBase, RentPriceBaseAdmin)

class PriceListFileAdmin(admin.ModelAdmin):
    list_display = ('pk', 'alliance', 'run_auto', 'all_free')
    list_filter = ('run_auto', 'all_free', ('alliance',  admin.RelatedOnlyFieldListFilter), )
    raw_id_fields = ('alliance',)

    def controls(self, f):
        return '<a href="./%s/run/" class="btn btn-success">Запустить</a>' % f.pk
    controls.short_description = 'Управление'
    controls.allow_tags = True


admin.site.register(PriceListFile, PriceListFileAdmin)

class RentSolarSystemAdmin(admin.ModelAdmin):
    list_display = ('alliance', 'system', 'rent_price',
                    'free', 'renter_corp', 'temp_status', 'update')
    list_filter =  ('free', ('alliance',  admin.RelatedOnlyFieldListFilter), )
    search_fields = ['alliance__alliance_name',
                     'renter_corp__corporation_name',
                     'renter_corp__corporation_id',
                     'alliance__alliance_id']

admin.site.register(RentSolarSystem, RentSolarSystemAdmin)


    # list_display = ('character_id', 'character_name', 'corporation',
    #                 'alliance', 'user' )
    # list_filter = ('is_npc', 'is_del', 'is_citizen')
    # search_fields = ['character_name', 'alliance__alliance_name', 'user__username',
    #                  'corporation__corporation_name', 'character_id', 'api__api_id',
    #                  'user__email', 'character_id', 'corporation__corporation_id',
    #                  'alliance__alliance_id']
    # raw_id_fields = ('api',)

class RentContractAdmin(admin.ModelAdmin):
    list_display = ('rent_system', 'price', 'renter_corp',
                    'renter_alliance', 'user', 'character', 'valid_ceo',
                    'status', )
    list_filter =  ('status', )
    raw_id_fields = ('character', 'renter_corp', 'rent_manager_character',
                     'rent_manager_user')
    # search_fields = ['alliance__alliance_name',
    #                  'renter_corpn__corporation_name',
    #                  'renter_corp__corporation_id',
    #                  'alliance__alliance_id']

admin.site.register(RentContract, RentContractAdmin)
