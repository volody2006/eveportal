# -*- coding: utf-8 -*-
from accounts.security import can_manage_corporate_account
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.utils.translation import ugettext as _
from evebank.managers import pain_obligation, permission_corp_account
from evebank.models import Obligations
from eveonline.models import SystemSovereignty, Alliance, Corporation, Character
from eveportal.settings import EVEBANK_CORP
from rent.models import RentSolarSystem, RentContract
from django.utils import timezone
from util.managers import date_end_month

# render_to_response('dashboard/evemail/mailbox.html', render_items, context_instance=RequestContext(request))

@login_required
def rent_home(request):
    system_lists = RentSolarSystem.objects.filter(free=True,
                                                  free_contract = True)
    render_items = {'system_list': system_lists,
                    'permissioncorpaccount': can_manage_corporate_account(request.user)}
    return render_to_response('dashboard/rent/rent_home.html', render_items, context_instance=RequestContext(request))





@login_required
def add_rent_system(request, system_id):
    # Персонажи из ПНС корпорации не могут арендовать систему
    if not request.user.main_char:
        return HttpResponseRedirect(reverse('auth_characters'))
    if request.user.main_char.corp.is_npc:
        return HttpResponseRedirect(reverse('rent_home'))
    valid_ceo = False
    system = get_object_or_404(SystemSovereignty, system=system_id)
    rent_system = get_object_or_404(RentSolarSystem, system=system, alliance=system.alliance)
    if not rent_system.free:
        return HttpResponseRedirect(reverse('rent_home'))
    user = request.user
    character = user.main_char
    if character.is_ceo:
        valid_ceo = True
    # Арендовать могут только пользователи с доступом к корп аккаунту.
    # Проверяем поле секонд юзер, если пользователя там нет шлем нахер
    corporation = user.main_char.corp
    if permission_corp_account(user, corporation):
        if not RentContract.objects.filter(rent_system=rent_system,
                                           renter_corp=character.corporation, end_date=None).exists():
            rent_cotract = RentContract.objects.create(rent_system=rent_system,
                                                       renter_corp=character.corporation,
                                                       price=rent_system.rent_price,
                                                       user=user,
                                                       character=character,
                                                       valid_ceo=valid_ceo,
                                                       status=4)
            rent_system.free_contract = False
            rent_system.save()
            rent_cotract = RentContract.objects.get(pk=rent_cotract.pk)

            # Создаем счет на следующий месяц
            source = user.main_char.corp_account()
            destination = Corporation.objects.get(pk=EVEBANK_CORP).get_account()
            if not source == destination:
                # Банк арендует систему? пока ничего не делаем.
                start_date = rent_cotract.created
                end_date = date_end_month(start_date)
                merchant_reference = ('%s - %s' % (start_date, end_date))

                obligation = Obligations.objects.create(source=source, destination=destination, amount=rent_cotract.price,
                                                        user=user,
                                                        merchant_reference=merchant_reference,
                                                        username=user.username,
                                                        contract=rent_cotract,
                                                        date_payment=(start_date + timezone.timedelta(days=3)))
                pain_obligation(obligation)

    return HttpResponseRedirect(reverse('contract_rent_system'))
    # render_to_response('dashboard/rent/rent_system.html', render_items, context_instance=RequestContext(request))


@login_required
def contract_rent_system(request):
    user = request.user
    if user.main_char:
        character = user.main_char
        render_items = {'rentcontract_list': RentContract.objects.filter(renter_corp=character.corporation),
                        'permissioncorpaccount': can_manage_corporate_account(request.user)}
        return render_to_response('dashboard/rent/rent_system.html', render_items,
                                  context_instance=RequestContext(request))
    return HttpResponseRedirect(reverse("auth_characters"))


@login_required
def close_rent_contract(request, rent_cotract_pk):
    rent_cotract = RentContract.objects.get(pk=rent_cotract_pk)
    # Проверяем, данный пользователь может закрыть контракт или нет. Закрыть может
    # или СЕО или тот кто его открыл
    user = request.user
    permis = False
    if user.main_char == rent_cotract.renter_corp.ceo:
        permis = True
    if user.main_char == rent_cotract.character and user == rent_cotract.user:
        permis = True
    if permis and rent_cotract.status in [0, 1, 4, 5]:
        rent_cotract.close(user=user)

    # render_items = {'rentcontract_list': RentContract.objects.filter(renter_corp = user.main_char.corporation),}
    # return render_to_response('dashboard/rent/rent_system.html', render_items, context_instance=RequestContext(request))
    return HttpResponseRedirect(reverse("contract_rent_system"))


@login_required
def obligations_view(request):
    user = request.user
    obligations = Obligations.objects.filter(user=user)
    render_items = {'obligations': obligations,
                    'permissioncorpaccount': can_manage_corporate_account(request.user)}
    return render_to_response('dashboard/rent/obligations_view.html', render_items,
                              context_instance=RequestContext(request))
