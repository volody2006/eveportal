# -*- coding: utf-8 -*-
import logging

from celerytask.managers import run_task
from eveportal import settings
from os import remove

from celery.schedules import crontab
from celery.task import task, periodic_task
from django.utils.translation import ugettext as _
from eve_db.models import MapSolarSystem
from eveonline.models import Alliance, SystemSovereignty
from rent.managers import Downloader
from rent.models import RentSolarSystem, PriceListFile
from rent.parsers import parse_cvs_file

logger = logging.getLogger(__name__)

@periodic_task(run_every=crontab(minute=59))
def run_import_feeds():
    name = ('%s:%s' % (__name__, 'run_import_feeds'))
    delta = 59*60
    if not run_task(key=name, delta=delta):
        for feed in PriceListFile.objects.filter(run_auto=True):
            logger.info('Start import file: %s' % feed.pk)
            download_feed.delay(feed)

@task
def download_feed(feed):
    name = ('%s:%s:%s' % (__name__, 'download_feed', feed.pk))
    delta = 59*60
    if not run_task(key=name, delta=delta):
        downloader = Downloader(feed)
        logger.info('Download import file: %s' % feed.pk)
        downloader.run()
        parse_feed.delay(feed, downloader.get_filename())


@task
def parse_feed(feed, filename):
    name = ('%s:%s:%s' % (__name__, 'parse_feed', feed.pk))
    delta = 59*60
    if not run_task(key=name, delta=delta):
        try:
            logger.info('Parse import file: %s' % feed.pk)
            all_update_rent(feed, filename)
            logger.info('[Feed=%s] Prepare to parse feed' % feed.pk)
        except:
            pass
        finally:
            remove(filename)

def all_update_rent(feed, filename):
    if feed.all_free:
        for system in RentSolarSystem.objects.filter(alliance = feed.alliance, renter_corp = None, free=True):
            system.free = False
            system.save()

    for i in parse_cvs_file(filename, feed.row_name):
        try:
            system_map = MapSolarSystem.objects.get(name= i[feed.colum_system])
            free = False
            if not feed.all_free:
                temp_status = i[feed.colum_status]
                if i[feed.colum_status].lower().strip() == feed.colum_status_free.encode('utf-8').lower().strip():
                    try:
                        if int(feed.alliance.pk) == int(SystemSovereignty.objects.get(pk=system_map.pk).alliance.alliance_id):
                            free = True
                    except:
                        pass
            else:
                free = True
                temp_status = ''
            rent_system , creade = \
                RentSolarSystem.objects.update_or_create(
                    system=SystemSovereignty.objects.get(pk=system_map.pk),
                    alliance = feed.alliance,
                    defaults=dict(
                        rent_price = float(i[feed.colum_price].replace(',', '.'))*feed.coefficient,
                        temp_status = temp_status,
                        free =free
                        )
                )
            logger.debug('alliance: %s, System %s, free: %s, creade: %s' % (rent_system.alliance,
                                                                           rent_system.system,
                                                                           rent_system.free, creade))
        except:
            pass
