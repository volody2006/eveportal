# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _

from django.conf.urls import url
from . import views


# url(r'^/alliance/(?P<alliance_id>\d+)/$', 'eveonline.views.alliance_info', name='alliance_info'),

urlpatterns = [
    url(r'^$', views.rent_home, name='rent_home'),
    url(r'^contract/$', views.contract_rent_system, name='contract_rent_system'),
    url(r'^system/(?P<system_id>\d+)/$', views.add_rent_system, name='add_rent_system'),
    url(r'^contract/close/(?P<rent_cotract_pk>\d+)/$', views.close_rent_contract, name='close_rent_contract'),
    url(r'^obligations/$', views.obligations_view, name='obligations_view'),
]