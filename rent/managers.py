# -*- coding: utf-8 -*-
import json
from urllib2 import URLError
from eveonline.models import SystemSovereignty
from eveportal import settings
from rent.models import RentPriceBase
import requests
import requests.exceptions

import gzip
import hashlib
import logging
import shutil

from requests.packages.urllib3 import HTTPConnectionPool
import os, itertools, random, string

logger = logging.getLogger('yml_robots')

def rand_key(length, stringset=string.ascii_letters+string.digits):
    '''
    Returns a string with `length` characters chosen from `stringset`
    >>> len(generate_random_string(20) == 20
    '''
    return ''.join([stringset[i%len(stringset)] \
        for i in [ord(x) for x in os.urandom(length)]])

class Downloader():
    """
    Робот. отвечающий за скачивание файла
    """
    chunk_size = 16 * 1024
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.22 (KHTML, like Gecko) ' + \
                 'Chrome/25.0.1364.172 Safari/537.22'

    def __init__(self, feed):
        self.gzipped = False
        self.fp = None
        self.feed = feed
        self.checksum = hashlib.md5()
        self.had_changed = False
        self.filename = self.generate_tmp_filename()

    def run(self):
        """
        Точка входа
        """
        self.save_anyway()

    def feed_had_changed(self):
        """
        Возвращает True, если по сравнению с прошлой версией скачанного прайслиста
        произошли какие-то изменения (не совпадает контрольная сумма)
        """
        return self.had_changed


    def get_filename(self):
        """
        Возвращает имя временного файла, под которым сохраняется скачиваемый.
        """
        return self.filename

    def generate_tmp_filename(self):
        """
        Генерирует имя файла, под которым будет сохранён прайс.
        """
        return os.path.join(settings.MEDIA_ROOT, '%s.csv' % rand_key(32))

    def get_auth(self):
        """
        Определяет реквизиты (username и password) для доступа
        """
        auth = None

        # if self.feed.credentials:
        #     username, password = self.feed.credentials.strip().split(':', 1)
        #
        #     if username and password:
        #         auth = (username, password)
        #         logger.info('Appends credentials: %s:%s' % auth)
        return auth

    def check_for_changes(self):
        """
        Проверяет, совпадает ли контрольная сумма (md5) скачанного файла с
        контрольной суммой этого фида, записанного во время последнего импорта.

        Если произошли изменения, выставляется флаг "есть изменения".
        """
        current_md5checksum = self.checksum.hexdigest()

        if self.feed.md5_checksum != current_md5checksum:
            logger.info('Checksum has changed: was %s, became %s' % (self.feed.md5_checksum, current_md5checksum))
            self.had_changed = True
            self.feed.md5_checksum=current_md5checksum
            self.feed.save()
        else:
            logger.info('Checksum the same, no more processes needed')
            #raise NoUpdateRequired()

    def save_anyway(self):
        """
        Выполняет безусловное скачивание файла
        """
        try:
            src = requests.get(url=self.feed.url_price, stream=True,
                               headers={'User-Agent': self.user_agent},
                               auth=self.get_auth())
            self.filesize = int(src.headers.get('content-length') or 0)
            self.gzipped = 'gzip' in (src.headers.get('content-encoding') or '')
            bytes = 0
            with open(self.filename, 'wb') as dst:
                while True:
                    buf = src.raw.read(self.chunk_size)
                    if not buf:
                        break
                    bytes += len(buf)
                    dst.write(buf)
                    self.checksum.update(buf)
            if self.gzipped:
                self.ungzip_file()
            self.check_for_changes()

        except (requests.exceptions.ConnectionError, HTTPConnectionPool) as e:
            logger.info('HTTP error: %s' % str(e.args[0]))
            raise ('Unable connect with host: %s' % e)
        except URLError as e:
            logger.info('url error [%e]' % e)
            raise ('Cannot download file: remote resource not found')

    def ungzip_file(self):
        """
        Распаковывает поток сжатых GZIP'ом данных
        """
        logger.info('YML looks like gzip file. Trying to ungzip it')
        ungzip_filename = self.filename + '.ungzip.yml'

        with gzip.open(self.filename, 'rb') as src, open(ungzip_filename, 'w') as dst:
            shutil.copyfileobj(src, dst)
        os.rename(ungzip_filename, self.filename)


class RentsManagers:

    def __init__(self):
        pass

    @staticmethod
    def get_price(system_id, alliance_id):
        system = SystemSovereignty.objects.get(system = system_id)
        base_price = RentPriceBase.objects.get(alliance = alliance_id)


# import gspread
# from oauth2client.client import SignedJwtAssertionCredentials


# json_key = json.load(open('eveportal-6c8c406b7174.json'))
# scope = ['https://spreadsheets.google.com/feeds']
# credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode(), scope)
# gc = gspread.authorize(credentials)
#
# wks = gc.open("Where is the money Lebowski?").sheet1
