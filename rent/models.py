# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from eve_db.models import MapSolarSystem
from eveonline.models import SystemSovereignty, Alliance, Corporation, Character
from django.utils.translation import ugettext as _
from eveportal import settings
import os
from users.models import CustomUser
from util.managers import date_end_month

__author__ = 'volody'


class RentSolarSystem(models.Model):

    system = models.ForeignKey(SystemSovereignty, blank=True, null=True)
    alliance = models.ForeignKey(Alliance, blank=True, null=True, related_name='rent_system_alliance')
    rent_price = models.BigIntegerField(default=0)
    free = models.BooleanField(default=False)
    free_contract = models.BooleanField(default=True)
    renter_corp = models.ForeignKey(Corporation, blank=True, null=True, on_delete=models.SET_NULL)
    renter_alliance = models.ForeignKey(Alliance, blank=True, null=True, on_delete=models.SET_NULL,
                                        related_name='rent_system_renter_alliance')
    temp_status = models.TextField(default='')
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)

    def __str__(self):
        return str(self.system.system.name)

    class Meta:
        unique_together = (('system', 'alliance'),)

class RentContract(models.Model):
    RENT_STATUS =(
        (0, _('unknown')),
        (1, _('Approved')), # Заявка на контракт утверждена
        (2, _('Reject')),   # Контракт отклонен
        (3, _('Closed')),    # Контракт закрыт
        (4, _('New')),       # Новый контракт, еще не заключен
        (5, _('Active')),   # Контракт активный
    )
    rent_system = models.ForeignKey(RentSolarSystem)
    price = models.BigIntegerField(default=0)

    renter_corp = models.ForeignKey(Corporation, blank=True, null=True, on_delete=models.SET_NULL)
    renter_alliance = models.ForeignKey(Alliance, blank=True, null=True, on_delete=models.SET_NULL,
                                        related_name='rent_contract_alliance')
    user = models.ForeignKey(CustomUser, blank=True, null=True)
    character = models.ForeignKey(Character, blank=True, null=True)
    valid_ceo = models.BooleanField(default=False,
                                    verbose_name=_('the application approved by the CEO of the Corporation'))

    status = models.IntegerField(choices=RENT_STATUS, default=0,
                                 verbose_name=_('rent status'))

    rent_manager_user = models.ForeignKey(CustomUser, blank=True, null=True, related_name="rent_contract_user")
    rent_manager_character = models.ForeignKey(Character, blank=True, null=True, related_name="rent_contract_character")

    start_date = models.DateTimeField(verbose_name=_('The beginning of the contract'), blank=True, null=True)
    end_date   = models.DateTimeField(verbose_name=_('The end of the contract'), blank=True, null=True)

    created = models.DateTimeField(verbose_name='Created Date/Time', auto_now_add=True)
    update = models.DateTimeField(verbose_name='Last Update Date/Time', auto_now=True)


    def __unicode__(self):
        return ('%s %s' % (self.pk, self.rent_system))

    class Meta:
        ordering = ('-created', )

    def get_renter_account(self):
        return self.renter_corp.account()


    def rent_active(self, rent_manager_user, rent_manager_character,
                    start_date=None, end_date=None, **kwargs):
        self.rent_manager_user = rent_manager_user
        self.rent_manager_character = rent_manager_user.main_char
        self.status = 5 # Active

        if start_date:
            self.start_date= start_date
        else:
            self.start_date = timezone.now()

        if end_date and (end_date > self.start_date):
            self.end_date = end_date
        else:
            pass
            #raise "End date invalid"

        self.save()
        return self

    def close(self, user= None):
        if self.status == 3:
            return self
        # Закрытие в конце месяце
        self.end_date = date_end_month(timezone.now())
        self.status = 3
        self.character = user.main_char
        self.user = user
        self.save()
        rent_system = self.rent_system
        rent_system.free_contract = True
        rent_system.save()
        return self

    def close_now(self, user= None):
        if self.status == 3:
            return self
        # Немедленное закрытие
        self.end_date = timezone.now()
        self.status = 3
        if user:
            self.character = user.main_char
        self.user = user
        self.save()
        rent_system = self.rent_system
        rent_system.free_contract = True
        rent_system.save()
        return self



# В этом классе описываем формулу расчета цены для системы
#
#
DAY = 1
WEEK = 7
MONTH = 30
YEAR = 365

PERIOD_TYPE = (
    (DAY, u'day'),
    (WEEK, u'week'),
    (MONTH, u'montn'),
    (YEAR, u'year'),
)

ISK = 1
KISK = ISK * 1000
KKISK = KISK * 1000
KKKISK = KKISK * 1000

UNITS = (
    (ISK, u'ISK'),
    (KISK, u'thousand ISK'),
    (KKISK, u'million ISK'),
    (KKKISK, u'billion ISK'),
)

class RentPriceBase(models.Model):
    alliance = models.ForeignKey(Alliance, related_name= 'rent_price_base_alliance')
    start_base_price = models.BigIntegerField(default=0)
    period = models.IntegerField(choices=PERIOD_TYPE, default=MONTH, verbose_name=u'Type period')
    margin_for_security_level = models.BigIntegerField(default=0)
    margin_for_deadlock = models.BigIntegerField(default=0)
    margin_for_station = models.BigIntegerField(default=0)
    margin_for_belts = models.BigIntegerField(default=0)
    margin_for_icebelts = models.BigIntegerField(default=0)
    margin_for_jump_bridge = models.BigIntegerField(default=0)
    multiplier = models.IntegerField(choices=UNITS, default=ISK, verbose_name=u'prices are in')



    def __str__(self):
        return self.alliance.alliance_name

# class PriceListStatus(models.Model):
#     alliance = models.ForeignKey(Alliance, related_name= 'rent_price_list_status_alliance')
#     status = models.TextField(default='')
#     is_free = models.BooleanField(default=False)


class PriceListFile(models.Model):
    alliance = models.ForeignKey(Alliance, related_name= 'rent_price_list_alliance')
    url_price = models.URLField('URL', max_length=1024,
                                help_text=u'адрес, по которому доступен файл прайслиста в формате YML')
    run_auto = models.BooleanField(default=False)
    all_free = models.BooleanField(default=False, help_text=u'Истина, если в файле только доступные предложения.')
    row_name = models.IntegerField(default=1)
    colum_system = models.CharField(max_length=255,
                                    blank=True, default='')
    colum_price = models.CharField(max_length=255,
                                   blank=True, default='')
    colum_status = models.CharField(max_length=255,
                                    blank=True, default='')
    colum_status_free = models.CharField(max_length=255,
                                         blank=True, default='')
    coefficient = models.IntegerField(default=1, help_text=u'Множитель цены')
    md5_checksum = models.CharField('MD5', max_length=255,
                                    editable=False,
                                    default='',
                                    help_text=(u'контрольная сумма скачанного файла. Используется для '
                                               u'того, чтобы не запускать обработку одинаковых прайслистов'))



    file = models.FileField(_("file"), upload_to=settings.MEDIA_ROOT, null=True, blank=True,)
    title = models.CharField(
        _("title"), max_length=255, null=True, blank=True,
        help_text=_("Optional title to display. If not supplied, the filename "
                    "will be used."))



    def __str__(self):
        if self.title:
            return self.title
        elif self.file:
            # added if, because it raised attribute error when
            # file wasn't defined
            return self.get_file_name()
        return "<empty>"



    def get_file_name(self):
        return os.path.basename(self.file.name)

    def get_ext(self):
        return os.path.splitext(self.get_file_name())[1][1:].lower()


