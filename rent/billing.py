# -*- coding: utf-8 -*-
import logging
from django.utils.translation import ugettext as _
from decimal import Decimal as D
from accounts import models, exceptions, facade


logger = logging.getLogger(__name__)

def submit(contract, order_total):
    # Take payment first
    transfer = facade.transfer(contract.get_user_account(),
                               contract.get_merchant_account(),
                               order_total)
    # Create order models
    try:
        contract.place_order()
    except Exception, e:
        # Something went wrong placing the order.  Roll-back the previous
        # transfer
        facade.reverse(transfer)




def redeem(order_number, user, amount):
    # Get user's non-empty accounts ordered with the first to expire first
    accounts = models.Account.active.filter(
        user=user, balance__gt=0).order_by('end_date')

    # Build up a list of potential transfers that cover the requested amount
    transfers = []
    amount_to_allocate = amount
    for account in accounts:
        to_transfer = min(account.balance, amount_to_allocate)
        transfers.append((account, to_transfer))
        amount_to_allocate -= to_transfer
        if amount_to_allocate == D('0.00'):
            break
    if amount_to_allocate > D('0.00'):
        raise exceptions.InsufficientFunds()

    # Execute transfers to some 'Sales' account
    destination = models.Account.objects.get(name="Sales")
    completed_transfers = []
    try:
        for account, amount in transfers:
            transfer = facade.transfer(
                account, destination, amount, user=user,
                description="Order %s" % order_number)
            completed_transfers.append(transfer)
    except exceptions.AccountException, transfer_exc:
        # Something went wrong with one of the transfers (possibly a race condition).
        # We try and roll back all completed ones to get us back to a clean state.
        try:
            for transfer in completed_transfers:
                facade.reverse(transfer)
        except Exception, reverse_exc:
            # Uh oh: No man's land.  We could be left with a partial redemption. This will
            # require an admin to intervene.  Make sure your logger mails admins on error.
            logger.error("Order %s, transfers failed (%s) and reverse failed (%s)",
                         order_number, transfer_exc, reverse_exc)
            logger.exception(reverse_exc)

        # Raise an exception so that your client code can inform the user appropriately.
        # raise RedemptionFailed()
    else:
        # All transfers completed ok
        return completed_transfers
