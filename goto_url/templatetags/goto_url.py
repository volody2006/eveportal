from django.template import Library
from .. import utils

register = Library()


@register.simple_tag
def goto_url(url):
    '''
    Wraps external links in base64 and relocate on the special View, later redirect to source URL

    Usage::

        {% load goto_url %}
        {% goto_url comment.user_url %}
        {% goto_url "http://adw0rd.com/" %}

    Args:
        url: http://adw0rd.com/

    Returns:
        /goto/aHR0cDovL2FkdzByZC5jb20v

    '''
    return utils.goto_url(url)
