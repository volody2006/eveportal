from django.conf.urls import patterns, url

from views import GotoView

urlpatterns = [
    url(r'^goto/(.+)$', GotoView.as_view()),

]

