# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from django.conf.urls import url
from . import views




urlpatterns = [
     # Eveonline Urls
    #url(r'^add_api_key/', views.add_api_key, name='add_api_key'),
    url(r'^add_api_key/', views.AddApiKey.as_view(), name='add_api_key'),
    url(r'^api_key_management/', views.ViewApiKeyManagement.as_view(),
    #url(r'^api_key_management/', views.api_key_management_view,
        name='api_key_management'),
    url(r'^delete_api_pair/(\w+)/$', views.api_key_removal, name='api_key_removal'),
    url(r'^characters/', views.characters_view, name='auth_characters'),
    url(r'^main_character_change/(\w+)/$', views.main_character_change, name='main_character_change'),
    # url(r'^/corporation_stats/$', views.corp_stats_view', name='auth_corp_stats'),
]