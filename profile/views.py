# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.views.generic import View, ListView
from eveonline.eve_api_manager import EveApiManager
from eveonline.managers import EveManager
from eveonline.models import Character, ApiKeyPair
from eveportal import settings
from profile.forms import UpdateKeyForm
from users.models import CustomUser
from eveonline.tasks import task_run_create_api_keypair

from django.contrib.auth.decorators import login_required

class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)



class AddApiKey(LoginRequiredMixin, View):
    form_class = UpdateKeyForm
    template_name = 'dashboard/eveonline/addapikey.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        context = {'form': form, 'apikeypairs': EveManager.get_api_key_pairs(request.user)}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, user=request.user)
        if form.is_valid():
            ApiKeyPair.objects.update_or_create(api_id=form.cleaned_data['api_id'],
                                                defaults=dict(api_key = form.cleaned_data['api_key'],
                                                              user = request.user))
            evemanager = EveManager()
            evemanager.create_api_keypair(form.cleaned_data['api_id'],
                                              form.cleaned_data['api_key'],
                                              request.user)
            task_run_create_api_keypair.delay(form.cleaned_data['api_id'],
                                              form.cleaned_data['api_key'],
                                              request.user)
            # <process form cleaned data>
            return HttpResponseRedirect(reverse("api_key_management"))

        context = {'form': form, 'apikeypairs': EveManager.get_api_key_pairs(request.user)}
        return render(request, self.template_name, context)



@login_required(login_url=settings.LOGIN_URL)
def add_api_key(request):
    if request.method == 'POST':
        form = UpdateKeyForm(request.POST)

        if form.is_valid():
            ApiKeyPair.objects.update_or_create(api_id=form.cleaned_data['api_id'],
                                                defaults=dict(api_key = form.cleaned_data['api_key'],
                                                              user = request.user))
            evemanager = EveManager()
            evemanager.create_api_keypair(form.cleaned_data['api_id'],
                                              form.cleaned_data['api_key'],
                                              request.user)
            task_run_create_api_keypair.delay(form.cleaned_data['api_id'],
                                              form.cleaned_data['api_key'],
                                              request.user)

            # Grab characters associated with the key pair
            # if EveApiManager.get_api_info(form.cleaned_data['api_id'],
            #                               form.cleaned_data['api_key'])['type'] != 'corp':
                #characters = EveApiManager.get_characters_from_api(form.cleaned_data['api_id'],
                #                                                   form.cleaned_data['api_key'])

            return HttpResponseRedirect(reverse("api_key_management"))
    else:
        form = UpdateKeyForm()
    context = {'form': form, 'apikeypairs': EveManager.get_api_key_pairs(request.user)}
    return render_to_response('dashboard/eveonline/addapikey.html', context,
                              context_instance=RequestContext(request))


class ViewApiKeyManagement(LoginRequiredMixin, ListView):
    context_object_name = 'book_list'
  #  queryset = EveManager.get_api_key_pairs(request.user)
    template_name = 'dashboard/eveonline/apikeymanagment.html'
    def get_queryset(self):
       # self.user = self.request.user
        return ApiKeyPair.objects.filter(user=self.request.user)



@login_required(login_url=settings.LOGIN_URL)
def api_key_management_view(request):
    context = {'apikeypairs': EveManager.get_api_key_pairs(request.user)}

    return render_to_response('dashboard/eveonline/apikeymanagment.html', context,
                              context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def api_key_removal(request, api_id):
    evemanager = EveManager()
    evemanager.delete_api_key_pair_user(api_id, request.user.id)
    return HttpResponseRedirect(reverse("api_key_management"))


@login_required
def characters_view(request):
    if Character.objects.filter(user = request.user).exists():
        render_items = {'characters': Character.objects.filter(user = request.user),
                        # 'authinfo': AuthServicesInfoManager.get_auth_service_info(request.user)  api = ApiKeyPair.objects.filter(user=request.user)
                        }
        return render_to_response('dashboard/eveonline/characters.html', render_items, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect(reverse("add_api_key"))
#


@login_required
def main_character_change(request, char_id):
    char = Character.objects.get(pk = char_id, user = request.user)
    user = CustomUser.objects.get(pk = request.user.pk)
    user.main_char = char
    user.save()
    return characters_view(request)


# @login_required
# @permission_required('auth.corp_stats')
# def corp_stats_view(request):
#    Если майн чар является сео то показываем, отправляем в хелп
#   Get the corp the member is in
    # auth_info = AuthServicesInfo.objects.get(user=request.user)
 #
  ## main_char = Character.objects.get(character_id=auth_info.main_char_id)
##   corp = Corporation.objects.get(corporation_id=main_char.corporation_id)
    # corp = Corporation.objects.get(corporation_id = auth_info.main_char_id.corporation_id)
    # if corp.ceo.character_id == auth_info.main_char_id.character_id:
    #     print 'CTJ'
    # current_count = 0
    # allcharacters = {}
##   all_characters = Character.objects.all()
    # all_characters = Character.objects.filter(corporation_id = corp.corporation_id)
    # for char in all_characters:
    #     if char:
    #         try:
    #             if int(char.corporation_id) == int(corp.corporation_id):
    #                 current_count = current_count + 1
    #                 allcharacters[char.character_name] = ApiKeyPair.objects.get(api_id=char.api_id)
    #         except:
    #             allcharacters[char.character_name] = None
    #
    # context = {"corp": corp,
    #            "currentCount": current_count,
    #            "characters": allcharacters}
    #
    # return render_to_response('dashboard/eveonline/corpstats.html', context, context_instance=RequestContext(request))
