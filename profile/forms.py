# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.utils.translation import ugettext as _
from eveonline.eve_api_manager import EveApiManager
from eveonline.forms import UserMixin
from eveonline.models import ApiKeyPair
from eveonline.verification import api_pilot_verification_passed, chars_verification_passed


class UpdateKeyForm(UserMixin, forms.Form):
    api_id = forms.CharField(max_length=254, required=True, label=_("Key ID"))
    api_key = forms.CharField(max_length=254, required=True, label=_("Verification Code"))

    def clean(self):
        user = self.user
        api_info = EveApiManager.check_api_is_valid(self.cleaned_data['api_id'],
                                                    self.cleaned_data['api_key'])
        if not api_info:
            raise forms.ValidationError(_('API key is invalid. Please make another.'))

        if ApiKeyPair.objects.filter(api_id=self.cleaned_data['api_id']).exists():
            if not api_pilot_verification_passed(api=ApiKeyPair.objects.get(api_id=self.cleaned_data['api_id']),
                                                 user=user):
                raise forms.ValidationError(_('This API Key belongs to another user. You are not able to use it.'))
                # raise forms.ValidationError(_('Данный Апи Ключ принадлежит другому юзеры. Вы не можете его использовать'))

        if not chars_verification_passed(api_info['characters'], user):
            # Пилот(ы) прошли верификацию. Если это ваши пилот(ы), пройдите верификацию.
            raise forms.ValidationError(_('Пилот(ы) прошли верификацию. Если это ваши пилот(ы), пройдите верификацию.'))




            # if EveManager.check_if_api_key_pair_exist(self.cleaned_data['api_id']):
            #     raise forms.ValidationError(u'API key already exist')

            # check_blue = False
            # try:
            #     check_blue = self.cleaned_data['is_blue']
            # except:
            #     pass

            # if not check_blue:
            #    if not EveApiManager.check_api_is_type_account(self.cleaned_data['api_id'],
            #                                                  self.cleaned_data['api_key']):
            #      raise forms.ValidationError(u'API not of type account')

            # if not EveApiManager.check_api_is_full(self.cleaned_data['api_id'],
            #                                        self.cleaned_data['api_key']):
            #    raise forms.ValidationError(u'API supplied is not a full api key')

        return self.cleaned_data
