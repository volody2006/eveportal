# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import logging
from time import sleep
from OpenSSL.SSL import SysCallError
from django.core.paginator import Paginator
from eveonline.eve_api_manager import EveApiManager
from eveonline.managers import EveManager
from eveonline.models import Character, Corporation, SystemSovereignty, Alliance
from requests import HTTPError
import requests
from util.eve_who import EVEWho
from util.eve_zkillboard import ZKillboardAPI
from util.managers import date_update
import lxml.html as html

logger = logging.getLogger(__name__)



# http://evewho.com/api.php?type=allilist&id=99001648
# http://evewho.com/api.php?type=corplist&id=869043665
# Парсим evewho
#>>> from eveonline.parsers import *
#>>> parse_evewho()


def char_parse():
    evemanager = EveManager()
    if EveApiManager.check_if_api_server_online():

        min_id = 90000001
        min_id = 90578969 #90578963 90578968

        while min_id < 90990001:
            min_id = min_id + 1

            if not Character.objects.filter(character_id=min_id).exists():
                char = evemanager.update_or_create_character(min_id)
                if char:
                    logger.info(u'Загружаем информацио о: %s' , char.character_name)
                else:
                    logger.info(u'Ошибка получения информации о персонаже: %s' , char['char_id'])
                #sleep(0.5)
            else:
                logger.info(u'Персонаж %s есть в базе, пропускаем...' , min_id)



def parse_kill_last_h():
    zkillboard = ZKillboardAPI()
    evemanager = EveManager()
    list = zkillboard.past_seconds(sec=3600)
    # print list
    for char in list:
        if int(char['char_id']) < 3019583:
#            print 'For pass'
            continue
        if not Character.objects.filter(character_id=char['char_id']).exists():
            char_info = evemanager.update_or_create_character(char['char_id'])
            if char_info:
                logger.info(u'Received information about: %s' , char_info.character_name)
            else:
                logger.info(u'Error getting information about the character: %s' , char['char_id'])
        else:
            char = Character.objects.get(character_id=char['char_id'])
            if date_update(ts=char.update, period=1):
                logger.info(u'The character %s is in the database, update information...' , char.character_name)
                char_info = evemanager.update_or_create_character(char.character_id)


def get_zkill_char(character_id):
    zkillboard = ZKillboardAPI()
    try:
        return zkillboard.character_list(character_id)
    except HTTPError:
        return False


def parse_kill():
    zkillboard = ZKillboardAPI()
    evemanager = EveManager()
    chars = Character.objects.filter(is_npc = False,
                                     is_del = False,
                                     is_citizen = False,
                                     zkillboard=False
                                     )
    p = Paginator(chars, 100)
    for i in p.page_range:
        chars_p = p.page(i)
        print (i)
        for character in chars_p:
            list = get_zkill_char(character.character_id)
            if list:
                try:
                    for char in list:
                        if int(char['char_id']) < 3019583:
                            continue
                        if not Character.objects.filter(character_id=char['char_id']).exists():
                            char_info = evemanager.update_or_create_character(char['char_id'])
                            if char_info:
                                logger.info(u'Получили информацио о: %s' , char_info.character_name)
                            else:
                                logger.info(u'Ошибка получения информации о персонаже: %s' , char['char_id'])
                        else:
                            logger.debug(u'Персонаж %s есть в базе, пропускаем...' , char['char_id'])

                except SysCallError:
                    continue
                character.zkillboard = True
                character.save()
                logger.info(u'Пометили чара %s как отпарсенного по киллборде' , character.character_name)




class F(object):
    def __init__(self):
        from requests.adapters import HTTPAdapter
        adapter = HTTPAdapter(pool_connections=100,
                              pool_maxsize=100)

        self.req = requests.Session()
        self.req.mount('http://', adapter)
        self.req.mount('https://', adapter)

    def __call__(self, url):
        return self.req.get(url).content
def pages(object, p=100):
    p = Paginator(object, p)
    return p


def parse_evewho():
    # f = F()
    # evewho = EVEWho(url_fetch_func=f)
    evewho = EVEWho()
    evemanager = EveManager()
    allians = Alliance.objects.filter(is_active=True, evewho=False).exclude(member_count = 0)
    p = pages(allians, 100)
    for i in p.page_range:
        object_p = p.page(i)
        print (i)
        for ali in object_p:
            logger.info(u'Парсим чаров по альянсу %s %s' % (ali.pk, ali.alliance_name))
            list = evewho.alliance_member_list(ali.pk)
            logger.debug(list)#print list
            for char in list:
                if not Character.objects.filter(character_id=char['char_id']).exists():
                    char_info = evemanager.update_or_create_character(char['char_id'])
                    if char_info:
                        logger.info(u'Получили информацио о: %s' % char_info.character_name)
                    else:
                        logger.info(u'Ошибка получения информации о персонаже: %s' , char['char_id'])
                else:
                    logger.debug(u'Персонаж %s есть в базе, пропускаем...' , char['char_id'])
            ali.evewho = True
            ali.save()
            logger.info(u'Али %s по evewho отпарсили' , ali)


    corps = Corporation.objects.filter(is_npc=False, evewho=False).exclude(member_count = 0)

    p = pages(corps, 100)
    for i in p.page_range:
        object_p = p.page(i)
        print (i)
        for corp in object_p:
            # temp_corp, create = TempParserEveWho.objects.get_or_create(id = corp)
            # if temp_corp.evewho:
            #     print ('Corp parser, continue....')
            #     continue
            logger.info(u'Парсим чаров по корпорации %s %s' , corp.corporation_id, corp.corporation_name)
            list = evewho.corp_member_list(corp.corporation_id)
            logger.debug(list)#print list
            for char in list:
                if not Character.objects.filter(character_id=char['char_id']).exists():
                    char_info = evemanager.update_or_create_character(char['char_id'])
                    if char_info:
                        logger.info(u'Получили информацио о: %s' , char_info.character_name)
                    else:
                        logger.info(u'Ошибка получения информации о персонаже: %s' , char['char_id'])
                else:
                    logger.debug(u'Персонаж %s есть в базе, пропускаем...' , char['char_id'])
            corp.evewho = True
            corp.save()
            logger.info(u'Корпу %s по evewho отпарсили' , corp)



class Pages(object):
    def __init__(self):
        self.session = requests.Session()

    def get(self, url):
        return self.session.get(url)

    def post(self, url):
        return self.session.post(url)
page = Pages()

def __get_ice_belts(system_name):
    try:
        url = ('http://games.chruker.dk/eve_online/solarsystem.php?name=%s' % system_name)
        #print (url)
        doc = html.fromstring(page.get(url).content)
        all_status = doc.xpath("//*[@id='top']/table/tbody/tr/td[1]/table/tbody/tr[8]/td[2]/a")
        status = all_status[0].text
        return True
    except:
        return False


def parse_belts():
    # http://games.chruker.dk/eve_online/solarsystem.php?name=6-GRN7
    #
    # //*[@id='top']/table/tbody/tr/td[1]/table/tbody/tr[8]/td[2]/a

    systems = SystemSovereignty.objects.all()
    for system in systems:
        if __get_ice_belts(system.system.name):
            system.ice_belts = 1
            system.save()
            logger.info(u'Лед в системе %s есть' , system.system.name )

