# -*- coding: utf-8 -*-
from django.core.exceptions import MultipleObjectsReturned
from django.utils.datetime_safe import datetime
from djcelery.utils import make_aware
from eveonline.managers import EveManager
from eveonline.models import Character, Corporation, Alliance, CharacterCorpHistory, ApiKeyPair
import mock
from test.test_base import APITestCase
from users.models import CustomUser


class EveManagerTest(APITestCase):

    def setUp(self):
        super(EveManagerTest, self).setUp()
        self.alliance_id = 99005103
        self.alliance_id_old = 10000001
        self.corp_id_is_alli_none_exe = 98292612
        self.corp_id_not_alliance = 98395774
        self.corp_id_alliance = 20000001
        self.corp_id_alliance_executor = 98368686
        self.character_id = 93959323
        self.character_id_citizen = 90000002
        self.character_id_in_npc_corp = 90000149
        self.user = CustomUser.objects.create_user('test_user', 'test@test.ru', 'pasw1234')
        self.bad_user = CustomUser.objects.create_user('test_user_bad', 'user_bad@test.ru', 'user_bad')


    def test_new_alliance(self):
        # Тестируем создание нового альянса, правильный выход создание альянса
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_alliance_information',
            return_value = self.get_alliance_information(self.alliance_id)
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_corporation',
            return_value = True
        ):
            evemanager = EveManager()
            alliance = evemanager.update_or_create_alliance(self.alliance_id)

            self.assertEquals(alliance.alliance_id , int(self.alliance_id))
            self.assertEquals(alliance.member_count, int(self.get_alliance_information(self.alliance_id)['member_count']))
            self.assertEquals(alliance.executor_corp.corporation_id, int(self.get_alliance_information(self.alliance_id)['executor_id']))


    def test_new_alliance_2(self):
        # Тестируем создание нового альянса, Проверяем, корпорация тоже создалась и обновилась.
        alliance_information = self.get_alliance_information(self.alliance_id)
        corporation_information = self.get_corporation_information(alliance_information['executor_id'])
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_alliance_information',
            return_value = alliance_information
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
            return_value = corporation_information
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = True
        ):
            evemanager = EveManager()
            alliance = evemanager.update_or_create_alliance(self.alliance_id)

            corp = Corporation.objects.get(pk=self.get_alliance_information(self.alliance_id)['executor_id'])
            self.assertEquals(corp.alliance, alliance)
            self.assertEquals(corp.ceo.character_id, corporation_information['ceo']['id'])


    def test_new_corp(self):
        # Тестируем создание новой корпорации в составе альянса.
        #alliance_information = self.get_alliance_information(self.alliance_id)
        corporation_information = self.get_corporation_information(self.corp_id_alliance_executor)
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_alliance_information',
            return_value = Alliance.objects.create(alliance_id = corporation_information['alliance']['id'],
                                                   alliance_name = corporation_information['alliance']['name'])
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
            return_value = corporation_information
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = True
        ):
            evemanager = EveManager()
            corp = evemanager.update_or_create_corporation(self.corp_id_alliance_executor)
            self.assertEquals(corp.alliance.alliance_id, self.alliance_id)
            self.assertEquals(corp.ceo.character_id, corporation_information['ceo']['id'])


    def test_new_char(self):
        # Тестируем создание нового чара

        character_information = self.get_character_from_id(self.character_id)
        alliance = Alliance.objects.create(alliance_id = character_information['alliance']['id'],
                                           alliance_name = character_information['alliance']['name'])
        corp = Corporation.objects.create(corporation_id = character_information['corp']['id'],
                                          corporation_name = character_information['corp']['name'])
        with mock.patch(
            'eveonline.managers.EveManager.update_or_create_alliance',
            return_value = alliance
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_corporation',
            return_value = corp
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_character_from_id',
            return_value = character_information
        ):
            evemanager = EveManager()
            char = evemanager.update_or_create_character(self.character_id)
            self.assertEquals(char.character_id, self.character_id)
            self.assertEquals(char.alliance, alliance)
            self.assertEquals(char.corp, corp)
            self.assertFalse(char.is_del)
            self.assertFalse(char.is_npc)
            self.assertFalse(char.is_citizen)


    # def test_update_not_active_alliance(self):
    #     alliance = Alliance.objects.create(alliance_id = 1,
    #                                        alliance_name = "Test Name",
    #                                        is_active = False)
    #
    #     evemanager = EveManager()
    #     alliance = evemanager.update_or_create_alliance(1)
    #     self.assertFalse(alliance.is_active)


    def test_bad_alliance(self):
        #
        alliance = Alliance.objects.create(alliance_id = self.alliance_id)
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_alliance_information',
            return_value = False

        ):
            evemanager = EveManager()
            alliance = evemanager.update_or_create_alliance(self.alliance_id)
            self.assertTrue(alliance.is_active)

    def test_bad_alliance_2(self):
        #
        #alliance = Alliance.objects.create(alliance_id = self.alliance_id)
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_alliance_information',
            return_value = False

        ):
            evemanager = EveManager()
            alliance = evemanager.update_or_create_alliance(self.alliance_id)
            self.assertFalse(alliance)


    def test_npc_corp(self):
        corp = Corporation.objects.create(corporation_id = 1,
                                          is_npc = True)

        evemanager = EveManager()
        corp = evemanager.update_or_create_corporation(1)
        self.assertTrue(corp.is_npc)


    # def test_del_corp(self):
    #
    #     ceo, ceo_created = Character.objects.get_or_create(
    #         character_id = 1,
    #         character_name = 'testttt')
    #     corp = Corporation.objects.create(corporation_id = 1,
    #                                       is_del = False,
    #                                       member_count = 0,
    #                                       ceo = ceo)
    #
    #     with mock.patch(
    #         'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
    #         return_value = True
    #
    #     ):
    #
    #         evemanager = EveManager()
    #         corp = evemanager.update_or_create_corporation(1)
    #         self.assertTrue(corp.is_del)
    #         self.assertEquals(corp.member_count, 0)


    # def test_del_corp_2(self):
    #     corp_1 = Corporation.objects.create(corporation_id = 1,
    #                                       is_del = True,
    #                                       )
    #     evemanager = EveManager()
    #     corp = evemanager.update_or_create_corporation(1)
    #     self.assertEquals(corp, corp_1)

    def test_bad_corp_id(self):
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
            return_value = False

        ):
            evemanager = EveManager()
            corp = evemanager.update_or_create_corporation(1)
            self.assertFalse(corp)


    def test_new_corp_not_ali(self):
        # Тестируем создание новой корпорации в составе альянса.
        #alliance_information = self.get_alliance_information(self.alliance_id)
        corporation_information = self.get_corporation_information(self.corp_id_not_alliance)
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
            return_value = corporation_information
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = True
        ):
            evemanager = EveManager()
            corp = evemanager.update_or_create_corporation(self.corp_id_alliance_executor)
            self.assertIsNone(corp.alliance)


    def test_new_corp_2(self):
        # Тестируем создание новой корпорации в составе альянса.
        #alliance_information = self.get_alliance_information(self.alliance_id)
        corporation_information = self.get_corporation_information(self.corp_id_alliance_executor)
        alliance_information = self.get_alliance_information(corporation_information['alliance']['id'])
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_alliance_information',
            return_value = alliance_information
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
            return_value = corporation_information
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = True
        ):
            evemanager = EveManager()
            corp = evemanager.update_or_create_corporation(self.corp_id_alliance_executor)
            self.assertEquals(corp.alliance.alliance_id, self.alliance_id)
            self.assertEquals(corp.ceo.character_id, corporation_information['ceo']['id'])


    def test_corp_ceo_1(self):

        # Тестируем создание новой корпорации в составе альянса.
        # alliance_information = self.get_alliance_information(self.alliance_id)
        # corporation_information = self.get_corporation_information(self.corp_id_alliance_executor)

        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
            return_value = None
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = None
        ):
            pass


    def test_bad_char(self):
        # Тестируем создание нового чара

        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_character_from_id',
            return_value = False
        ):
            evemanager = EveManager()
            char = evemanager.update_or_create_character(self.character_id)
            self.assertFalse(char)


    def test_bad_char_2(self):
        # Тестируем создание нового чара
        character = Character.objects.create(character_id=self.character_id,
                                             character_name = 'test res',
                                             is_del = False
                                             )
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_character_from_id',
            return_value = False
        ):
            evemanager = EveManager()
            char = evemanager.update_or_create_character(self.character_id)
            #
            self.assertFalse(char.is_del)


    def test_char_del(self):
        # Тестируем создание нового чара
        character = Character.objects.create(character_id=self.character_id,
                                             character_name = 'test res',
                                             is_del = True)
        evemanager = EveManager()
        char = evemanager.update_or_create_character(self.character_id)
        self.assertEquals(char, character)
        self.assertTrue(char.is_del)


    def test_char_npc(self):
        # Тестируем создание нового чара
        character = Character.objects.create(character_id=self.character_id,
                                             character_name = 'test res',
                                             is_npc = True)
        evemanager = EveManager()
        char = evemanager.update_or_create_character(self.character_id)
        self.assertEquals(char, character)
        self.assertTrue(char.is_npc)


    def test_cit_char(self):
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
            return_value = False
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_character_from_id',
            return_value = self.get_character_from_id(self.character_id_citizen)
        ):
            evemanager = EveManager()
            char = evemanager.update_or_create_character(self.character_id_citizen)
            self.assertTrue(char.is_citizen)
            self.assertTrue(char.corp.is_npc)


    def test_char_in_ali(self):
        character_information = self.get_character_from_id(self.character_id)
        with mock.patch(
            'eveonline.managers.EveManager.update_or_create_alliance',
            return_value = Alliance.objects.create(alliance_id = character_information['alliance']['id'],
                                                   alliance_name = character_information['alliance']['name'])
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_corporation',
            return_value = Corporation.objects.create(corporation_id = character_information['corp']['id'],
                                                      corporation_name = character_information['corp']['name'])
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_character_from_id',
            return_value = character_information
        ):
            evemanager = EveManager()
            char = evemanager.update_or_create_character(self.character_id)
            self.assertEquals(char.character_id, self.character_id)
            self.assertFalse(char.is_citizen)
            self.assertFalse(char.corp.is_npc)
            self.assertEquals(char.alliance.alliance_id, character_information['alliance']['id'])
            self.assertEquals(char.corporation.corporation_id, character_information['corp']['id'])


    def test_char_history(self):
        character_information = self.get_character_from_id(self.character_id_in_npc_corp)
        with mock.patch(
            'eveonline.managers.EveManager.update_or_create_corporation',
            return_value = False
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_character_from_id',
            return_value = character_information
        ):
            evemanager = EveManager()
            char = evemanager.update_or_create_character(self.character_id)
            self.assertEquals(char.character_id, self.character_id)
            self.assertFalse(char.is_citizen)
            self.assertTrue(char.corp.is_npc)

            corps_history = CharacterCorpHistory.objects.filter(character = char)
            self.assertEquals(corps_history.count(), 9)


    def test_multi_char_history(self):
        character_information = self.get_character_from_id(self.character_id_in_npc_corp)
        with mock.patch(
            'eveonline.managers.EveManager.update_or_create_corporation',
            return_value = False
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_character_from_id',
            return_value = character_information
        ):
            evemanager = EveManager()
            corps_history_info = character_information['history']
            char = Character.objects.create(character_id =   character_information['id'],
                                            character_name = character_information['name'])
            evemanager.character_corp_history(char, corps_history_info)
            for corp_history in corps_history_info:
                corp_id = corp_history['corp_id']
                start_date = make_aware(datetime.utcfromtimestamp(corp_history['start_ts']))
                corp, corp_history_created = Corporation.objects.get_or_create(corporation_id=corp_history['corp_id'],
                                                                               corporation_name = corp_history['corp_name'])
                char_corp_history = CharacterCorpHistory.objects.create(
                    character = char,
                    corporation = corp,
                    start_date = start_date)
            char = evemanager.update_or_create_character(self.character_id_in_npc_corp)
            corps_history = CharacterCorpHistory.objects.filter(character = char)
            self.assertEquals(corps_history.count(), len(corps_history_info))


    def test_error_1(self):
        #DataError: integer out of range
        # Ошибка большого числа в поле shares. Ошибка проявляется только с базой Постгриса
        ceo, ceo_created = Character.objects.get_or_create(
            character_id = 2,
            character_name = 'Imanima Hinpas')
        corporation, corp_created = Corporation.objects.update_or_create(
            corporation_id = 1000002,
            defaults=dict(
                corporation_name = 'CBD Corporation',
                corporation_ticker = 'CBDC',
                member_count= 154,
                alliance=None,
                ceo = ceo,
                tax = 0.0,
                url = None,
                shares = 30515077373,
                description = None,
                is_del = False,
            )
        )
        self.assertEquals(corporation.corporation_id, 1000002)

    def test_create_api_keypair_account(self):
        api = 3110415

        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_api_info',
            return_value = self.get_api_info(api)
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = None
        ):
            api_info = self.get_api_info(api)
            for i in api_info['characters']:
                all, create = Alliance.objects.get_or_create(
                    alliance_id = api_info['characters'][i]['alliance']['id'],
                    alliance_name = api_info['characters'][i]['alliance']['name'])
                corp, create = Corporation.objects.get_or_create(
                    corporation_id = api_info['characters'][i]['corp']['id'],
                    corporation_name = api_info['characters'][i]['corp']['name'])
                char, create = Character.objects.get_or_create(
                    character_id = i,
                    character_name = api_info['characters'][i]['name'])

            #user = CustomUser.objects.create_user('test_user', 'test@test.ru', 'pasw1234')
            evemanager = EveManager()
            evemanager.create_api_keypair(api, ',blabalalala', self.user)
            api_obj = ApiKeyPair.objects.get(pk = api)
            self.assertEquals(api_obj.api_id, api)
            for char in Character.objects.all():
                self.assertIn(api_obj, char.api.all())
                self.assertEquals(char.user, self.user)
            self.assertEquals(api_obj.user, self.user)
            self.assertIsNone(api_obj.corporation)
            self.assertEquals(api_obj.type, 'account')


    def test_create_api_keypair_corp(self):
        api = 4512089
        corp = Corporation.objects.create(corporation_id = 98292612)
        char = Character.objects.create(pk = 93959323)
        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_api_info',
            return_value = self.get_api_info(api)
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_corporation',
            return_value = corp
        ),   mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_corporation_information',
            return_value = self.get_corporation_information(corp.pk)
        ),  mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = char
        ):
            #user = CustomUser.objects.create_user('test_user', 'test@test.ru', 'pasw1234')
            evemanager = EveManager()
            evemanager.create_api_keypair(api, ',blabalalala', self.user)
            api_obj = ApiKeyPair.objects.get(pk = api)
            self.assertEquals(api_obj.api_id, api)

            self.assertEquals(api_obj.user, self.user)
            self.assertIsNotNone(api_obj.corporation)
            self.assertEquals(api_obj.type, 'corp')
            char = Character.objects.get(pk = 93959323)
            self.assertEquals(char.user, self.user)


    def test_delete_api_key_pair_user(self):
        api = 3110415

        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_api_info',
            return_value = self.get_api_info(api)
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = None
        ):
            api_info = self.get_api_info(api)
            for i in api_info['characters']:
                all, create = Alliance.objects.get_or_create(
                    alliance_id = api_info['characters'][i]['alliance']['id'],
                    alliance_name = api_info['characters'][i]['alliance']['name'])
                corp, create = Corporation.objects.get_or_create(
                    corporation_id = api_info['characters'][i]['corp']['id'],
                    corporation_name = api_info['characters'][i]['corp']['name'])
                char, create = Character.objects.get_or_create(
                    character_id = i,
                    character_name = api_info['characters'][i]['name'])

            evemanager = EveManager()
            evemanager.create_api_keypair(api, ',blabalalala', self.user)
            api_del = evemanager.delete_api_key_pair_user(api, self.user)
            api_obj = ApiKeyPair.objects.filter(pk = api).count()
            self.assertEquals(api_obj, 0)
            self.assertTrue(api_del)
            for char in Character.objects.all():
                self.assertEquals(char.api.all().count(), 0)
                self.assertIsNone(char.user)

    def test_delete_api_key_pair_bad_user(self):
        api = 3110415

        with mock.patch(
            'eveonline.eve_api_manager.EveApiManager.get_api_info',
            return_value = self.get_api_info(api)
        ),   mock.patch(
            'eveonline.managers.EveManager.update_or_create_character',
            return_value = None
        ):
            api_info = self.get_api_info(api)
            for i in api_info['characters']:
                all, create = Alliance.objects.get_or_create(
                    alliance_id = api_info['characters'][i]['alliance']['id'],
                    alliance_name = api_info['characters'][i]['alliance']['name'])
                corp, create = Corporation.objects.get_or_create(
                    corporation_id = api_info['characters'][i]['corp']['id'],
                    corporation_name = api_info['characters'][i]['corp']['name'])
                char, create = Character.objects.get_or_create(
                    character_id = i,
                    character_name = api_info['characters'][i]['name'])

            evemanager = EveManager()
            evemanager.create_api_keypair(api, ',blabalalala', self.user)
            api_del = evemanager.delete_api_key_pair_user(api, self.bad_user)
            api_obj = ApiKeyPair.objects.filter(pk = api).count()
            self.assertEquals(api_obj, 1)
            self.assertFalse(api_del)
            for char in Character.objects.all():
                self.assertEquals(char.api.all().count(), 1)
                self.assertEquals(char.user, self.user)