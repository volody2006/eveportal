# -*- coding: utf-8 -*-


class UserMixin(object):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(UserMixin, self).__init__(*args, **kwargs)
