# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function, absolute_import, division
from datetime import timedelta
from time import sleep
from urllib2 import HTTPError

from OpenSSL.SSL import SysCallError

# from evelink.thirdparty.eve_who import EVEWho
from celery.task import periodic_task
from celery.task import task

from celerytask.managers import run_task
from eve_db.models import MapSolarSystem, ChrFaction
from evelink.map import Map
from eveonline.eve_api_manager import EveApiManager
from eveonline.managers import EveManager, GrestManagers, Error
from eveonline.models import Corporation, Character, Alliance, SystemSovereignty, ApiKeyPair
from eveonline.parsers import parse_kill_last_h, parse_kill, parse_evewho
import logging

from eveonline.verification import api_pilot_verification_passed
from eveportal import settings
from util.eve_who import EVEWho

from util.eve_zkillboard import ZKillboardAPI
from util.managers import date_update, date_update_timedelta
from django.core.paginator import Paginator

logger = logging.getLogger(__name__)


# logger.debug("some message")
# logger.warning("oops, it is a warning")
# logger.error("bad, very bad")
#
# try:
#     do something
# except ValueError:
#     logger.exception("I know it could happen")


# >>> from eveonline.tasks import *
# >>> parse_evewho() update_zkill_all()

# Обновляем апи ключи, если ключ недействителен, удаляем.
# Если он единственный , уведомляем юзера о потере доступа к чарам
# @periodic_task(run_every=crontab(minute=0, hour="*/3"))



@periodic_task(run_every=timedelta(hours=3, minutes=3))
def run_api_refresh():
    name = ('%s:%s' % (__name__, 'run_api_refresh'))
    delta = 3 * 60 * 60 + 3 * 60
    if not run_task(key=name, delta=delta):
        if EveApiManager.check_if_api_server_online():
            apis = ApiKeyPair.objects.filter(active=True)
            for api in apis:
                if not EveApiManager.api_key_is_valid(api.api_id, api.api_key):
                    for char in api.character_set.all():
                        char.api.remove(api)
                    api.active = False
                    api.save()


@periodic_task(run_every=timedelta(days=1))
def run_create_api_keypair():
    name = ('%s:%s' % (__name__, 'run_create_api_keypair'))
    delta = 24 * 60 * 60 - 1
    if not run_task(key=name, delta=delta):
        for api in ApiKeyPair.objects.filter(active=True):
            logger.info('run_create_api_keypair %s %s' % (api.api_id, api.user))
            task_run_create_api_keypair.delay(api.api_id, api.api_key, api.user)


@task
def task_run_create_api_keypair(api_id, api_key, user):
    name = ('%s:%s:%s:%s' % (__name__, 'task_run_create_api_keypair', api_id, user))
    delta = 60 * 60
    if not run_task(key=name, delta=delta):
        logger.info('Task run_create_api_keypair %s %s' % (api_id, user))
        evemanager = EveManager()
        if ApiKeyPair.objects.filter(pk=api_id).exists():
            if api_pilot_verification_passed(api=ApiKeyPair.objects.get(pk=api_id),
                                             user=user):
                api_create = evemanager.create_api_keypair(api_id, api_key, user)
            else:
                logger.info('Задача создание, обновление ключа %s провалилась, ключ не принадлежит %s', api_id, user)


@periodic_task(run_every=timedelta(hours=4))
def updateEveSovereignty():
    name = ('%s:%s' % (__name__, 'updateEveSovereignty'))
    delta = 4 * 60 * 60
    if not run_task(key=name, delta=delta):
        evemanager = EveManager()
        map = Map()
        maps = map.sov_by_system()
        for i in maps.result[0]:
            if maps.result[0][i]['faction_id'] is not None:
                continue
            system = MapSolarSystem.objects.get(id=i)
            if maps.result[0][i]['alliance_id'] is not None:
                if Alliance.objects.filter(alliance_id=maps.result[0][i]['alliance_id']).exists():
                    alliance = Alliance.objects.get(alliance_id=maps.result[0][i]['alliance_id'])
                else:
                    alliance = evemanager.update_or_create_alliance(maps.result[0][i]['alliance_id'])
                if Corporation.objects.filter(corporation_id=maps.result[0][i]['corp_id']).exists():
                    corp = Corporation.objects.get(corporation_id=maps.result[0][i]['corp_id'])
                else:
                    corp = evemanager.update_or_create_corporation(maps.result[0][i]['corp_id'])
            else:
                alliance = None
                corp = None

            obj, created = SystemSovereignty.objects.update_or_create(
                system=system,
                defaults=dict(alliance=alliance,
                              corp=corp,
                              )
            )
        evemanager.conquerable_station_list()


def update_corp():
    evemanager = EveManager()
    corps = Corporation.objects.filter(is_npc=False,
                                       is_del=False
                                       )
    p = Paginator(corps, 100)
    for i in p.page_range:
        corps_p = p.page(i)
        print(i)
        for corp in corps_p:
            if date_update(ts=corp.update, period=1):
                logger.info('Обновили информацию о корпорации %s: %s' % (corp.corporation_id, corp.corporation_name))
                corp = evemanager.update_or_create_corporation(corp.corporation_id)


# @periodic_task(run_every = timedelta (days = 1))
# @task
def update_char():
    evemanager = EveManager()
    chars = Character.objects.filter(is_npc=False,
                                     is_del=False,
                                     is_citizen=False, )
    p = Paginator(chars, 100)
    for i in p.page_range:
        chars_p = p.page(i)
        for char in chars_p:
            if date_update(ts=char.update, period=14):
                logger.info('Пришло время обновить информацию об персонаже %s' % char.character_name)
                char = evemanager.update_or_create_character(char.character_id)


# @periodic_task(run_every=crontab(minute=45, hour="*/6"))
@periodic_task(run_every=timedelta(hours=5, minutes=3))
def run_new_alliance_corp_update():
    name = ('%s:%s' % (__name__, 'run_new_alliance_corp_update'))
    delta = 59 * 60
    if settings.DEBUG:
        print('Дебагер run_new_alliance_corp_update')
        delta = 10
    if not run_task(key=name, delta=delta):
        evemanager = EveManager()
        if EveApiManager.check_if_api_server_online():
            alliances_info = EveApiManager.get_all_alliance_information()

            for alliance_id in alliances_info:
                if Alliance.objects.filter(alliance_id=alliance_id).exists():
                    alliance = Alliance.objects.get(alliance_id=alliance_id)
                    if date_update_timedelta(ts=alliance.update, hours=1):
                        logger.info("It is time to update the information about the Alliance %s %s", alliance_id,
                                    alliances_info[alliance_id]['name'])
                        evemanager.update_or_create_alliance(alliance_id)
                else:
                    logger.info('Receive information about the Alliance %s %s', alliance_id,
                                alliances_info[alliance_id]['name'])
                    evemanager.update_or_create_alliance(alliance_id)

                for corp_id in alliances_info[alliance_id]['member_corps']:
                    logger.info('It is time to update the information about the corporation %s' % corp_id)
                    evemanager.update_or_create_corporation(corp_id)

                all_corp_alliance = Corporation.objects.filter(alliance=alliance_id)
                for corp in all_corp_alliance:
                    if date_update_timedelta(ts=corp.update, hours=1):
                        logger.info('The Corporation %s is no longer in the Alliance, updated information',
                                    corp.corporation_name)
                        evemanager.update_or_create_corporation(corp.corporation_id)


@periodic_task(run_every=timedelta(days=1))
def update_ali_info():
    name = ('%s:%s' % (__name__, 'update_ali_info'))
    delta = 24 * 60 * 60 - 1
    if not run_task(key=name, delta=delta):
        Grest = GrestManagers()
        alliances = Alliance.objects.filter(is_active=True)
        for alli in alliances:
            print('Update alliance %s' % alli.alliance_id)
            Grest._update_or_create_alliance(alli.alliance_id)


@periodic_task(run_every=timedelta(hours=1, minutes=1))
def update_zkill_last_hour():
    name = ('%s:%s' % (__name__, 'update_zkill_last_hour'))
    delta = 60 * 60
    if not run_task(key=name, delta=delta):
        parse_kill_last_h()
    else:
        logger.info('Do not need to upgrade, reset the task %s', name)


@task
# @periodic_task(run_every = timedelta(hours = 22))
def update_zkill_all():
    zkillboard = ZKillboardAPI()
    evemanager = EveManager()
    chars = Character.objects.filter(is_npc=False, is_del=False, is_citizen=False, zkillboard=False)
    p = Paginator(chars, 100)
    for i in p.page_range:
        chars_p = p.page(i)
        print(i)
        for character in chars_p:
            try:
                for char in zkillboard.character_list(character.character_id):
                    if int(char['char_id']) < 3019583:
                        continue
                    if not Character.objects.filter(character_id=char['char_id']).exists():
                        char_info = evemanager.update_or_create_character(char['char_id'])
                        if char_info:
                            logger.info('Received information about: %s', char_info.character_name)
                            parse_char_zkillboard.delay(char_info, True)
                        else:
                            logger.info('Error getting information about the character: %s', char['char_id'])
                    else:
                        logger.debug('The character %s is in the database, skipping...', char['char_id'])

            except SysCallError as e:
                logger.error('ERROR SysCallError: %s', e)
                continue
            except HTTPError as e:
                logger.error('ERROR HTTPError: %s', e)
                print('sleep 10 c')
                sleep(10)
                continue
            character.zkillboard = True
            character.save()
            logger.info('Пометили чара %s как отпарсенного по киллборде', character.character_name)


@task
def parse_corp_evewho(corporation_id):
    evewho = EVEWho()
    evemanager = EveManager()
    corp = Corporation.objects.get(pk=corporation_id)
    logger.info('Parsim characters for corporations %s %s', corp.corporation_id, corp.corporation_name)
    for char in evewho.corp_member_list(corp.corporation_id):
        if not Character.objects.filter(character_id=char['char_id']).exists():
            char_info = evemanager.update_or_create_character(char['char_id'])
            if char_info:
                parse_char_zkillboard.delay(char['char_id'], True)
                logger.info('Received information about: %s', char_info.character_name)
            else:
                logger.info('Error getting information about the character: %s', char['char_id'])
        else:
            logger.debug('The character %s is in the database, skipping...', char['char_id'])
    corp.evewho = True
    corp.save()
    # logger.info(u'Корпу %s по evewho отпарсили', corp)


@task
def task_evewho():
    # f = F()
    # evewho = EVEWho(url_fetch_func=f)
    evewho = EVEWho()
    evemanager = EveManager()
    allians = Alliance.objects.filter(is_active=True, evewho=False).exclude(member_count=0)
    p = Paginator(allians, 100)
    for i in p.page_range:
        object_p = p.page(i)
        print(i)
        for ali in object_p:
            logger.info('Парсим чаров по альянсу %s %s', ali.pk, ali.alliance_name)
            for char in evewho.alliance_member_list(ali.pk):
                if not Character.objects.filter(character_id=char['char_id']).exists():
                    char_info = evemanager.update_or_create_character(char['char_id'])
                    if char_info:
                        parse_char_zkillboard.delay(char_info, True)
                        logger.info('Received information about: %s', char_info.character_name)
                    else:
                        logger.info('Error getting information about the character: %s', char['char_id'])
                else:
                    logger.debug('The character %s is in the database, skipping..', char['char_id'])
            ali.evewho = True
            ali.save()
            logger.info('Али %s по evewho отпарсили', ali)

    corps = Corporation.objects.filter(is_npc=False, evewho=False).exclude(member_count=0).exclude(member_count=1)

    p = Paginator(corps, 100)
    for i in p.page_range:
        object_p = p.page(i)
        print(i)
        for corp in object_p:
            logger.info('Парсим чаров по корпорации %s %s', corp.corporation_id, corp.corporation_name)
            for char in evewho.corp_member_list(corp.corporation_id):
                if not Character.objects.filter(character_id=char['char_id']).exists():
                    char_info = evemanager.update_or_create_character(char['char_id'])
                    if char_info:
                        parse_char_zkillboard.delay(char_info, True)
                        logger.info('Received information about: %s', char_info.character_name)
                    else:
                        logger.info('Error getting information about the character: %s', char['char_id'])
                else:
                    logger.debug('The character %s is in the database, skipping...', char['char_id'])
            corp.evewho = True
            corp.save()
            logger.info(u'Корпу %s по evewho отпарсили', corp)


@task
def parse_char_zkillboard(character_id, recursive=True):
    zkillboard = ZKillboardAPI()
    evemanager = EveManager()
    character = Character.objects.get(pk=character_id)
    logger.info('parse_char_zkillboard %s start', character)
    try:
        list = zkillboard.character_list(character.pk)
        for char in list:
            if int(char['char_id']) < 3019583:
                continue
            if not Character.objects.filter(character_id=char['char_id']).exists():
                char_info = evemanager.update_or_create_character(char['char_id'])
                if char_info:
                    logger.info('Get information about: %s', char_info.character_name)
                    if recursive:
                        parse_char_zkillboard.delay(char_info, True)
                else:
                    logger.info('Failed to get information about character: %s', char['char_id'])
            else:
                logger.debug('Character %s is in the database, are missing...', char['char_id'])
        character.zkillboard = True
        character.save()
        logger.info('parse_char_zkillboard %s end', character.character_name)

    except Exception:
        character.zkillboard = True
        character.save()
        logger.info('parse_char_zkillboard %s end (error)', character.character_name)


@periodic_task(run_every=timedelta(hours=1, minutes=1))
def update_corp_is_del():
    name = ('%s:%s' % (__name__, 'update_corp_is_del'))
    delta = 60 * 60
    if not run_task(key=name, delta=delta):
        evemanager = EveManager()
        for corp in Corporation.objects.filter(member_count=0, is_del=False, is_npc=False):
            print(evemanager.update_or_create_corporation(corp.pk))


# parse_char_zkillboard(char, True)


@task()
def task_update_or_create_corporation(corporation_id):
    evemanager = EveManager()
    corporation = evemanager.update_or_create_corporation(corporation_id)
    # logger.info(u'Update corporation %s', corporation.corporation_name)
    if not corporation.evewho:
        parse_corp_evewho.delay(corporation_id)


@task()
def task_update_or_create_character(character_id):
    evemanager = EveManager()
    character = evemanager.update_or_create_character(character_id)
    logger.info(u'Update character %s', character.name)
    if not character.zkillboard:
        parse_char_zkillboard.delay(character_id, recursive=True)
