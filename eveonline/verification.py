# -*- coding: utf-8 -*-
# Верификация: Почтой, Ингейм браузер, деньгами, SSO
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from eveonline.managers import EveManager

from eveonline.models import Character


def verifiable_char(char_id, user):
    char = Character.objects.get(pk=char_id)
    char.verified_user = user
    char.save()
    # Юзер принадлежал другому юзеру.
    # Меняем пользователя, переписываем письма,
    # кошелек и аккаунты на настоящего пользователя.
    # Апи ключи приписанные этому чару также переходят новому юзеру,
    # чары которые приписаны к этому ключу, также верифицируем.
    if char.user != user:
        char.user = user
        char.save()
        for api in char.api.all():
            api.user = user
            api.save()
            for char_one_api in api.character_set.filter(verified_user=None):
                char_one_api.user = user
                char_one_api.verified_user = user
                char_one_api.save()


def api_pilot_verification_passed(api, user):
    # Проверка, прошли ли пилоты, у которых данный ключ верификацию.
    # Если прошли, то проверяем, что пользователь именно этот.
    # Возвращаем Истину, если верно и Ложь, если нет.
    # Если верификации не было, возвращаем Истину
    for char in api.character_set.all():
        if char.verified_user:
            if char.verified_user == user:
                return True
            else:
                return False
    return True


def chars_verification_passed(chars, user):
    # chars = {93959323: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
    #                     'corp': {'id': 98292612, 'name': 'Stalin Corporation'}, 'id': 93959323,
    #                     'name': 'Stalin Vozmezdie'},
    #          95367615: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
    #                     'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'}, 'id': 95367615,
    #                     'name': 'Vladimir Mackevich'},
    #          94539743: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
    #                     'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'}, 'id': 94539743,
    #                     'name': 'Margaret Baroness'}}

    # Проверка, прошли ли пилоты, у которых данный ключ верификацию.
    # Если прошли, то проверяем, что пользователь именно этот.
    # Возвращаем Истину, если верно и Ложь, если нет.
    # Если верификации не было, возвращаем Истину
    e = EveManager()
    for character_id in chars:
        if Character.objects.filter(character_id=character_id).exists():
            char = Character.objects.get(character_id=character_id)
            if char.verified_user:
                if char.verified_user == user:
                    return True
                else:
                    return False
    return True