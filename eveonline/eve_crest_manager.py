# -*- coding: utf-8 -*-

import calendar
import time
from requests import ConnectionError
from eveportal import settings
__author__ = 'volody'
import pycrest

import logging

logger = logging.getLogger(__name__)

class EveGrest():

    def __init__(self):

        self.client_id = settings.ClientID
        self.key = settings.SecretKey
        self.callback_url = settings.CallbackURL
        #eve = pycrest.EVE(client_id="your_client_id", api_key="your_api_key", redirect_uri="https://your.site/crest")
        self.eve = pycrest.EVE(client_id=settings.ClientID, api_key=settings.SecretKey, redirect_uri=settings.CallbackURL)

    def getByAttrVal(self, objlist, attr, val):
        ''' Searches list of dicts for a dict with dict[attr] == val '''
        matches = [getattr(obj, attr) == val for obj in objlist]
        print matches
        index = matches.index(True)  # find first match, raise ValueError if not found
        print index
        print objlist[index]
        return objlist[index]


    def getAllItems(self, page):

        ''' Fetch data from all pages '''
        ret = page().items
        print ret
        while hasattr(page(), 'next'):
            page = page().next()
            ret.extend(page().items)
        print ret
        return ret

    def _get (self, url):
        try:
            return self.eve.get(url)
        except ConnectionError:
            print u'разрыв соединения'
            for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]:
                print i
                time.sleep(5)
            return self.eve.get(url)

    def parse_ts(self, v):
        """Parse a timestamp from EVE into a unix-ish timestamp."""
        if v == '':
            return None
        ts = calendar.timegm(time.strptime(v, "%Y-%m-%dT%H:%M:%S"))
        # Deal with EVE's nonexistent 0001-01-01T00:00:00 timestamp
        return ts if ts > 0 else None


    def alliances(self):
        self.eve.alliances()
        aliance = self.eve.get('https://public-crest.eveonline.com/alliances/')

    def alliance(self, alliance_id= 99000006):
        url = 'https://public-crest.eveonline.com/alliances/%s/' % alliance_id
        alliance_info = self._get(url)
     #   print url
      #  print alliance_info
        return alliance_info

    def character(self, character_id= 549618368):
        url = 'https://public-crest.eveonline.com/characters/%s/' % character_id
        character_info = self._get(url)
       # print url
       # print character_info
        return character_info

    def corporation(self, corporation_id):
        url = 'https://public-crest.eveonline.com/corporations/%s/' % corporation_id

        return self._get(url)

#ali
{u'startDate': u'2010-11-04T13:11:00',
 u'corporationsCount_str': u'2',
 u'creatorCharacter':
     {u'name': u'Drako Zay',
      u'isNPC': False,
      u'href': u'https://public-crest.eveonline.com/characters/549618368/',
      u'capsuleer':
          {u'href': u'https://public-crest.eveonline.com/characters/549618368/capsuleer/'},
      u'portrait': {u'64x64':
                        {u'href': u'http://imageserver.eveonline.com/Character/549618368_64.jpg'},
                    u'128x128': {u'href': u'http://imageserver.eveonline.com/Character/549618368_128.jpg'},
                    u'256x256': {u'href': u'http://imageserver.eveonline.com/Character/549618368_256.jpg'},
                    u'32x32': {u'href': u'http://imageserver.eveonline.com/Character/549618368_32.jpg'}},
      u'id': 549618368,
      u'id_str': u'549618368'},
 u'description': u'<font size="12" color="#bfffffff">No Friend nor Ally we are Mercenaries. '
                 u'<br><br>Lurking in the shadows hunting our latest victims for nothing more than blood '
                 u'/font>',
 u'executorCorporation':
     {u'name': u'Demonic Imperial Holding',
      u'isNPC': False,
      u'href': u'https://public-crest.eveonline.com/corporations/1983708877/',
      u'id_str': u'1983708877',
      u'logo':
          {u'64x64':
               {u'href': u'http://imageserver.eveonline.com/Corporation/1983708877_64.png'},
           u'128x128':
               {u'href': u'http://imageserver.eveonline.com/Corporation/1983708877_128.png'},
           u'256x256': {u'href': u'http://imageserver.eveonline.com/Corporation/1983708877_256.png'},
           u'32x32': {u'href': u'http://imageserver.eveonline.com/Corporation/1983708877_32.png'}},
      u'id': 1983708877},
 u'name': u'Everto Rex Regis',
 u'creatorCorporation':
     {u'name': u'Demonic Empire',
      u'isNPC': False,
      u'href': u'https://public-crest.eveonline.com/corporations/665335352/',
      u'id_str': u'665335352',
      u'logo': {u'64x64': {u'href': u'http://imageserver.eveonline.com/Corporation/665335352_64.png'},
                u'128x128': {u'href': u'http://imageserver.eveonline.com/Corporation/665335352_128.png'},
                u'256x256': {u'href': u'http://imageserver.eveonline.com/Corporation/665335352_256.png'},
                u'32x32': {u'href': u'http://imageserver.eveonline.com/Corporation/665335352_32.png'}},
      u'id': 665335352},
 u'deleted': False,
 u'corporationsCount': 2,
 u'url': u'http://evertorexregis.net',
 u'corporations':
     [{u'name': u'Demonic Empire',
       u'isNPC': False,
       u'href': u'https://public-crest.eveonline.com/corporations/665335352/',
       u'id_str': u'665335352',
       u'logo': {u'64x64': {u'href': u'http://imageserver.eveonline.com/Corporation/665335352_64.png'},
                 u'128x128': {u'href': u'http://imageserver.eveonline.com/Corporation/665335352_128.png'},
                 u'256x256': {u'href': u'http://imageserver.eveonline.com/Corporation/665335352_256.png'},
                 u'32x32': {u'href': u'http://imageserver.eveonline.com/Corporation/665335352_32.png'}},
       u'id': 665335352},
      ],
 u'shortName': u'666',
 u'id': 99000006,
 u'id_str': u'99000006'}

{u'motd':
     {u'dust':
          {u'href': u'http://newsfeed.eveonline.com/articles/71'},
      u'eve': {u'href': u'http://client.eveonline.com/motd/'},
      u'server': {u'href': u'http://client.eveonline.com/motd/'}},
 u'crestEndpoint': {u'href': u'https://public-crest.eveonline.com/'},
 u'corporationRoles':
     {u'href': u'https://public-crest.eveonline.com/corporations/roles/'},
 u'itemGroups':
     {u'href': u'https://public-crest.eveonline.com/inventory/groups/'},
 u'channels': {u'href': u'https://public-crest.eveonline.com/chat/channels/'},
 u'corporations':
     {u'href': u'https://public-crest.eveonline.com/corporations/'},
 u'alliances':
     {u'href': u'https://public-crest.eveonline.com/alliances/'},
 u'itemTypes':
     {u'href': u'https://public-crest.eveonline.com/types/'},
 u'decode':
     {u'href': u'https://public-crest.eveonline.com/decode/'},
 u'battleTheatres':
     {u'href': u'https://public-crest.eveonline.com/battles/theatres/'},
 u'marketPrices':
     {u'href': u'https://public-crest.eveonline.com/market/prices/'},
 u'itemCategories':
     {u'href': u'https://public-crest.eveonline.com/inventory/categories/'},
 u'regions':
     {u'href': u'https://public-crest.eveonline.com/regions/'},
 u'bloodlines':
     {u'href': u'https://public-crest.eveonline.com/bloodlines/'},
 u'marketGroups': {u'href': u'https://public-crest.eveonline.com/market/groups/'},
 u'sovereignty':
     {u'campaigns':
          {u'href': u'https://public-crest.eveonline.com/sovereignty/campaigns/'},
      u'structures':
          {u'href': u'https://public-crest.eveonline.com/sovereignty/structures/'}},
 u'tournaments':
     {u'href': u'https://public-crest.eveonline.com/tournaments/'},
 u'map': {u'href': u'https://public-crest.eveonline.com/map/'},
 u'virtualGoodStore': {u'href': u'https://vgs-tq.eveonline.com/'},
 u'serverVersion': u'EVE-TRANQUILITY 13.09.969345.969338',
 u'wars': {u'href': u'https://public-crest.eveonline.com/wars/'},
 u'incursions': {u'href': u'https://public-crest.eveonline.com/incursions/'},
 u'races': {u'href': u'https://public-crest.eveonline.com/races/'},
 u'authEndpoint': {u'href': u'https://login-tq.eveonline.com/oauth/token/'},
 u'serviceStatus': {u'dust': u'online', u'eve': u'online', u'server': u'online'},
 u'userCounts': {u'dust': 1690, u'dust_str': u'1690', u'eve': 14970,
                 u'eve_str': u'14970'},
 u'industry':
     {u'facilities':
          {u'href': u'https://public-crest.eveonline.com/industry/facilities/'},
      u'systems': {u'href': u'https://public-crest.eveonline.com/industry/systems/'}},
 u'clients': {u'dust': {u'href': u'https://public-crest.eveonline.com/roots/dust/'},
              u'eve': {u'href': u'https://public-crest.eveonline.com/roots/eve/'}},
 u'time': {u'href': u'https://public-crest.eveonline.com/time/'},
 u'marketTypes': {u'href': u'https://public-crest.eveonline.com/market/types/'},
 u'serverName': u'TRANQUILITY'}
