from requests import HTTPError

import evelink
from cache import DJCache
import logging
from eveportal import settings


logger = logging.getLogger(__name__)

class EveApiManager():

    def __init__(self, cache=None, api_id= None, api_key=None):
        if cache:
            self.cache = cache
        else:
            self.cache = DJCache(cache_name='redis-cache')
            # self.cache = DJCache(cache_name='evelink')
        if api_id and api_key:
            self.api = evelink.api.API(api_key=(api_id, api_key),
                                       cache=self.cache)
        else:
            self.api = evelink.api.API(cache=self.cache)


    @staticmethod
    def api_evelint(api_id= None, api_key=None):
        user_agent = '%s %s' % (settings.ALLOWED_HOSTS[0], settings.ADMINS[0][1])
        if api_id and api_key:
            return evelink.api.API(api_key=(api_id, api_key),
                                   # cache=DJCache(cache_name='redis-cache'),
                                   # cache=DJCache(cache_name='evelink'),
                                   user_agent = user_agent)


        return evelink.api.API(
            # cache=DJCache(cache_name='redis-cache'),
            user_agent = user_agent
        )
        # return evelink.api.API(cache=DJCache(cache_name='evelink'), user_agent = user_agent)

    @staticmethod
    def get_characters_from_api(api_id, api_key):
        try:
            api = EveApiManager.api_evelint(api_id, api_key)
            # api = evelink.api.API(api_key=(api_id, api_key))
            # Should get characters
            account = evelink.account.Account(api=api)
            chars = account.characters()
            return chars.result
        except evelink.api.APIError as error:
            print ('ERROR: get_characters_from_api - %s ' % error)
            return False


    @staticmethod
    def get_character_from_id(char_id):

        try:
            eve = evelink.eve.EVE(api=EveApiManager.api_evelint())
            character_info = eve.character_info_from_id(char_id)
            return character_info.result
        except evelink.api.APIError as error:
            logger.exception ('ERROR %s: get_characters_from_api - %s ' % (error, char_id))
            return False



    @staticmethod
    def get_corporation_ticker_from_id(corp_id):
        ticker = ""
        try:
            api = EveApiManager.api_evelint()
            corp = evelink.corp.Corp(api=api)
            response = corp.corporation_sheet(corp_id)
            ticker = response[0]['ticker']
        except evelink.api.APIError as error:
            logger.exception ('ERROR: get_corporation_ticker_from_id - %s ' % error)

        return ticker

    @staticmethod
    def get_alliance_information(alliance_id):
        results = {}
        try:
            api = EveApiManager.api_evelint()
            eve = evelink.eve.EVE(api=api)
            alliance = eve.alliances()
            results = alliance[0][int(alliance_id)]
            return results
        except evelink.api.APIError as error:
            logger.exception ('ERROR: get_alliance_information - %s %s' % (alliance_id, error))
            return False
        except KeyError:
            logger.exception('KeyError: get_alliance_information(%s) ' % alliance_id)
            return False

    @staticmethod
    def get_all_alliance_information():
        results = {}
        try:
            api = EveApiManager.api_evelint()
            eve = evelink.eve.EVE(api=api)
            alliance = eve.alliances()
            results = alliance[0]
        except evelink.api.APIError as error:
            logger.exception ('ERROR: get_all_alliance_information - %s ' % error)
            print( error)

        return results

    @staticmethod
    def get_corporation_information(corp_id=None, api_id=None, api_key=None):
        try:
            if api_id and api_key:
                api = EveApiManager.api_evelint(api_id, api_key)
                corp = evelink.corp.Corp(api=api)
                corpinfo = corp.corporation_sheet()
            elif corp_id:
                api = EveApiManager.api_evelint()
                corp = evelink.corp.Corp(api=api)
                corpinfo = corp.corporation_sheet(corp_id=int(corp_id))
            else:
                return False

            results = corpinfo[0]
            return results
        except evelink.api.APIError as error:
            logger.exception ('ERROR: get_corporation_information - %s ' % error)

        return False

    @staticmethod
    def check_api_is_type_account(api_id, api_key):
        try:
            api = EveApiManager.api_evelint(api_id, api_key)
            account = evelink.account.Account(api=api)
            info = account.key_info()
            return info[0]['type'] == "account"

        except evelink.api.APIError as error:
            logger.exception ('ERROR: check_api_is_type_account - %s ' % error)

        return False


    @staticmethod
    def check_api_is_full(api_id, api_key):
        try:
            api = EveApiManager.api_evelint(api_id, api_key)
            account = evelink.account.Account(api=api)
            info = account.key_info()
            return info[0]['access_mask'] == 1073741823

        except evelink.api.APIError as error:
            logger.exception ('ERROR: check_api_is_full - %s ' % error)

        return False

    @staticmethod
    def check_api_is_valid(api_id, api_key):
        try:
            api = EveApiManager.api_evelint(api_id, api_key)
            account = evelink.account.Account(api=api)
            info = account.key_info()
            return info[0]

        except evelink.api.APIError as error:
            logger.exception ('ERROR: check_api_is_valid - %s ' % error)

        return False


    @staticmethod
    def get_api_info(api_id, api_key):
        try:
            api = EveApiManager.api_evelint(api_id, api_key)
            account = evelink.account.Account(api=api)
            info = account.key_info()
            return info.result

        except evelink.api.APIError as error:
            logger.exception ('ERROR: get_api_info - %s ' % error)

        return False

    @staticmethod
    def api_key_is_valid(api_id, api_key):
        try:
            api = EveApiManager.api_evelint(api_id, api_key)
            account = evelink.account.Account(api=api)
            info = account.key_info()
            return True
        except evelink.api.APIError as error:
            logger.exception ('ERROR: api_key_is_valid - %s ' % error)

        return False

    @staticmethod
    def check_if_api_server_online():
        try:
            api = EveApiManager.api_evelint()
            server = evelink.server.Server(api=api)
            info = server.server_status()
            return True

        except evelink.api.APIError as error:
            logger.exception ('ERROR: check_if_api_server_online - %s ' % error)
        except:
            return False

        return False

    @staticmethod
    def check_if_id_is_corp(corp_id):
        try:
            api = EveApiManager.api_evelint()
            corp = evelink.corp.Corp(api=api)
            corpinfo = corp.corporation_sheet(corp_id=int(corp_id))
            results = corpinfo[0]
            return True
        except evelink.api.APIError as error:
            logger.exception ('ERROR: check_if_id_is_corp %s - %s ' % (corp_id, error))

        return False

    # @staticmethod
    # def get_alliance_standings():
    #     if settings.ALLIANCE_EXEC_CORP_ID != "":
    #         try:
    #             api = evelink.api.API(api_key=(settings.ALLIANCE_EXEC_CORP_ID, settings.ALLIANCE_EXEC_CORP_VCODE))
    #             corp = evelink.corp.Corp(api=api)
    #             corpinfo = corp.contacts()
    #             results = corpinfo[0]
    #             return results
    #         except evelink.api.APIError as error:
    #             print ('ERROR: get_alliance_standings - %s ' % error)
    #
    #     return {}

    @staticmethod
    def check_if_id_is_alliance(alliance_id):
        try:
            api = EveApiManager.api_evelint()
            eve = evelink.eve.EVE(api=api)
            alliance = eve.alliances()
            results = alliance[0][int(alliance_id)]
            if results:
                return True
        except evelink.api.APIError as error:
            logger.exception ('ERROR: check_if_id_is_alliance - %s ' % error)

        return False

    @staticmethod
    def check_if_id_is_character(character_id):
        try:
            api = EveApiManager.api_evelint()
            eve = evelink.eve.EVE(api=api)
            results = eve.character_info_from_id(character_id)
            if results:
                return True
        except evelink.api.APIError as error:
            logger.exception ('ERROR: check_if_id_is_character %s - %s ' % (character_id, error))

        return False

    @staticmethod
    def get_station_list():
        try:
            api = EveApiManager.api_evelint()
            eve = evelink.eve.EVE(api=api)
            list = eve.conquerable_stations().result
            return list
        except evelink.api.APIError as error:
            logger.exception ('ERROR: get_station_list %s ' % error)
            return {}

    @staticmethod
    def get_id_for_name(name):
        try:
            api = EveApiManager.api_evelint()
            eve = evelink.eve.EVE(api=api)
            ids = eve.character_id_from_name(name)
            return ids.result
        except evelink.api.APIError as error:
            logger.exception ('ERROR: get_id_for_name %s %s ' , name, error)
            return {}
