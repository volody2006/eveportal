# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function, absolute_import, division

from eve_db.models import CrpNPCCorporation, StaStationType, MapSolarSystem
from evemail.models import MailingLists
from eveonline.eve_crest_manager import EveGrest
from datetime import datetime
from django.utils.timezone import make_aware
from eveonline.eve_api_manager import EveApiManager
from eveonline.models import Corporation, Alliance, Character, ApiKeyPair, CharacterCorpHistory, ConquerableStationList
import logging
from xml.etree import ElementTree
from util.models import BadCorp
_xml_error = getattr(ElementTree, 'ParseError', None)
if _xml_error is None:
    import xml.parsers.expat

    _xml_error = xml.parsers.expat.ExpatError


logger = logging.getLogger(__name__)


class Error(Exception):
    pass


class EveManager:
    def __init__(self):
        pass

    def update_or_create_alliance(self, alliance_id):

        logger.info(u'update_or_create_alliance: %s' % alliance_id)
        if Alliance.objects.filter(alliance_id=alliance_id).exists():
            alliance = Alliance.objects.get(alliance_id=alliance_id)
            # удаленые альянсы не обновляем!!!
            if alliance.is_active is False:
                return alliance

        if not EveApiManager.check_if_api_server_online():
            print('ОШИБКА апи сервера, останавливаемся')
            return False

        alliance_info = EveApiManager.get_alliance_information(alliance_id)

        if alliance_info is False:
            if Alliance.objects.filter(alliance_id=alliance_id).exists():
                return Alliance.objects.get(alliance_id=alliance_id)
                #     alliance.is_active = False
                #     alliance.save()
            return False

        executor_corp, created_exe_corp = Corporation.objects.get_or_create(corporation_id=alliance_info['executor_id'])

        alliance, created = Alliance.objects.update_or_create(
            alliance_id=alliance_id,
            alliance_name=alliance_info['name'].encode("utf-8"),
            defaults=dict(
                alliance_ticker=alliance_info['ticker'],
                member_count=alliance_info['member_count'],
                executor_corp=executor_corp)
        )

        if created_exe_corp is True:
            self.update_or_create_corporation(alliance_info['executor_id'])

        return alliance

    def update_or_create_corporation(self, corporation_id):

        logger.info(u'update_or_create_corporation %s' % corporation_id)
        corporation = False
        if Corporation.objects.filter(corporation_id=corporation_id).exists():

            corporation = Corporation.objects.get(corporation_id=corporation_id)

            # НПС и удаленые корпорации не обновляем!!!
            if corporation.is_del is True:
                return corporation
            if corporation.is_npc is True:
                return corporation

            if corporation.evewho is False:
                from eveonline.tasks import parse_corp_evewho
                parse_corp_evewho.delay(corporation_id)

        if not EveApiManager.check_if_api_server_online():
            print('ОШИБКА апи сервера, останавливаемся')
            return False

        try:
            corp_info = EveApiManager.get_corporation_information(corporation_id)
        except _xml_error as e:
            corp, created = BadCorp.objects.get_or_create(corp_id=corporation_id)
            corp.error_text = 'get_corporation_information. %s' % e
            corp.save()
            return corporation

        if corp_info is False:
            return False

        alliance_id = corp_info['alliance']['id']

        if alliance_id is None:
            alliance = None
        else:
            if Alliance.objects.filter(alliance_id=alliance_id).exists():
                alliance = Alliance.objects.get(alliance_id=alliance_id)
            else:
                alliance = self.update_or_create_alliance(alliance_id)

        ceo, ceo_created = Character.objects.update_or_create(
            character_id=corp_info['ceo']['id'],
            defaults=dict(character_name=corp_info['ceo']['name'])
        )
        ticker = corp_info['ticker']
        is_del = False
        if corp_info['members']['current'] == 0:
            is_del = True
        if corp_info['ticker'] is None:
            ticker = ' '
            is_del = True
        corporation_name = corp_info['name'].encode('utf-8')

        corporation, corp_created = Corporation.objects.update_or_create(
            corporation_id=corporation_id,
            defaults=dict(
                corporation_name=corporation_name,
                corporation_ticker=ticker,
                member_count=corp_info['members']['current'],
                alliance=alliance,
                ceo=ceo,
                tax=corp_info['tax_percent'],
                url=corp_info['url'],
                shares=corp_info['shares'],
                description=corp_info['description'],
                is_del=is_del,
            )
        )

        if ceo_created is True:
            self.update_or_create_character(corp_info['ceo']['id'])

        if corp_info['ceo']['id'] == 1:
            corporation.is_del = True
            corporation.save()

        if CrpNPCCorporation.objects.filter(pk=corporation_id).exists():
            # Проверям принадлежность к НПС корпорации.
            corporation = Corporation.objects.get(corporation_id=corporation_id)
            corporation.is_npc = True
            corporation.is_del = False
            corporation.save()
            ceo = Character.objects.get(character_id=corp_info['ceo']['id'])
            ceo.is_npc = True
            ceo.is_del = False
            ceo.corporation = corporation
            ceo.save()

        return corporation

    def update_or_create_character(self, character_id):
        # Error 90000457 90002409
        logger.info(u'update_or_create_character %s', character_id)
        if Character.objects.filter(character_id=character_id).exists():
            character = Character.objects.get(character_id=character_id)
            if character.is_del is True:
                return character
            if character.is_npc is True:
                return character
                # if character.is_citizen is True:
                #     return character

        # 2100602090
        if not EveApiManager.check_if_api_server_online():
            print('ОШИБКА апи сервера, останавливаемся')
            return False

        character_info = EveApiManager.get_character_from_id(character_id)
        if character_info is False:
            if Character.objects.filter(character_id=character_id).exists():
                character = Character.objects.get(character_id=character_id)
                # character.is_del = True
                # character.save()
                return character
            return False
        name = character_info['name'].encode("utf-8")  # .decode("utf-8")
        alliance_id = character_info['alliance']['id']
        corp_id = int(character_info['corp']['id'])

        if name.endswith(str(character_id)):
            logger.debug('is citizen char: %s' % name)
            is_citizen = True
        else:
            is_citizen = False

        # Создаем альянс, если его нет 673381830
        if alliance_id is None:
            alliance = None
        else:
            if Alliance.objects.filter(alliance_id=alliance_id).exists():
                alliance = Alliance.objects.get(alliance_id=alliance_id)
            else:
                alliance = self.update_or_create_alliance(alliance_id)

        # Создаем корпу, если ее нет
        if Corporation.objects.filter(corporation_id=corp_id).exists():
            corp = Corporation.objects.get(corporation_id=corp_id)
        else:
            corp = self.update_or_create_corporation(corp_id)
            if corp is False:
                corp = Corporation.objects.create(corporation_id=character_info['corp']['id'],
                                                  corporation_name=character_info['corp']['name'],
                                                  is_npc=True)

        character, created = Character.objects.update_or_create(
            character_id=character_id,
            defaults=dict(character_name=name,
                          corporation=corp,
                          alliance=alliance,
                          is_citizen=is_citizen)
        )

        # Обновляем корпоративную историю чара
        corps_history = character_info['history']
        self.character_corp_history(character, corps_history)
        # try:
        #     # logger.info(character, corps_history)
        #     self.character_corp_history(character, corps_history)
        # except MultipleObjectsReturned:
        #     char_history = CharacterCorpHistory.objects.filter(character=character)
        #     char_history.delete()
        #     self.character_corp_history(character, corps_history)

        return character

    def character_corp_history(self, character, corps_history):
        logger.debug('char % s, corp_history %s', character, corps_history)
        corp_created = False
        end_date = None

        for corp_history in corps_history:
            try:
                start_date = make_aware(datetime.utcfromtimestamp(corp_history['start_ts']))
            except:
                start_date = None
            corp_history['end_date'] = end_date
            corp_history['start_date'] = start_date
            end_date = start_date

        corps_history.reverse()
        for corp_history in corps_history:

            corp, corp_created = Corporation.objects.get_or_create(
                corporation_id=corp_history['corp_id'],
                defaults={'corporation_name': corp_history['corp_name']})
            try:
                corp_temp, cread = CharacterCorpHistory.objects.update_or_create(character=character,
                                                                                 corporation=corp,
                                                                                 start_date=corp_history['start_date'],
                                                                                 defaults=dict(
                                                                                     end_date=corp_history['end_date']))
            except:
                # Случилась неприятность, удаляем дубли
                char_history = CharacterCorpHistory.objects.filter(
                    character=character, corporation=corp, start_date=corp_history['start_date'])
                char_history.delete()
                corp_temp, cread = CharacterCorpHistory.objects.update_or_create(character=character, corporation=corp,
                                                                                 start_date=corp_history['start_date'],
                                                                                 defaults=dict(
                                                                                     end_date=corp_history['end_date']))
            if corp_created:
                self.update_or_create_corporation(corp.corporation_id)

    def get_or_created_corporation(self, corporation_id):
        if Corporation.objects.filter(corporation_id=corporation_id).exists():
            return Corporation.objects.get(corporation_id=corporation_id)
        else:
            return self.update_or_create_corporation(corporation_id)

    # @staticmethod
    def check_access_bit(accessmask, bit):
        """ Returns a bool indicating if the bit is set in the accessmask """
        mask = 1 << bit
        return (accessmask & mask) > 0

    def conquerable_station_list(self):
        lists = EveApiManager.get_station_list()
        for id in lists:
            station = lists[id]
            if station['corp']['id'] == None:
                corporation = None
            else:
                corporation = self.get_or_created_corporation(station['corp']['id'])
            obj, creade = ConquerableStationList.objects.update_or_create(
                id=id,
                defaults=dict(name=station['name'],
                              type=StaStationType.objects.get(pk=station['type_id']),
                              system=MapSolarSystem.objects.get(pk=station['system_id']),
                              corporation=corporation,
                              ))

    # @staticmethod
    def create_api_keypair(self, api_id, api_key, user):
        # Not Test

        if not EveApiManager.check_if_api_server_online():
            print('ОШИБКА апи сервера, останавливаемся')
            return False

        api_info = EveApiManager.get_api_info(api_id, api_key)
        mask = api_info['access_mask']
        if not api_info:
            return False
        if api_info['type'] == 'corp':
            if mask > 0:
                corp_info = EveApiManager.get_corporation_information(api_id=api_id, api_key=api_key)
            corporation = self.update_or_create_corporation(
                api_info['characters'][api_info['characters'].keys()[0]]['corp']['id'])
        else:
            corporation = None

        expire = api_info['expire_ts']
        if expire != None:
            expire = make_aware(datetime.utcfromtimestamp(expire))
        obj, creade = ApiKeyPair.objects.update_or_create(
            api_id=api_id,
            api_key=api_key,
            defaults=dict(
                user=user,
                active=True,
                mask=mask,
                expire=expire,
                corporation=corporation,
                type=api_info['type']
            )
        )
        #    if api_info['type'] == 'account':
        for char_id in api_info['characters']:

            if Character.objects.filter(pk=char_id).exists():
                char = Character.objects.get(pk=char_id)
                char.api.add(obj)
                char.user = user
                char.save()
            else:
                char = self.update_or_create_character(char_id)
                char.api.add(obj)
                char.user = user
                char.save()

            if user.main_char is None:
                user.main_char = Character.objects.get(pk=char_id)
                user.save()
        return obj

    def delete_api_key_pair_user(self, api_id, user):
        if ApiKeyPair.objects.filter(api_id=api_id, user=user).exists():
            api = ApiKeyPair.objects.filter(api_id=api_id, user=user)
            api.delete()
            for char in Character.objects.filter(user=user):
                if char.api.count() == 0:
                    char.user = None
                    char.save()
            return True
        else:
            return False

    # @staticmethod
    # def create_characters_from_list(chars, user, api_id):
    #
    #     for char in chars.result:
    #         #  if not EveManager.check_if_character_exist(chars.result[char]['name']):
    #         EveManager.create_character(id=chars.result[char]['id'],
    #                                     name=chars.result[char]['name'],
    #                                     corp_id=chars.result[char]['corp']['id'],
    #                                     corp_name=chars.result[char]['corp']['name'],
    #                                     corp_ticker=EveApiManager.get_corporation_ticker_from_id(
    #                                         chars.result[char]['corp']['id']),
    #                                     alliance_id=chars.result[char]['alliance']['id'],
    #                                     alliance_name=chars.result[char]['alliance']['name'],
    #                                     user=user,
    #                                     api_id=api_id)

    @staticmethod
    def get_api_key_pairs(user):
        if ApiKeyPair.objects.filter(user=user).exists():
            return ApiKeyPair.objects.filter(user=user)

    @staticmethod
    def get_char(char_id):
        if Character.objects.filter(character_id=char_id).exists():
            return Character.objects.get(character_id=char_id)
        else:
            evemanager = EveManager()
            evemanager.update_or_create_character(char_id)
            return Character.objects.get(character_id=char_id)

    @staticmethod
    def get_mail_list(list_id):
        object, creadet = MailingLists.objects.get_or_create(list_id=list_id)
        return object

    def get_char_or_corp_is_name(self, name):
        id = EveApiManager.get_id_for_name(name)
        if id is None:
            return False
        char = self.update_or_create_character(id)
        if char:
            return char
        corp = self.update_or_create_corporation(id)
        if corp:
            return corp
        return False


class GrestManagers():
    def __init__(self):
        self.grest = EveGrest()
        self.evemanager = EveManager()

    def _update_or_create_alliance(self, alliance_id):
        # Не активные альянсы не обнавляем
        if Alliance.objects.filter(alliance_id=alliance_id).exists():
            alliance = Alliance.objects.get(alliance_id=alliance_id)
            if alliance.is_active is False:
                return alliance

        alliance_info = self.grest.alliance(alliance_id)

        ts = self.grest.parse_ts(alliance_info['startDate'])
        startDate = make_aware(datetime.utcfromtimestamp(ts))

        # logger.debug(alliance_info)


        # executor Corporation
        if Corporation.objects.filter(corporation_id=alliance_info['executorCorporation']['id']).exists():
            executor_corp = Corporation.objects.get(corporation_id=alliance_info['executorCorporation']['id'])
        else:
            executor_corp = self.evemanager.update_or_create_corporation(alliance_info['executorCorporation']['id'])

        # creator Corporation
        if Corporation.objects.filter(corporation_id=alliance_info['creatorCorporation']['id']).exists():
            creator_corp = Corporation.objects.get(corporation_id=alliance_info['creatorCorporation']['id'])
        else:
            creator_corp = self.evemanager.update_or_create_corporation(alliance_info['creatorCorporation']['id'])

        # creator_character
        if Character.objects.filter(character_id=alliance_info['creatorCharacter']['id']).exists():
            creator_character = Character.objects.get(character_id=alliance_info['creatorCharacter']['id'])
        else:
            creator_character = self.evemanager.update_or_create_character(alliance_info['creatorCharacter']['id'])
        is_active = True
        if alliance_info['deleted'] == True:
            is_active = False

        alliance, created = Alliance.objects.update_or_create(
            alliance_id=alliance_id,
            defaults=dict(
                alliance_name=alliance_info['name'],
                alliance_ticker=alliance_info['shortName'],
                #                member_count = alliance_info['member_count'],
                is_active=is_active,
                executor_corp=executor_corp,
                url=alliance_info['url'],
                description=alliance_info['description'],
                creator_corp=creator_corp,
                creator_character=creator_character,
                start_date=startDate

            )
        )

        if is_active is False:
            alliance.member_count = 0
            alliance.save()
            if Corporation.objects.filter(alliance=alliance_id).exists():
                corps = Corporation.objects.filter(alliance=alliance_id)
                for corp in corps:
                    corp.alliance = None
                    corp.save()
                    logger.info(u'Корпорация %s больше не в альянсе, обновляем информацию' % corp.corporation_name)
                    self.evemanager.update_or_create_corporation(corp.corporation_id)

        return alliance
