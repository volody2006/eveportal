# -*- coding: utf-8 -*-
import datetime

from django.utils.translation import ugettext as _
from eveonline.models import Character, Alliance, Corporation
from haystack import indexes
from celery_haystack.indexes import CelerySearchIndex

class CharacterIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='character_name')
   # corporation = indexes.CharField(model_attr='corporation')
    update = indexes.DateTimeField(model_attr='update')

    def get_model(self):
        return Character

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()


class AllianceIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='alliance_name')
    ticker = indexes.CharField(model_attr='alliance_ticker')
    description_or_none = indexes.CharField(model_attr='description_or_none')
    update = indexes.DateTimeField(model_attr='update')

    def get_model(self):
        return Alliance

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()


class CorporationIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='corporation_name')
    ticker = indexes.CharField(model_attr='corporation_ticker')
    description_or_none = indexes.CharField(model_attr='description_or_none')
    update = indexes.DateTimeField(model_attr='update')

    def get_model(self):
        return Corporation

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()