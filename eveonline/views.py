# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function, absolute_import, division
import logging
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.generic import TemplateView, ListView, DetailView
from eveonline.models import Character, CharacterCorpHistory, Corporation, Alliance
from eveonline.tasks import task_update_or_create_corporation, task_update_or_create_character
from eveportal import settings
from util.managers import date_update

logger = logging.getLogger(__name__)

if settings.DEBUG:
    _period = 0
else:
    _period = settings.UPDATE_PERIOD_CHAR_AND_CORP

def run_task_update_or_create_character(character_id):
    try:
        task_update_or_create_character.delay(character_id)
    except Exception:
        logger.warning('Таски не работают')


def run_task_update_or_create_corporation(corporation_id):
    try:
        task_update_or_create_corporation.delay(corporation_id)
    except Exception:
        logger.warning('Таски не работают')


# Главная страница
class IndexView(TemplateView):
    template_name = 'public/index.html'


class CharactersList(ListView):
    model = Character
    paginate_by = 10


class CorporationsList(ListView):
    model = Corporation
    try:
        queryset = Corporation.objects.filter(is_del=False,
                                              is_npc=False).exclude(ceo=Character.objects.get(pk=1))
    except:
        queryset = Corporation.objects.all()
    paginate_by = 10


class AlliancesList(ListView):
    model = Alliance
    queryset = Alliance.objects.filter(is_active=True)
    paginate_by = 10


class CharacterDetailView(DetailView):
    queryset = Character.objects.all()

    def run_task(self):
        task_update_or_create_character.delay(self.kwargs['pk'])

    def get_queryset(self):

        character_id = self.kwargs['pk']
        if not Character.objects.filter(character_id=character_id).exists():
            logger.info('create character %s', character_id)
            run_task_update_or_create_character(character_id)
        character = get_object_or_404(Character, character_id=character_id)

        if not character.is_del and not character.is_citizen and not character.is_npc:
            if date_update(ts=character.update, period=_period):
                logger.info('update character %s', character)
                run_task_update_or_create_character(character_id)

        return Character.objects.filter(character_id=character_id)

    def get_context_data(self, **kwargs):
        context = super(CharacterDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        if self.object.is_del or self.object.is_npc:
            return context

        context['corphistory'] = CharacterCorpHistory.objects.filter(character=self.object)
        return context


class CorporationDetailView(DetailView):
    queryset = Corporation.objects.all()

    def get_queryset(self):
        corporation_id = self.kwargs['pk']
        if not Corporation.objects.filter(corporation_id=corporation_id).exists():
            run_task_update_or_create_corporation(corporation_id)
        corporation = get_object_or_404(Corporation, corporation_id=corporation_id)
        if not corporation.is_del and not corporation.is_npc:
            if date_update(ts=corporation.update, period=_period):
                logger.info('update corp %s', corporation)
                run_task_update_or_create_corporation(corporation_id)

        return Corporation.objects.filter(corporation_id=corporation_id)

    def get_context_data(self, **kwargs):
        context = super(CorporationDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        if self.object.is_del or self.object.is_npc:
            return context
        return context


class AllianceDetailView(DetailView):
    queryset = Alliance.objects.all()

    def get_context_data(self, **kwargs):
        context = super(AllianceDetailView, self).get_context_data(**kwargs)

        if not self.object.is_active or self.object.member_count == 0:
            return context

        context['corps'] = Corporation.objects.filter(alliance=self.object)
        return context

