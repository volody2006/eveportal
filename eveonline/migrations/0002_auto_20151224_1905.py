# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='character',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='apikeypair',
            name='corporation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Corporation', null=True),
        ),
        migrations.AddField(
            model_name='apikeypair',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='alliance',
            name='creator_character',
            field=models.ForeignKey(related_name='creator_char', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Character', null=True),
        ),
        migrations.AddField(
            model_name='alliance',
            name='creator_corp',
            field=models.ForeignKey(related_name='creator_corp', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Corporation', null=True),
        ),
        migrations.AddField(
            model_name='alliance',
            name='executor_corp',
            field=models.ForeignKey(related_name='executor_corp', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Corporation', null=True),
        ),
    ]
