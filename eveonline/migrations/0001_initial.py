# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('eve_db', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Alliance',
            fields=[
                ('alliance_id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('alliance_name', models.CharField(max_length=254)),
                ('alliance_ticker', models.CharField(max_length=254)),
                ('member_count', models.IntegerField(default=0)),
                ('is_active', models.BooleanField(default=True)),
                ('url', models.URLField(max_length=254, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('start_date', models.DateTimeField(null=True, verbose_name=b'start date/time', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
            ],
            options={
                'ordering': ['alliance_name'],
            },
        ),
        migrations.CreateModel(
            name='ApiKeyPair',
            fields=[
                ('api_id', models.IntegerField(serialize=False, primary_key=True)),
                ('api_key', models.CharField(unique=True, max_length=254)),
                ('mask', models.BigIntegerField(default=0, verbose_name=b'Access Mask')),
                ('active', models.BooleanField(default=True, verbose_name=b'Active')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('expire', models.DateTimeField(default=None, null=True, blank=True)),
                ('type', models.CharField(max_length=254, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Character',
            fields=[
                ('character_id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('character_name', models.CharField(max_length=254)),
                ('is_npc', models.BooleanField(default=False)),
                ('is_del', models.BooleanField(default=False)),
                ('is_citizen', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('alliance', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Alliance', null=True)),
                ('api', models.ManyToManyField(to='eveonline.ApiKeyPair')),
            ],
            options={
                'ordering': ['character_name'],
            },
        ),
        migrations.CreateModel(
            name='CharacterCorpHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date', models.DateTimeField(default=None, null=True, blank=True)),
                ('end_date', models.DateTimeField(default=None, null=True, blank=True)),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('character', models.ForeignKey(to='eveonline.Character')),
            ],
        ),
        migrations.CreateModel(
            name='ConquerableStationList',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=254)),
            ],
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Corporation',
            fields=[
                ('corporation_id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('corporation_name', models.CharField(max_length=254, null=True, blank=True)),
                ('corporation_ticker', models.CharField(max_length=254, null=True, blank=True)),
                ('member_count', models.IntegerField(default=0)),
                ('tax', models.DecimalField(default=0, max_digits=28, decimal_places=2)),
                ('description', models.TextField(null=True, blank=True)),
                ('url', models.URLField(max_length=254, null=True, blank=True)),
                ('shares', models.BigIntegerField(default=0)),
                ('is_npc', models.BooleanField(default=False)),
                ('is_del', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('alliance', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Alliance', null=True)),
                ('ceo', models.ForeignKey(related_name='corp_ceo', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Character', null=True)),
            ],
            options={
                'ordering': ['corporation_name'],
            },
        ),
        migrations.CreateModel(
            name='SystemSovereignty',
            fields=[
                ('system', models.OneToOneField(primary_key=True, serialize=False, to='eve_db.MapSolarSystem')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('belts', models.IntegerField(null=True, blank=True)),
                ('ice_belts', models.IntegerField(null=True, blank=True)),
                ('alliance', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Alliance', null=True)),
                ('corp', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Corporation', null=True)),
                ('faction', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eve_db.ChrFaction', null=True)),
            ],
            options={
                'ordering': ['system'],
            },
        ),
        migrations.AddField(
            model_name='conquerablestationlist',
            name='corporation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Corporation', null=True),
        ),
        migrations.AddField(
            model_name='conquerablestationlist',
            name='system',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eve_db.MapSolarSystem', null=True),
        ),
        migrations.AddField(
            model_name='conquerablestationlist',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eve_db.StaStationType', null=True),
        ),
        migrations.AddField(
            model_name='charactercorphistory',
            name='corporation',
            field=models.ForeignKey(to='eveonline.Corporation'),
        ),
        migrations.AddField(
            model_name='character',
            name='corporation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.Corporation', null=True),
        ),
    ]
