# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-03 18:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0010_auto_20160213_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alliance',
            name='alliance_name',
            field=models.CharField(db_index=True, max_length=254),
        ),
        migrations.AlterField(
            model_name='character',
            name='character_name',
            field=models.CharField(db_index=True, max_length=254),
        ),
        migrations.AlterField(
            model_name='corporation',
            name='corporation_name',
            field=models.CharField(db_index=True, default=' ', max_length=254),
        ),
    ]
