# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0006_auto_20160119_0850'),
    ]

    operations = [
        migrations.AddField(
            model_name='alliance',
            name='evewho',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='alliance',
            name='zkillboard',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='character',
            name='evewho',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='character',
            name='zkillboard',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='corporation',
            name='evewho',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='corporation',
            name='zkillboard',
            field=models.BooleanField(default=False),
        ),
    ]
