# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0007_auto_20160121_0550'),
    ]

    operations = [
        migrations.AlterField(
            model_name='corporation',
            name='corporation_ticker',
            field=models.CharField(default=b' ', max_length=254),
        ),
    ]
