# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_core_accounts'),
        ('eveonline', '0002_auto_20151224_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='corporation',
            name='account',
            field=models.ForeignKey(related_name='corp_account', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounts.Account', null=True),
        ),
    ]
