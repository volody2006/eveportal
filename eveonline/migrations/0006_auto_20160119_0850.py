# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0005_auto_20160109_0132'),
    ]

    operations = [
        migrations.AlterField(
            model_name='corporation',
            name='url',
            field=models.TextField(null=True, blank=True),
        ),
    ]
