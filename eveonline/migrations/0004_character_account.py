# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_core_accounts'),
        ('eveonline', '0003_corporation_account'),
    ]

    operations = [
        migrations.AddField(
            model_name='character',
            name='account',
            field=models.ForeignKey(related_name='char_account', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounts.Account', null=True),
        ),
    ]
