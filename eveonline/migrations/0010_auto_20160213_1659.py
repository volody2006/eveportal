# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-13 16:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0009_auto_20160206_2136'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='charactercorphistory',
            options={'ordering': ['-id']},
        ),
        migrations.AlterField(
            model_name='alliance',
            name='url',
            field=models.TextField(blank=True, null=True),
        ),
    ]
