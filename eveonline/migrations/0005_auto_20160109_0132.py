# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0004_character_account'),
    ]

    operations = [
        migrations.AlterField(
            model_name='character',
            name='api',
            field=models.ManyToManyField(to='eveonline.ApiKeyPair', null=True),
        ),
    ]
