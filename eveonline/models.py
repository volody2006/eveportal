# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from accounts.models import Account
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from eve_db.models import ChrFaction, MapSolarSystem, StaStationType, MapSolarSystemJump
import caching.base
from eveonline.eve_api_manager import EveApiManager
from eveportal import settings
from django.utils.translation import ugettext as _
from django.utils.encoding import smart_unicode, python_2_unicode_compatible


@python_2_unicode_compatible
class Character(models.Model):
    character_id = models.BigIntegerField(primary_key=True)
    character_name = models.CharField(max_length=254, db_index=True)
    corporation = models.ForeignKey('Corporation', blank=True, null=True, on_delete=models.SET_NULL, db_index=True)
    alliance = models.ForeignKey('Alliance', blank=True, null=True, on_delete=models.SET_NULL, db_index=True)
    api = models.ManyToManyField('ApiKeyPair', null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    is_npc = models.BooleanField(default=False)
    is_del = models.BooleanField(default=False)
    is_citizen = models.BooleanField(default=False)

    #Верификация предохраняет угон персонажа, не дает использовать ключи другому юзеру.
    # verified = models.BooleanField(default=False)
    verified_user = models.ForeignKey(settings.AUTH_USER_MODEL,
                                      blank=True, null=True,
                                      on_delete=models.SET_NULL, related_name='verified_user_char')

    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)

    account = models.ForeignKey(Account, blank=True, null=True, on_delete=models.SET_NULL,
                                related_name='char_account')
    # Временные флаги, парсилки
    evewho = models.BooleanField(default=False)
    zkillboard = models.BooleanField(default=False)

    class Meta:
        ordering = ['character_name']

    def __str__(self):
        return self.character_name
    # def __unicode__(self):
    #     return unicode(self.character_name)

    # def __unicode__(self):
    #
    #     return smart_unicode(self.character_name)

    def created_account(self):
        self.account = Account.objects.create(primary_user=self.user)
        self.save()
        return self.account


    def get_account(self):
        if self.account:
            if self.account.primary_user != self.user:
                return self.created_account()
            return self.account
        return self.created_account()


    def corp_account(self):
        if self.corporation:
            return self.corporation.get_account()
        return False


    @property
    def corp(self):
        return Corporation.objects.filter(pk=self.corporation_id or 0).first()

    @property
    def is_main(self):
        return self.user.main_char

    @property
    def name(self):
        return self.character_name

    @property
    def is_ceo(self):
        if int(self.character_id) == int(self.corporation.ceo.character_id):
            return True
        else:
            return False

    def get_absolute_url(self):
        return reverse('char_info_public', args=[self.pk,])


    def api_char_access(self, macka):
        # 93493316
        for api in self.api.filter(corporation=None, active=True):
            if api.expire:
                if api.expire<timezone.now():
                    api.active=False
                    api.save()
                    # ключ просрочен? пропускаем
                    continue
            if api.check_access(macka):
                # Получили ключ с нулевой маской? Пропускаем...
                if api.mask == 0:
                    continue
                return api
        # Все ключи перебрали, ни один не подходит.
        return False

    def api_corp_access(self, macka):
        # 93493316
        for api in self.api.exclude(corporation=None).filter(active=True):
            if api.expire:
                if api.expire<timezone.now():
                    api.active=False
                    api.save()
                    # ключ просрочен? пропускаем
                    continue
            if api.check_access(macka):
                # Получили ключ с нулевой маской? Пропускаем...
                if api.mask == 0:
                    continue
                return api
        # Все ключи перебрали, ни один не подходит.
        return False

    def valid(self):
        if self.corporation.is_del or self.corporation.is_npc:
            return False
        return True

# = 946568705
@python_2_unicode_compatible
class Alliance(models.Model):
    alliance_id = models.BigIntegerField(primary_key=True)
    alliance_name = models.CharField(max_length=254, db_index=True)
    alliance_ticker = models.CharField(max_length=254)
    executor_corp = models.ForeignKey('Corporation', blank=True, null=True, on_delete=models.SET_NULL, related_name = 'executor_corp')
    member_count = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)
    url = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    creator_character = models.ForeignKey(Character, blank=True, null=True, on_delete=models.SET_NULL, related_name = 'creator_char')
    creator_corp = models.ForeignKey('Corporation', blank=True, null=True, on_delete=models.SET_NULL, related_name = 'creator_corp')
    start_date = models.DateTimeField('start date/time', blank=True, null=True)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)

    # Временные флаги, парсилки
    evewho = models.BooleanField(default=False)
    zkillboard = models.BooleanField(default=False)

    class Meta:
        ordering = ['alliance_name']


    def __str__(self):
        return self.alliance_name
    #
    # def __str__(self):
    #     if self.alliance_name is None:
    #         return ''
    #     return '%s' % self.alliance_name

    @property
    def name(self):
        return self.alliance_name

    @property
    def description_or_none(self):
        if self.description is None:
            return ''
        return self.description


    @property
    def ticker(self):
        return self.alliance_ticker

    def get_absolute_url(self):
        return reverse('alliance_info', args=[self.pk,])

    def get_url(self):
        if self.url:
            if self.url == 'http://' or self.url == 'http://none':
                return None
            else:
                return self.url
        return None

@python_2_unicode_compatible
class Corporation(models.Model):
    corporation_id = models.BigIntegerField(primary_key=True)
    corporation_name = models.CharField(max_length=254, default=' ', db_index=True)
    corporation_ticker = models.CharField(max_length=254, default=' ')
    member_count = models.IntegerField(default=0)
    alliance = models.ForeignKey(Alliance, blank=True, null=True, on_delete=models.SET_NULL)
    tax = models.DecimalField(max_digits=28, decimal_places=2, default=0)
    description = models.TextField(blank=True, null=True)
    ceo = models.ForeignKey(Character, blank=True, null=True, on_delete=models.SET_NULL, related_name = 'corp_ceo')
    url = models.TextField(blank=True, null=True)
    shares = models.BigIntegerField(default=0)
    is_npc = models.BooleanField(default=False)
    is_del = models.BooleanField(default=False)

    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)

    account = models.ForeignKey(Account, blank=True, null=True, on_delete=models.SET_NULL, related_name = 'corp_account')

    # Временные флаги, парсилки
    evewho = models.BooleanField(default=False)
    zkillboard = models.BooleanField(default=False)

    class Meta:
        ordering = ['corporation_name']

    def __str__(self):
        return self.corporation_name

    # def __str__(self):
    #     if self.corporation_name is None:
    #         return ''
    #     return self.corporation_name

    @property
    def name(self):
        return self.corporation_name

    @property
    def ticker(self):
        return self.corporation_ticker

    def get_url(self):
        if self.url:
            if self.url == 'http://' or self.url == 'http://none':
                return None
            else:
                return self.url
        return None


    def get_absolute_url(self):
        return reverse('corp_info', args=[self.pk,])


    def created_account(self):
        self.account = Account.objects.create()

        self.save()
        return self.account

    def get_account(self):
        if self.is_npc:
            return False
        if self.is_del:
            return False
        if self.account:
            return self.account
        return self.created_account()


    @property
    def description_or_none(self):
        if self.description is None:
            return ''
        return self.description

@python_2_unicode_compatible
class SystemSovereignty(models.Model):
    system = models.OneToOneField(MapSolarSystem, primary_key=True)
    faction = models.ForeignKey(ChrFaction, blank=True, null=True, on_delete=models.SET_NULL)
    alliance = models.ForeignKey(Alliance, blank=True, null=True, on_delete=models.SET_NULL)
    corp = models.ForeignKey(Corporation, blank=True, null=True, on_delete=models.SET_NULL)
  #  price_base = models.BigIntegerField(blank=True, null=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    belts = models.IntegerField(blank=True, null=True,)
    ice_belts = models.IntegerField(blank=True, null=True,)

    class Meta:
        ordering = ['system']

    def __str__(self):
        return self.system.name

    @property
    def gate(self):
        return MapSolarSystemJump.objects.filter(from_solar_system = self.system).count()

    @property
    def impasse(self):
        return self.gate > 1

    @property
    def station_list(self):
        if ConquerableStationList.objects.filter(system = self.system).exists():
            return ConquerableStationList.objects.filter(system = self.system)
        else:
            return None

    @property
    def ice(self):
        return self.ice_belts >= 1


@python_2_unicode_compatible
class ApiKeyPair(models.Model):
    api_id = models.IntegerField(primary_key=True)
    api_key = models.CharField(max_length=254, unique=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    mask = models.BigIntegerField('Access Mask', default=0)
    active = models.BooleanField('Active', default=True)
    corporation = models.ForeignKey(Corporation, blank=True, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    expire = models.DateTimeField(blank=True, null=True, default=None)
    type = models.CharField(max_length=254, blank=True, null=True,)

    @property
    def is_corp(self):
        if self.corporation:
            return True
        return False


    # @staticmethod
    # def _mask_check(accessmask, bit):
    #     """ Returns a bool indicating if the bit is set in the accessmask """
    #     mask = 1 << bit
    #     return (accessmask & mask) > 0
    # #
    # # def has_access(self, bit):
    # #     """ Checks if a specific bit is enabled in the key's access mask """
    # #     if gargoyle.is_active('eve-cak') and self.is_cak:
    # #         return self._mask_check(self.api_accessmask, bit)
    # #     else:
    # #         return True
    #
    # def check_access(self, accessmask):
    #     """ Checks if the account has equal or higher access than the bitmask provided """
    #
    #     length = 0
    #     i = accessmask
    #     while i:
    #         i >>= 1
    #         length += 1
    #
    #     for bit in range(0, length-1):
    #         if self._mask_check(accessmask, bit) and self.mask == 0: # and not self.has_access(bit):
    #             return False
    #     return True

    def check_access(self, accessmask):
        """ Checks if the account has equal or higher access than the bitmask provided """
        return (self.mask & accessmask) > 0

    def __str__(self):
        return str(self.api_id)



@python_2_unicode_compatible
class CharacterCorpHistory(models.Model):
    character = models.ForeignKey(Character, db_index=True)
    corporation = models.ForeignKey(Corporation, db_index=True)
    start_date = models.DateTimeField(blank=True, null=True, default=None)
    end_date = models.DateTimeField(blank=True, null=True, default=None)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)

    def __str__(self):
        return self.start_date

    class Meta:
        ordering = ['-id']
        unique_together = (('character', 'corporation', 'start_date'),)

@python_2_unicode_compatible
class ConquerableStationList(caching.base.CachingMixin, models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=254, default='')
    type = models.ForeignKey(StaStationType, blank=True, null=True, on_delete=models.SET_NULL)
    system = models.ForeignKey(MapSolarSystem, blank=True, null=True, on_delete=models.SET_NULL)
    corporation = models.ForeignKey(Corporation,  blank=True, null=True, on_delete=models.SET_NULL)
    #alliance = models.ForeignKey(Alliance, blank=True, null=True, on_delete=models.SET_NULL)


    def __str__(self):
        return self.name

# class ApiKeyTask(models.Model):
#     name = models.CharField(max_length=254, blank=True, null=True)
#     type = models.CharField(max_length=254, blank=True, null=True, default='')
    #
    # def __str__(self):
    #     return self.name
#
#
# class ApiKeyUpdateHistory(models.Model):
#     api = models.ForeignKey(ApiKeyPair)
#     api_task = models.ForeignKey(ApiKeyTask)
#
#     def __str__(self):
#         return self.api.api_id

