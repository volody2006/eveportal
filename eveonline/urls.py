# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _




from django.conf.urls import url
from django.views.decorators.cache import cache_page
from eveonline.views import CharactersList, CorporationsList, AlliancesList, CharacterDetailView, CorporationDetailView, \
    AllianceDetailView
from . import views

urlpatterns = [
    url(r'^character/(?P<pk>[0-9]+)/$', CharacterDetailView.as_view(), name='char_info_public'),
    url(r'^corporation/(?P<pk>[0-9]+)/$', CorporationDetailView.as_view(), name='corp_info'),
    # url(r'^corporation/(?P<corporation_id>\d+)/chars/$', views.corp_info_privat, name='corp_info_privat'),
    url(r'^alliance/(?P<pk>[0-9]+)/$', AllianceDetailView.as_view(), name='alliance_info'),
    url(r'^characters/$', cache_page(86400)(CharactersList.as_view()), name='characters_list'),
    url(r'^corporations/$', cache_page(86400)(CorporationsList.as_view()), name='corporations_list'),
    url(r'^alliances/$', cache_page(86400)(AlliancesList.as_view()), name='alliances_list'),
]


    # url(r'^alliance/(?P<alliance_id>\d+)/$', views.alliance_info, name='alliance_info'),

    # url(r'^corporation/(?P<corporation_id>\d+)/$', views.corp_info, name='corp_info'),
    # url(r'^character/(?P<character_id>\d+)/$', views.char_info_public, name='char_info_public'),
