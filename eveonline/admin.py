from django.contrib import admin



# Register your models here.
from eveonline.models import *


class CharacterAdmin(admin.ModelAdmin):
    list_display = ('character_id', 'character_name', 'corporation',
                    'alliance', 'user' )
    list_filter = ('is_npc', 'is_del', 'is_citizen', 'evewho', 'zkillboard')
    search_fields = ['character_name', 'character_id',
                     # 'alliance__alliance_name', 'user__username',
                     # 'corporation__corporation_name',  'api__api_id',
                     # 'user__email', 'corporation__corporation_id',
                     # 'alliance__alliance_id'
                     ]
    raw_id_fields = ('api', 'alliance', 'corporation', 'verified_user', 'user')

admin.site.register(Character, CharacterAdmin)


class AllianceAdmin(admin.ModelAdmin):
    list_display = ('alliance_id', 'alliance_name', 'alliance_ticker',
                    'executor_corp', 'member_count', 'is_active',)
    list_filter = ('is_active', 'evewho', 'zkillboard' )
    search_fields = ['alliance_id', 'alliance_name', 'alliance_ticker',
                     # 'executor_corp__corporation_name', 'description',
                     ]
admin.site.register(Alliance, AllianceAdmin)


class CorporationAdmin(admin.ModelAdmin):
    list_display = ('corporation_id', 'corporation_name', 'corporation_ticker',
                    'alliance', 'member_count')
    list_filter = ('is_npc', 'is_del', 'evewho', 'zkillboard', 'alliance' )
    search_fields = ['corporation_id', 'corporation_name',
                     'corporation_ticker',
                     # 'alliance__alliance_name',
                     # 'ceo__character_name', 'alliance__alliance_id', 'description'
                     ]
    raw_id_fields = ('ceo', 'alliance')
admin.site.register(Corporation, CorporationAdmin)

class SystemSovereigntyAdmin(admin.ModelAdmin):
    list_display = ('system', 'alliance', 'corp', 'update', 'created',

    )
    list_filter = ('system__region', ('alliance',  admin.RelatedOnlyFieldListFilter), )
admin.site.register(SystemSovereignty, SystemSovereigntyAdmin)

class ApiKeyPairAdmin(admin.ModelAdmin):
    list_display = ('api_id', 'user', 'mask', 'expire')
    raw_id_fields = ('user', 'corporation')
admin.site.register(ApiKeyPair, ApiKeyPairAdmin)

class CharacterCorpHistoryAdmin(admin.ModelAdmin):
    list_display = ('character', 'corporation', 'start_date', 'end_date')
    date_hierarchy = 'start_date'
    search_fields = ['character__character_name', 'corporation__corporation_name']
    raw_id_fields = ('character', 'corporation', )
admin.site.register(CharacterCorpHistory, CharacterCorpHistoryAdmin)

class ConquerableStationListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'type', 'system', 'corporation')
admin.site.register(ConquerableStationList, ConquerableStationListAdmin)