#!/usr/bin/env bash

coverage run --omit='.env/*'  manage.py test -v=3 --settings=eveportal.test_settings

coverage html -i