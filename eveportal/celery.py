# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'eveportal.settings')

app = Celery('eveportal')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
app.conf.task_default_queue = 'eveportal_default'

app.conf.update(
    task_routes = {
        # 'proj.tasks.add': {'queue': 'hipri'},
        'evebank.tasks.new_obligation': {'queue': 'eveportal_default'},
        'evebank.tasks.task_obligations': {'queue': 'eveportal_default'},
        'evemail.tasks.load_mail': {'queue': 'eveportal_default'},
        'evemail.tasks.load_notification': {'queue': 'eveportal_default'},
        'evemail.tasks.run_load_mailing_lists': {'queue': 'eveportal_default'},
        'eveonline.tasks.parse_char_zkillboard': {'queue': 'eveportal_default'},
        'eveonline.tasks.parse_corp_evewho': {'queue': 'eveportal_default'},
        'eveonline.tasks.run_api_refresh': {'queue': 'eveportal_default'},
        'eveonline.tasks.run_create_api_keypair': {'queue': 'eveportal_default'},
        'eveonline.tasks.run_new_alliance_corp_update': {'queue': 'eveportal_default'},
        'eveonline.tasks.task_evewho': {'queue': 'eveportal_default'},
        'eveonline.tasks.task_run_create_api_keypair': {'queue': 'eveportal_default'},
        'eveonline.tasks.task_update_or_create_character': {'queue': 'eveportal_default'},
        'eveonline.tasks.task_update_or_create_corporation': {'queue': 'eveportal_default'},
        'eveonline.tasks.updateEveSovereignty': {'queue': 'eveportal_default'},
        'eveonline.tasks.update_ali_info': {'queue': 'eveportal_default'},
        'eveonline.tasks.update_corp_is_del': {'queue': 'eveportal_default'},
        'eveonline.tasks.update_zkill_all': {'queue': 'eveportal_default'},
        'eveonline.tasks.update_zkill_last_hour': {'queue': 'eveportal_default'},
        'eveportal.celery.debug_task': {'queue': 'eveportal_default'},
        'rent.tasks.download_feed': {'queue': 'eveportal_default'},
        'rent.tasks.parse_feed': {'queue': 'eveportal_default'},
        'rent.tasks.run_import_feeds': {'queue': 'eveportal_default'},
        'static_sitemaps.tasks.GenerateSitemap': {'queue': 'eveportal_default'},
        'wallet.tasks.task_get_corp_divisions': {'queue': 'eveportal_default'},
        'wallet.tasks.updade_corp_wallet_api': {'queue': 'eveportal_default'},
        'wallet.tasks.updade_corps_wallets': {'queue': 'eveportal_default'},
        'wallet.tasks.update_corp_info_api': {'queue': 'eveportal_default'},
        'wallet.tasks.updating_character_wallet_api': {'queue': 'eveportal_default'},
        'wallet.tasks.updating_characters_wallets': {'queue': 'eveportal_default'},
    },
)



# CELERY_ROUTES = {
#     'evebank.tasks.new_obligation': {'queue': 'eveportal_default'},
#     'evebank.tasks.task_obligations': {'queue': 'eveportal_default'},
#     'evemail.tasks.load_mail': {'queue': 'eveportal_default'},
#     'evemail.tasks.load_notification': {'queue': 'eveportal_default'},
#     'evemail.tasks.run_load_mailing_lists': {'queue': 'eveportal_default'},
#     'eveonline.tasks.parse_char_zkillboard': {'queue': 'eveportal_default'},
#     'eveonline.tasks.parse_corp_evewho': {'queue': 'eveportal_default'},
#     'eveonline.tasks.run_api_refresh': {'queue': 'eveportal_default'},
#     'eveonline.tasks.run_create_api_keypair': {'queue': 'eveportal_default'},
#     'eveonline.tasks.run_new_alliance_corp_update': {'queue': 'eveportal_default'},
#     'eveonline.tasks.task_evewho': {'queue': 'eveportal_default'},
#     'eveonline.tasks.task_run_create_api_keypair': {'queue': 'eveportal_default'},
#     'eveonline.tasks.task_update_or_create_character': {'queue': 'eveportal_default'},
#     'eveonline.tasks.task_update_or_create_corporation': {'queue': 'eveportal_default'},
#     'eveonline.tasks.updateEveSovereignty': {'queue': 'eveportal_default'},
#     'eveonline.tasks.update_ali_info': {'queue': 'eveportal_default'},
#     'eveonline.tasks.update_corp_is_del': {'queue': 'eveportal_default'},
#     'eveonline.tasks.update_zkill_all': {'queue': 'eveportal_default'},
#     'eveonline.tasks.update_zkill_last_hour': {'queue': 'eveportal_default'},
#     'eveportal.celery.debug_task': {'queue': 'eveportal_default'},
#     'rent.tasks.download_feed': {'queue': 'eveportal_default'},
#     'rent.tasks.parse_feed': {'queue': 'eveportal_default'},
#     'rent.tasks.run_import_feeds': {'queue': 'eveportal_default'},
#     'static_sitemaps.tasks.GenerateSitemap': {'queue': 'eveportal_default'},
#     'wallet.tasks.task_get_corp_divisions': {'queue': 'eveportal_default'},
#     'wallet.tasks.updade_corp_wallet_api': {'queue': 'eveportal_default'},
#     'wallet.tasks.updade_corps_wallets': {'queue': 'eveportal_default'},
#     'wallet.tasks.update_corp_info_api': {'queue': 'eveportal_default'},
#     'wallet.tasks.updating_character_wallet_api': {'queue': 'eveportal_default'},
#     'wallet.tasks.updating_characters_wallets': {'queue': 'eveportal_default'},
#     # '': {'queue': 'eveportal_default'},
#
# }

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
