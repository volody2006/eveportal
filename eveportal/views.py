# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext as _


def index_view(request):
    # if request.user.is_authenticated():
    #     return HttpResponseRedirect(reverse("auth_characters"))
    return render_to_response('public/index.html', None, context_instance=RequestContext(request))