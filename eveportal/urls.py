from django.conf.urls import include, url
from django.contrib.flatpages.sitemaps import FlatPageSitemap


from eveonline.models import Character, Alliance, Corporation

from eveonline.views import IndexView
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from eveportal import settings
# from django.contrib.sitemaps.views import sitemap
# from django.contrib.sitemaps import views as sitemaps_views
from my_sitemaps.sitemaps import GenericSitemap



sitemap_character = {
    'queryset': Character.objects.filter(is_citizen=False, is_del=False),
    'date_field': 'update'
}
sitemap_character_cit = {
    'queryset': Character.objects.filter(is_citizen=True, is_del=False),
    'date_field': 'update'
}
sitemap_alliance = {
    'queryset': Alliance.objects.filter(is_active=True),
    'date_field': 'update',
}
sitemap_corporation = {
    'queryset': Corporation.objects.filter(is_del=False, is_npc=False),
    'date_field': 'update',
}
sitemaps = {
    'flatpages': FlatPageSitemap,
    'character': GenericSitemap(sitemap_character, priority=0.5, changefreq = 'daily'),
    'character_cit': GenericSitemap(sitemap_character_cit, priority=0.2, changefreq = 'monthly'),
    'alliance': GenericSitemap(sitemap_alliance, priority=0.5, changefreq = 'daily'),
    'corporation': GenericSitemap(sitemap_corporation, priority=0.5, changefreq = 'daily')
}




urlpatterns = [

    # url(r'^sitemap\.xml$', cache_page(86400)(sitemaps_views.index), {'sitemaps': sitemaps, 'sitemap_url_name': 'sitemaps'}),
    # url(r'^sitemap-(?P<section>.+)\.xml$', cache_page(86400)(sitemaps_views.sitemap), {'sitemaps': sitemaps}, name='sitemaps'),

    url(r'^robots\.txt', include('robots.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'', include('goto_url.urls')),
    url(r'^sitemap.xml', include('static_sitemaps.urls')),

    url(r'^i18n/', include('django.conf.urls.i18n')),

]
# i18n_patterns
urlpatterns += i18n_patterns(

        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
        url(r'^pages/', include('django.contrib.flatpages.urls')),
        url(r'^rosetta/', include('rosetta.urls')),
        url(r'^$', IndexView.as_view(), name='index'),
        url(r'^', include('dashboard.urls')),
        url(r'^eveonline/', include('eveonline.urls')),
        url(r'^', include('wallet.urls')),
        url(r'^users/', include('users.urls')),
        url(r'^profile/', include('profile.urls')),
        url(r'^rent/', include('rent.urls')),
        url(r'^hr/', include('hrapplications.urls')),
        url(r'^mail/', include('evemail.urls')),
        # url(r'^weblog/', include('zinnia.urls', namespace='zinnia')),
        url(r'^comments/', include('django_comments.urls')),
        # url(r'^dashboard/accounts/', include(accounts_app.urls)),
        url(r'^evebank/', include('evebank.urls')),
        url(r"^blog/", include("blog.urls")),
        url(r'^search/', include('search.urls')),
        url(r'^messages/', include('postman.urls', namespace='postman', app_name='postman')),

)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
