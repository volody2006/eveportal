# -*- coding: utf-8 -*-

import re
from django.core.urlresolvers import reverse_lazy
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = '%agvmen7^tnnup&x4rb6)w2os6i9rwv54s=*70b9@1a*c-7u_9'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
ALLOWED_HOSTS = ['in-un.ru', 'evebank.ru', '127.0.0.1']

ADMINS = (('Vladimir', 'senokosov.vs@gmail.com'),)
MANAGERS = ADMINS
# Application definition

INSTALLED_APPS = (
    'modeltranslation',

    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sitemaps',
    'django.contrib.flatpages',
    'django_celery_results',
    'django_celery_beat',
    'goto_url',
    'static_sitemaps',
    'robots',
    # 'celerytask',
    'haystack',
    'celery_haystack',
    'wkhtmltopdf',
    'rosetta',
    # 'djcelery',
    'eve_db',
    'bootstrap3',
    'eveonline',
    'users',
    'bootstrapform',
    'dashboard',
    'evemail',
    'profile',
    'rent',
    'util',
    'hrapplications',
    'wallet',
    'treebeard',
    'evebank',

    'blog',
    'search',
    'services',

    'django_comments',
    'django.contrib.sites',
    'mptt',
    'tagging',
    'accounts',
    'widget_tweaks',
    'postman',
    # 'zinnia',


    'my_sitemaps',
    # Forum

)

# django-static-sitemaps
STATICSITEMAPS_ROOT_SITEMAP = 'eveportal.urls.sitemaps'
STATICSITEMAPS_ROOT_DIR = os.path.join(BASE_DIR, 'static', )
STATICSITEMAPS_REFRESH_AFTER = 3600
STATICSITEMAPS_PING_GOOGLE = False

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'eveportal',
    },
}


MIDDLEWARE_CLASSES = (
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'users.middleware.ActiveUserMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

IGNORABLE_404_URLS = (
    re.compile(r'\.(php|cgi)$'),
    re.compile(r'^/phpmyadmin/'),
    re.compile(r'^/apple-touch-icon.*\.png$'),
    re.compile(r'^/favicon\.ico$'),
)

ROOT_URLCONF = 'eveportal.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],

        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.i18n',
                'dashboard.context_processors.site_name',
                'dashboard.context_processors.site_name_short',

            ],
            # 'loaders': [
            #
            #     # insert your TEMPLATE_LOADERS here
            #     'django.template.loaders.app_directories.Loader',
            # ]
        },
    },
]

WSGI_APPLICATION = 'eveportal.wsgi.application'

# Custom User
AUTH_USER_MODEL = 'users.CustomUser'
AUTHENTICATION_BACKENDS = (
    'users.backends.UsernameBackend',
    'users.backends.EmailBackend',
)

LOGIN_URL = reverse_lazy('auth_login_user')
LOGIN_REDIRECT_URL = reverse_lazy('auth_characters')

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'eveportal',
        'USER': 'eveportal',
        'PASSWORD': 'eveportal',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/




LANGUAGE_CODE = 'ru'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGES = (
    ('ru', ('Russian')),
    ('en', ('English')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)
MODELTRANSLATION_DEFAULT_LANGUAGE = 'ru'
# Rosetta
YANDEX_TRANSLATE_KEY = 'trnsl.1.1.20150604T103608Z.1b07026be52e874f.3ed98a8fcf0da87879113a03a507e0be03b5ea50'
ROSETTA_MESSAGES_PER_PAGE = 20
# ROSETTA_GOOGLE_TRANSLATE = True
ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# st = os.path.join(BASE_DIR, "static")
# STATIC_ROOT = st
# print STATIC_ROOT

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    'static',
)

STATIC_URL = '/static/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
        'new_formatter': {
            'format': '%(asctime)s %(levelname)s: %(name)s:%(filename)s:%(lineno)d:  %(message)s ',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'new_formatter',
        },
        'production_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'logs/main.log',
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 7,
            'formatter': 'new_formatter',
            'filters': ['require_debug_false'],
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'logs/main_debug.log',
            'maxBytes': 1024 * 1024 * 5,  # 5 MB
            'backupCount': 7,
            'formatter': 'new_formatter',
            'filters': ['require_debug_true'],
        },
        # 'null': {
        #     "class": 'django.utils.log.NullHandler',
        # }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'elasticsearch.trace': {
            'handlers': ['production_file', 'debug_file'],
            'level': "ERROR",
        },
        # 'django': {
        #     'handlers': ['null', ],
        # },
        # 'py.warnings': {
        #     'handlers': ['null', ],
        # },
        '': {
            'handlers': ['console', 'production_file', 'debug_file'],
            'level': "INFO",
        },
    }
}


HAYSTACK_SIGNAL_PROCESSOR = 'celery_haystack.signals.CelerySignalProcessor'


# CELERY
CELERY_SEND_TASK_ERROR_EMAILS = False
# BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERY_RESULT_BACKEND = 'django-db'
CELERY_TASK_SERIALIZER = 'json'
CELERY_BROKER_URL = 'amqp://guest:guest@localhost//'
CELERY_ACCEPT_CONTENT = ['json']
# CELERY_DEFAULT_QUEUE = "eveportal_default"


CELERY_HAYSTACK_QUEUE = 'eveportal_celery_haystack'
# CELERY_ROUTES = {
#     'evebank.tasks.new_obligation': {'queue': 'eveportal_default'},
#     'evebank.tasks.task_obligations': {'queue': 'eveportal_default'},
#     'evemail.tasks.load_mail': {'queue': 'eveportal_default'},
#     'evemail.tasks.load_notification': {'queue': 'eveportal_default'},
#     'evemail.tasks.run_load_mailing_lists': {'queue': 'eveportal_default'},
#     'eveonline.tasks.parse_char_zkillboard': {'queue': 'eveportal_default'},
#     'eveonline.tasks.parse_corp_evewho': {'queue': 'eveportal_default'},
#     'eveonline.tasks.run_api_refresh': {'queue': 'eveportal_default'},
#     'eveonline.tasks.run_create_api_keypair': {'queue': 'eveportal_default'},
#     'eveonline.tasks.run_new_alliance_corp_update': {'queue': 'eveportal_default'},
#     'eveonline.tasks.task_evewho': {'queue': 'eveportal_default'},
#     'eveonline.tasks.task_run_create_api_keypair': {'queue': 'eveportal_default'},
#     'eveonline.tasks.task_update_or_create_character': {'queue': 'eveportal_default'},
#     'eveonline.tasks.task_update_or_create_corporation': {'queue': 'eveportal_default'},
#     'eveonline.tasks.updateEveSovereignty': {'queue': 'eveportal_default'},
#     'eveonline.tasks.update_ali_info': {'queue': 'eveportal_default'},
#     'eveonline.tasks.update_corp_is_del': {'queue': 'eveportal_default'},
#     'eveonline.tasks.update_zkill_all': {'queue': 'eveportal_default'},
#     'eveonline.tasks.update_zkill_last_hour': {'queue': 'eveportal_default'},
#     'eveportal.celery.debug_task': {'queue': 'eveportal_default'},
#     'rent.tasks.download_feed': {'queue': 'eveportal_default'},
#     'rent.tasks.parse_feed': {'queue': 'eveportal_default'},
#     'rent.tasks.run_import_feeds': {'queue': 'eveportal_default'},
#     'static_sitemaps.tasks.GenerateSitemap': {'queue': 'eveportal_default'},
#     'wallet.tasks.task_get_corp_divisions': {'queue': 'eveportal_default'},
#     'wallet.tasks.updade_corp_wallet_api': {'queue': 'eveportal_default'},
#     'wallet.tasks.updade_corps_wallets': {'queue': 'eveportal_default'},
#     'wallet.tasks.update_corp_info_api': {'queue': 'eveportal_default'},
#     'wallet.tasks.updating_character_wallet_api': {'queue': 'eveportal_default'},
#     'wallet.tasks.updating_characters_wallets': {'queue': 'eveportal_default'},
#     # '': {'queue': 'eveportal_default'},
#
# }

# eve GREST

ClientID = "932882323f8449548ff4f97642fc0873"

SecretKey = "HW7r63D2DVc11CD6RqwnAUnbSadWHAp751rG6tdr"

CallbackURL = "localhost"

#################
# EMAIL SETTINGS
#################
# DOMAIN - The alliance auth domain_url
# EMAIL_HOST - SMTP Server URL
# EMAIL_PORT - SMTP Server PORT
# EMAIL_HOST_USER - Email Username
# EMAIL_HOST_PASSWORD - Email Password
# EMAIL_USE_TLS - Set to use TLS encryption
#################
DOMAIN = 'http://in-un.ru'  # os.environ.get('AA_DOMAIN', 'http://in-un.ru')
EMAIL_HOST = 'smtp.yandex.ru'  # os.environ.get('AA_EMAIL_HOST', 'smtp.gmail.com')
EMAIL_PORT = 587  # int(os.environ.get('AA_EMAIL_PORT', '587'))
EMAIL_HOST_USER = 'webmaster@in-un.ru'  # os.environ.get('AA_EMAIL_HOST_USER', 'netotveta.inun@gmail.com') #v.senokosov@yandex.ru netotveta.inun@gmail.com
EMAIL_HOST_PASSWORD = 'Eshemoba1'  # os.environ.get('AA_EMAIL_HOST_PASSWORD', 'Eshemoba1')
EMAIL_USE_TLS = 'True'  # == os.environ.get('AA_EMAIL_USE_TLS', 'True')
DEFAULT_FROM_EMAIL = 'webmaster@in-un.ru'
SERVER_EMAIL = 'webmaster@in-un.ru'

#####################
# HR Configuration
#####################
# JACK_KNIFE_URL - Url for the audit page of API Jack knife
#                  Should seriously replace with your own.
#####################
JACK_KNIFE_URL = os.environ.get('AA_JACK_KNIFE_URL', 'http://ridetheclown.com/eveapi/audit.php')

# Site name
SITE_NAME = 'EVEPortal'
SITE_NAME_SHORT = 'EPOR'

# GOOGLE Api
PROJECT_ID = 'eveportal-1147'
Project_number = '10324321673'

# OAuth client
OAuth_ID = '10324321673-po9qtvqk8vgctn1lm9hunns5l80la87g.apps.googleusercontent.com'
OAuth_Secret = 'VjASgF3angCoC3d7Eizi96AB'

# COMMENTS_APP = 'threadedcomments'
SITE_ID = 1

#########################
# Alliance Configuration
#########################
# ALLIANCE_ID - Set this to your AllianceID
# ALLIANCE_NAME - Set this to your Alliance Name
# ALLIANCE_EXEC_CORP_ID - Set this to the api id for the exec corp
# ALLIANCE_EXEC_CORP_VCODE - Set this to the api vcode for the exe corp
# ALLIANCE_BLUE_STANDING - The default lowest standings setting to consider blue
########################
ALLIANCE_ID = int(os.environ.get('AA_ALLIANCE_ID', '99005103'))
ALLIANCE_NAME = os.environ.get('AA_ALLIANCE_NAME', 'Independent Union')
ALLIANCE_TIKER = os.environ.get('AA_ALLIANCE_NAME', 'IN-UN')
ALLIANCE_EXEC_CORP_ID = os.environ.get('AA_ALLIANCE_EXEC_CORP_ID', '4505846')
ALLIANCE_EXEC_CORP_VCODE = os.environ.get('AA_ALLIANCE_EXEC_CORP_VCODE',
                                          '0MOSFmlwnzqbO5LXdZSOtj6jxWwuLsbgLVeJChG85OhGHdriGtqz1DEteXbxYcww')
ALLIANCE_BLUE_STANDING = float(os.environ.get('AA_ALLIANCE_BLUE_STANDING', '5.0'))

EVEBANK_CORP = '98368686'

OSCAR_DEFAULT_CURRENCY = ' ISK '

ACCOUNTS_MAX_ACCOUNT_VALUE = 0

CACHES = {
    'default': {
        # 'BACKEND': 'cache.backends.filebased.FileBasedCache',
        # "BACKEND": "django_redis.cache.RedisCache",
        "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        "LOCATION": "redis://127.0.0.1:6379/2",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
    },

    'redis-cache': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        # "LOCATION": "redis://127.0.0.1:6379/2",
        # "OPTIONS": {
        #     "CLIENT_CLASS": "django_redis.client.DefaultClient",
        # }
    },

    'zkillboard': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        # "LOCATION": "redis://127.0.0.1:6379/2",
        # "OPTIONS": {
        #     "CLIENT_CLASS": "django_redis.client.DefaultClient",
        # },
    },
    'evelink': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        # "LOCATION": "redis://127.0.0.1:6379/2",
        # "OPTIONS": {
        #     "CLIENT_CLASS": "django_redis.client.DefaultClient",
        # },
    },
    'evewho': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        # "LOCATION": "redis://127.0.0.1:6379/2",
        # "OPTIONS": {
        #     "CLIENT_CLASS": "django_redis.client.DefaultClient",
        # },
    }
}

# Используем во вьюхе, Значение измеряется в сутках.
UPDATE_PERIOD_CHAR_AND_CORP = 1

# https://pypi.python.org/pypi/django-robots
ROBOTS_CACHE_TIMEOUT = 60 * 60  # 1 час

try:
    from local import *
except ImportError:
    pass
