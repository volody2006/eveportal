# -*- coding: utf-8 -*-
import logging

from accounts.models import Account
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin)
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from eveonline.models import Character

logger = logging.getLogger(__name__)

class CustomUserManager(BaseUserManager):

    def create_user(self, username, email=None, password=None, **extra_fields):
        now = timezone.now()

        if not username:
            raise ValueError('The given username must be set')

        email = self.normalize_email(email)
        user = self.model(username=username,
                          email=email,
                          last_login=now,
                   #       date_created=now,
                         **extra_fields
                          )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, email, password, **extra_fields):
        user = self.create_user(username, email, password, **extra_fields)
        user.is_staff = True
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    username = models.CharField(_('username'),
                                help_text=u'Обязательно. До 50 cимволов (буквы, цифры и @/./+/-/)',
                                max_length=50,
                                unique=True)


    email = models.EmailField(_('email address'), max_length=255, unique=True, db_index=True)

    first_name = models.CharField(verbose_name=u'Имя',
                                  max_length=255,
                                  blank=True,
                                  default='')
    last_name = models.CharField(verbose_name=u'Фамилия',
                                 max_length=255,
                                 blank=True,
                                 default='')


    is_staff = models.BooleanField(
        _('staff status'), default=False, help_text=_(
            'Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(_('active'), default=True, help_text=_(
        'Designates whether this user should be treated as '
        'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    main_char = models.ForeignKey(Character, blank=True, null=True, on_delete=models.SET_NULL)

    objects = CustomUserManager()


    def is_ceo(self):
        if self.main_char:
            if self.main_char.corporation.ceo == self.main_char:
                return True
        return False

    def get_full_name(self):
        if self.main_char:
            return self.main_char.character_name
        return self.username

    def get_email(self):
        """Return the email."""
        return self.email

    def get_short_name(self):
        if self.main_char:
            return self.main_char.character_name
        return self.username

    def get_profile(self):
        """
        """
        return self

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this User."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_user_account(self):
        return Account.objects.get(primary_user=self)

    def __unicode__(self):
        return self.username

    class Meta(object):
        verbose_name = _('user')
        verbose_name_plural = _('users')


