# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from django.conf.urls import patterns, include, url
from . import views




urlpatterns = [
    url(r'^login/', views.login_user, name='auth_login_user'),
    url(r'^logout/', views.logout_user, name='auth_logout_user'),
    url(r'^register/', views.register_user_view, name='auth_register_user'),

    url(r'^password/$', views.custom_password_change, name='password_change'),
    url(r'^password/done/$', views.custom_password_change_done, name='password_change_done'),
    url(r'^password/reset/$', views.custom_password_reset, name='password_reset'),
    url(r'^password/password/reset/done/$', views.custom_password_reset_done, name='password_reset_done'),
    url(r'^password/reset/complete/$', views.custom_password_reset_complete, name='password_reset_complete'),
    url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        views.custom_password_reset_confirm, name='password_reset_confirm'),

]