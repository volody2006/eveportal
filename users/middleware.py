# coding: utf-8

from django.contrib.auth import logout
from django.utils import timezone


class ActiveUserMiddleware(object):
    def process_request(self, request):
        if not request.user.is_authenticated():
            return

        if not request.user.is_active:
            logout(request)

   #     request.user.update(last_login=timezone.now())
