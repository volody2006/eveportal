# -*- coding: utf-8 -*-
import urlparse
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import logout, password_change, password_change_done, password_reset, password_reset_done, \
    password_reset_complete, password_reset_confirm
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext as _
from eveportal import settings
from users.forms import RegistrationForm, AuthenticationForm  # LoginForm, EmailUserCreationForm
from users.models import CustomUser


def register_user_view(request):
    initial = {}
    form = RegistrationForm(request.POST or None, initial=initial)

    if form.is_valid():

        if not CustomUser.objects.filter(username=form.cleaned_data['username']).exists():
            user = CustomUser.objects.create_user(
                form.cleaned_data['username'],
                form.cleaned_data['email'],
                form.cleaned_data['password'])
            #user.backend ='blabal'
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            login(request, user)
            # print user
            # print reverse('auth_help')
            return HttpResponseRedirect(reverse('auth_help'))
        else:
            return render_to_response('register.html',
                                      {'form': form, 'error': True},
                                      context_instance=RequestContext(request))
    else:
        form = RegistrationForm()

    return render_to_response('register.html', {'form': form, 'error': False},  context_instance=RequestContext(request))


def login_user(request):
    form = AuthenticationForm()

    # redirect_to = request.GET('next', '')
    # original_next = redirect_to
    redirect_to = settings.LOGIN_REDIRECT_URL
    if request.method == 'POST':
        # validate the form
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            netloc = urlparse.urlparse(redirect_to)[1]
            # Use default setting if redirect_to is empty
            # if not redirect_to:
            #     redirect_to = settings.LOGIN_REDIRECT_URL
            # Security check -- don't allow redirection to a different host.
            # elif netloc and netloc != request.get_host():
            #     redirect_to = settings.LOGIN_REDIRECT_URL

            # входим пользователя в сайт
            user = form.get_user()
            login(request, user)

            response = redirect(redirect_to)


            return response

    context = {'form': form, 'next': redirect_to}
    # return render_to_response('public/login.html', {'form': form, 'next': redirect_to}, context_instance=RequestContext(request))
    return render_to_response('login_user.html', {'form': form, 'next': redirect_to}, context_instance=RequestContext(request))







    # if request.method == 'POST':
    #     form = AuthenticationForm(data=request.POST)
    #
    #     if form.is_valid():
    #         user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
    #         if user is not None:
    #             if user.is_active:
    #                 login(request, user)
    #                 return HttpResponseRedirect("/dashboard")
    #
    #         return render_to_response('public/login.html', {'form': form, 'error': True},
    #                                   context_instance=RequestContext(request))
    # else:
    #     form = LoginForm()
    #
    # return render_to_response('public/login.html', {'form': form}, context_instance=RequestContext(request))





def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/')

def custom_password_change(request, **kwargs):
    template_name = "password_change_form.html"
    return password_change(request, template_name=template_name, **kwargs)

def custom_password_change_done(request, **kwargs):
    template_name='password_change_done.html'
    return password_change_done(request, template_name=template_name, **kwargs)

def custom_password_reset(request, **kwargs):
    template_name='password_reset_form.html'
    return password_reset(request, template_name=template_name, **kwargs)

def custom_password_reset_done(request, **kwargs):
    template_name='password_reset_done.html'
    return password_reset_done(request, template_name=template_name, **kwargs)

def custom_password_reset_complete(request, **kwargs):
    template_name='password_reset_complete.html'
    return password_reset_complete(request, template_name=template_name, **kwargs)

def custom_password_reset_confirm(request, **kwargs):
    template_name='password_reset_confirm.html'
    return password_reset_confirm(request, template_name=template_name, **kwargs)
