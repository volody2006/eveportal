"""Admin definition for EmailUser."""
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from users.forms import RegistrationForm, EmailUserChangeForm
from users.models import CustomUser


class CustomUserAdmin(UserAdmin):

    """EmailUser Admin model."""

    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),

    )
    add_fieldsets = ((
        None, {
            'classes': ('wide',),
            'fields': ('username','email', 'password', 'password2')
        }
    ),
    )

    # The forms to add and change user instances
    form = EmailUserChangeForm
    add_form = RegistrationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'main_char' ,'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('username', 'email', )
    ordering = ('username', 'email',)
    filter_horizontal = ('groups', 'user_permissions',)

# Register the new EmailUserAdmin
admin.site.register(CustomUser, CustomUserAdmin)
