# -*- coding: utf-8 -*-
from django.db.models import Q
from django.contrib.auth.backends import ModelBackend
from users.models import CustomUser
import datetime
import logging

logger = logging.getLogger(__name__)


class UsernameBackend(ModelBackend):
    natural_field = 'username'

    def authenticate(self, username=None, password=None, **kwargs):
        backend = self.__class__.__name__
        username = username or kwargs.get(CustomUser.USERNAME_FIELD)

        if username is None:
            logger.debug('%s: no username provided; return' % backend)
            return None

        try:
            user = CustomUser.objects.get(**{'%s__iexact' % self.natural_field: username})
            if user.check_password(password):
                logger.debug('%s: password accepted; user %s' % (backend, user.pk))
                return user
            logger.debug('%s: password check failed' % backend)
        except (ValueError, CustomUser.DoesNotExist):
            logger.debug('%s: user not found' % backend)
            return None


class EmailBackend(UsernameBackend):
    natural_field = 'email'


class CustomUserBackend(ModelBackend):
    """
    New style (django way) authentication, with salt
    """
    def authenticate(self, username=None, password=None):
        try:
            user = CustomUser.objects.get( Q(username__iexact=username) | Q(email__iexact=username) )
            if user.check_password(password):
                return user
        except CustomUser.DoesNotExist:
            return None

    def get_user(self, user_id):
        """
        Repeats a super `get_user` method except this one uses
        a users.models.User rather than django.contrib.auth.models.User
        """
        try:
            user = CustomUser.objects.get(pk=user_id)
            return user
        except CustomUser.DoesNotExist:
            return None
