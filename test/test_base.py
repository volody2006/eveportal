# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.test import TestCase, Client
from django.utils.translation import ugettext as _
from evelink.api import APIResult


class APITestCase(TestCase):
    def setUp(self):
        super(APITestCase, self).setUp()
       # self.api = mock.MagicMock(spec=evelink_api.API)

    def api_valid_full(self):
        return {'api_id':  '4641111',
                'api_key': 'bmVVdUoBtamumPM6g9AevKDGDnxblLZI98O4ZqBI5ObdVuAxJZGhLtXvP5epAAI9'}

    def api_no_valid(self):
        return {'api_id':  '123456',
                'api_key': 'lolololoxoxloloosdflkdsjfsdhfsdhfsdhsdjkhfjhskjdhkjfhkjsdhkjh'}

    def api_valid_limit(self):
        return {'api_id':  '4658157',
                'api_key': 'y6e0npUT95xdIP9K54Fr3JCJm3Jr92IhdvcAo7j9DK887iSq6rf7Y5Rq2BqxjKNK'}

    def api_valid_corp(self):
        return {'api_id':  '4653308',
                'api_key': 'U4BM2ywvumJaUakh6T9gzZVDJrjeuYMUxVC4v7wIyBk5HUEsTkPv5Zep0Ffqz75Y'}



    def get_api_info(self, api_id):
        if api_id == 3110415:
            var = {'access_mask': 1073741823,
                   'type': 'account',
                   'characters': {93959323: {'alliance': {'id': 99005103, 'name': 'Independent Union'}, 'corp': {'id': 98292612, 'name': 'Stalin Corporation'}, 'id': 93959323, 'name': 'Stalin Vozmezdie'},
                                  95367615: {'alliance': {'id': 99005103, 'name': 'Independent Union'}, 'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'}, 'id': 95367615, 'name': 'Vladimir Mackevich'},
                                  94539743: {'alliance': {'id': 99005103, 'name': 'Independent Union'}, 'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'}, 'id': 94539743, 'name': 'Margaret Baroness'}},
                   'expire_ts': None}
            return var
        elif api_id == 4512089:
            var = {'access_mask': 67108863, 'type': 'corp', 'characters': {93959323: {'alliance': {'id': 99005103, 'name': 'Independent Union'}, 'corp': {'id': 98292612, 'name': 'Stalin Corporation'}, 'id': 93959323, 'name': 'Stalin Vozmezdie'}}, 'expire_ts': None}
            return var


    def get_characters_from_api(self, api_id):
        if int(api_id) == 4641111:
            var = APIResult(result={
                93840640: {'alliance':
                               {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98292612, 'name': 'Stalin Corporation'},
                           'id': 93840640, 'name': 'Lili Cadelanne'},
                95208193: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98368686, 'name': 'Holding corporations'},
                           'id': 95208193, 'name': 'Carmen Ellecon'},
                94541861: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'},
                           'id': 94541861, 'name': 'Avrora Titay'}}, timestamp=1441874871, expires=1441874971)

            return var
        elif int(api_id) == 4653308:
            var = APIResult(result={
                93840640: {'alliance':
                               {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98292612, 'name': 'Stalin Corporation'},
                           'id': 93840640, 'name': 'Lili Cadelanne'},
                95208193: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98368686, 'name': 'Holding corporations'},
                           'id': 95208193, 'name': 'Carmen Ellecon'},
                94541861: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'},
                           'id': 94541861, 'name': 'Avrora Titay'}}, timestamp=1441874871, expires=1441874971)
            return var


    def check_api_is_valid(self, api_id):
        if int(api_id) == 4641111:
            var = {'access_mask': 1073741823, 'type': 'account', 'characters':
            {93840640:
                 {'alliance':
                      {'id': 99005103, 'name': 'Independent Union'},
                  'corp': {'id': 98292612, 'name': 'Stalin Corporation'},
                  'id': 93840640, 'name': 'Lili Cadelanne'},
             95208193:
                 {'alliance':
                      {'id': 99005103, 'name': 'Independent Union'},
                  'corp': {'id': 98368686, 'name': 'Holding corporations'},
                  'id': 95208193, 'name': 'Carmen Ellecon'},
             94541861:
                 {'alliance':
                      {'id': 99005103, 'name': 'Independent Union'},
                  'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'},
                  'id': 94541861, 'name': 'Avrora Titay'}}, 'expire_ts': None}

            return var
        elif int(api_id) == 4653308:
            var = {'access_mask': 134217727, 'type': 'corp', 'characters':
                    {93493316: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                                'corp': {'id': 98368686, 'name': 'Holding corporations'},
                                'id': 93493316, 'name': 'Olihera'}}, 'expire_ts': None}
            return var

    def get_character_from_id(self, char_id):
        if int(char_id) == 93959323:
            return {'alliance': {'timestamp': 1421130900, 'id': 99005103, 'name': 'Independent Union'},
                    'sec_status': 5.00037997816508, 'skillpoints': None,
                    'corp': {'timestamp': 1392818880, 'id': 98292612, 'name': 'Stalin Corporation'},
                    'ship': {'type_name': None, 'name': None, 'type_id': None},
                    'id': 93959323,
                    'name': 'Stalin Vozmezdie', 'bloodline': 'Civire', 'isk': None, 'race': 'Caldari', 'location': None,
                    'history': [{'corp_name': 'Stalin Corporation', 'start_ts': 1392818880, 'corp_id': 98292612},
                                {'corp_name': 'Caldari Provisions', 'start_ts': 1392817740, 'corp_id': 1000009},
                                {'corp_name': 'Guards Assault Corps', 'start_ts': 1383755700, 'corp_id': 98238722},
                                {'corp_name': 'Caldari Provisions', 'start_ts': 1383755520, 'corp_id': 1000009},
                                {'corp_name': 'Stalin Vozmezdie Corporation', 'start_ts': 1383067980, 'corp_id': 98259680},
                                {'corp_name': 'Science and Trade Institute', 'start_ts': 1383052320, 'corp_id': 1000045}]}

        elif int(char_id) == 90000002:
            return {'alliance': {'timestamp': None, 'id': None, 'name': None},
                    'sec_status': 0.0, 'skillpoints': None,
                    'corp': {'timestamp': 1288697760, 'id': 1000172, 'name': 'Pator Tech School'},
                    'ship': {'type_name': None, 'name': None, 'type_id': None},
                    'id': 90000002, 'name': 'Minmatar Citizen 90000002', 'bloodline': 'Sebiestor', 'isk': None, 'race': 'Minmatar', 'location': None,
                    'history': [{'corp_name': 'Pator Tech School', 'start_ts': 1288697760, 'corp_id': 1000172}]}
        elif char_id == 90000149:
            return {'alliance': {'timestamp': None, 'id': None, 'name': None},
                    'sec_status': 0.662744948748174, 'skillpoints': None,
                    'corp': {'timestamp': 1395437160, 'id': 1000066, 'name': 'Viziam'},
                    'ship': {'type_name': None, 'name': None, 'type_id': None},
                    'id': 90000149, 'name': 'Luxicon Lamena', 'bloodline': 'Amarr', 'isk': None, 'race': 'Amarr', 'location': None,
                    'history': [{'corp_name': 'Viziam', 'start_ts': 1395437160, 'corp_id': 1000066},
                                {'corp_name': 'Blood Warriors', 'start_ts': 1362355740, 'corp_id': 98048845},
                                {'corp_name': 'Viziam', 'start_ts': 1350752940, 'corp_id': 1000066},
                                {'corp_name': 'Chesterblade Industries', 'start_ts': 1302729600, 'corp_id': 98033610},
                                {'corp_name': 'Viziam', 'start_ts': 1302728160, 'corp_id': 1000066},
                                {'corp_name': 'nova magna varius viverra Eden', 'start_ts': 1292360520, 'corp_id': 98007060},
                                {'corp_name': 'Corpes Taurus', 'start_ts': 1289846940, 'corp_id': 2046206334},
                                {'corp_name': 'The Great European Engineers', 'start_ts': 1289072220, 'corp_id': 1424907798},
                                {'corp_name': 'Imperial Academy', 'start_ts': 1288719960, 'corp_id': 1000166}]}


        else:
            return False



    def evelink_character_info_from_id(self, character_id):
        if int(character_id) == 93840640:

            val = APIResult(result={'alliance':
                                         {'timestamp': 1421130900, 'id': 99005103, 'name': 'Independent Union'},
                                     'sec_status': 5.00640606085186, 'skillpoints': None, 'corp':
                                         {'timestamp': 1429953480, 'id': 98292612, 'name': 'Stalin Corporation'},
                                     'ship': {'type_name': None, 'name': None, 'type_id': None},
                                     'id': 93840640, 'name': 'Lili Cadelanne', 'bloodline': 'Gallente',
                                     'isk': None,
                                     'race': 'Gallente',
                                     'location': None,
                                     'history': [
                                         {'corp_name': 'Stalin Corporation', 'start_ts': 1429953480, 'corp_id': 98292612},
                                         {'corp_name': 'The Scope', 'start_ts': 1429953480, 'corp_id': 1000107},
                                         {'corp_name': 'Holding corporations', 'start_ts': 1425212940, 'corp_id': 98368686},
                                         {'corp_name': 'The Scope', 'start_ts': 1425212940, 'corp_id': 1000107},
                                         {'corp_name': 'Stalin Corporation', 'start_ts': 1417879800, 'corp_id': 98292612},
                                         {'corp_name': 'The Scope', 'start_ts': 1417879740, 'corp_id': 1000107},
                                         {'corp_name': 'DIE-HARD PATRIOTS', 'start_ts': 1410921000, 'corp_id': 98075476},
                                         {'corp_name': 'The Scope', 'start_ts': 1410797880, 'corp_id': 1000107},
                                         {'corp_name': 'Shadows of the Day', 'start_ts': 1395467460, 'corp_id': 1662639041},
                                         {'corp_name': 'The Scope', 'start_ts': 1395415620, 'corp_id': 1000107},
                                         {'corp_name': 'Stalin Corporation', 'start_ts': 1392819000, 'corp_id': 98292612},
                                         {'corp_name': 'The Scope', 'start_ts': 1392817440, 'corp_id': 1000107},
                                         {'corp_name': 'Guards Assault Corps', 'start_ts': 1385562480, 'corp_id': 98238722},
                                         {'corp_name': 'The Scope', 'start_ts': 1385515500, 'corp_id': 1000107},
                                         {'corp_name': 'hoooot', 'start_ts': 1381412280, 'corp_id': 98254263},
                                         {'corp_name': 'Center for Advanced Studies', 'start_ts': 1379707860, 'corp_id': 1000169}]},
                             timestamp=1441826189,
                             expires=1441829162)
            return val
        elif int(character_id)  == 93959323:
            val =  APIResult(result={'alliance':
                                         {'timestamp': 1421130900, 'id': 99005103, 'name': 'Independent Union'},
                                     'sec_status': 5.00640606085186, 'skillpoints': None, 'corp':
                                         {'timestamp': 1429953480, 'id': 98292612, 'name': 'Stalin Corporation'},
                                     'ship': {'type_name': None, 'name': None, 'type_id': None},
                                     'id': 93959323, 'name': 'Stalin Vozmezdie', 'bloodline': 'Gallente',

                                     'isk': None,
                                     'race': 'Gallente',
                                     'location': None,
                                     'history': [{'corp_name': 'Stalin Corporation', 'start_ts': 1429953480, 'corp_id': 98292612}]},
                             timestamp=1441826189,
                             expires=1441829162)
            return val

    def get_corporation_information(self, corp_id):
        if int(corp_id)  == 98292612:
            var = {'alliance':
                                    {'id': 99005103, 'name': 'Independent Union'},
                                'description': u'',
                                'ceo': {'id': 93959323, 'name': 'Stalin Vozmezdie'},
                                'hq': {'id': 60014740, 'name': 'Balle VII - Moon 17 - Center for Advanced Studies School'},
                                'members': {'current': 46},
                                'logo': {'shapes': [{'color': 680, 'id': 565}, {'color': 0, 'id': 415}, {'color': 0, 'id': 415}],
                                         'graphic_id': 0},
                                'ticker': 'SVC-S', 'id': 98292612, 'tax_percent': 5.0, 'name': 'Stalin Corporation',
                                'url': 'https://forums.eveonline.com/default.aspx?g=posts&t=411958', 'shares': 1000}

        elif int(corp_id) == 98395774:
            var = {'alliance':
                       {'id': None, 'name': None},
                   'description': 'Enter a description of your corporation here.',
                   'ceo':
                       {'id': 95578574, 'name': 'Lenka 7B'},
                   'hq':
                       {'id': 60014056, 'name': 'X-7OMU II - Moon 7 - The Sanctuary School'},
                   'members':
                       {'current': 114},
                   'logo': {'shapes': [{'color': 0, 'id': 513}, {'color': 0, 'id': 415}, {'color': 0, 'id': 415}],
                            'graphic_id': 0},
                   'ticker': 'RSPAC',
                   'id': 98395774,
                   'tax_percent': 0.1,
                   'name': 'Rspace of banderlogs',
                   'url': 'http://',
                   'shares': 1000}
        elif int(corp_id) == 20000001:
            var = {'alliance':
                       {'id': 99005103, 'name': 'Independent Union'},
                   'description': 'Enter a description of your corporation here.',
                   'ceo':
                       {'id': 95578574, 'name': 'Lenka 7B'},
                   'hq':
                       {'id': 60014056, 'name': 'X-7OMU II - Moon 7 - The Sanctuary School'},
                   'members':
                       {'current': 114},
                   'logo': {'shapes': [{'color': 0, 'id': 513}, {'color': 0, 'id': 415}, {'color': 0, 'id': 415}],
                            'graphic_id': 0},
                   'ticker': 'RSPAC',
                   'id': 20000001,
                   'tax_percent': 0.1,
                   'name': 'Rspace of banderlogs',
                   'url': 'http://',
                   'shares': 1000}
        elif int(corp_id) == 98368686:
            var = {'alliance':
                       {'id': 99005103, 'name': 'Independent Union'},
                   'description': u'<font size="12" color="#bfffffff">\u0414\u0438\u043f\u043b\u043e\u043c\u0430\u0442\u044b \u0410\u043b\u044c\u044f\u043d\u0441\u0430 <br><br>RU/EN - </font><font size="12" color="#ffffa600"><a href="showinfo:1377//93074871">Nemisid Novus</a></font><font size="12" color="#bfffffff"> <br><br></font><font size="12" color="#fff7931e"><a href="joinChannel:-69999085//None//None">IN-UN Dip</a></font><font size="12" color="#bfffffff"> - The diplomatic channel / \u0414\u0438\u043f\u043b\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0438\u0439 \u0447\u0430\u0442<br></font><font size="12" color="#fff7931e"><a href="joinChannel:-70158991//None//None">IN-UN Public</a></font><font size="12" color="#bfffffff"> - Public channel./ \u041f\u0443\u0431\u043b\u0438\u0447\u043d\u044b\u0439 \u043a\u0430\u043d\u0430\u043b.<br>     <br>The President of the Alliance  </font><font size="12" color="#ffffa600"><a href="showinfo:1378//93968404">Yura Berestish</a></font><font size="12" color="#bfffffff"> <br>Vice President of the Alliance  </font><font size="12" color="#ffffa600"><a href="showinfo:1375//93959323">Stalin Vozmezdie</a></font><font size="12" color="#bfffffff"> </font>',
                   'ceo':
                       {'id': 95208193, 'name': 'Carmen Ellecon'},
                   'hq':
                       {'id': 60014740, 'name': 'Balle VII - Moon 17 - Center for Advanced Studies School'},
                   'members':
                       {'current': 4},
                   'logo': {'shapes': [{'color': 0, 'id': 436}, {'color': 0, 'id': 415}, {'color': 0, 'id': 415}],
                            'graphic_id': 0},
                   'ticker': 'GHBDT',
                   'id': 98368686,
                   'tax_percent': 0.0,
                   'name': 'Holding corporations',
                   'url': 'http://in-un.ru',
                   'shares': 1000}


        else:
            var = False
        return var

    def get_alliance_information(self, alliance_id):
        if int(alliance_id) == 99005103:
            val = {'executor_id': 98368686,
                   'name': 'Independent Union',
                   'member_count': 100,
                   'timestamp': 1421041904,
                   'member_corps':
                       {98292612: {'timestamp': 1421130900, 'id': 98292612},
                        98391397: {'timestamp': 1429277700, 'id': 98391397},
                        98384042: {'timestamp': 1434356640, 'id': 98384042},
                        98368686: {'timestamp': 1421041920, 'id': 98368686},
                        98296915: {'timestamp': 1421182320, 'id': 98296915},
                        98408412: {'timestamp': 1437241080, 'id': 98408412},
                        98409373: {'timestamp': 1437760560, 'id': 98409373},
                        98364403: {'timestamp': 1422197700, 'id': 98364403}},
                   'ticker': 'IN-UN',
                   'id': 99005103}
            return val

        if int(alliance_id) == 10000001:
            val = {'executor_id': 98368686,
                   'name': 'Independent Union',
                   'member_count': 111,
                   'timestamp': 1421041904,
                   'member_corps':
                       {98292612: {'timestamp': 1421130900, 'id': 98292612},
                        98391397: {'timestamp': 1429277700, 'id': 98391397},
                        98384042: {'timestamp': 1434356640, 'id': 98384042},
                        98368686: {'timestamp': 1421041920, 'id': 98368686},
                        98296915: {'timestamp': 1421182320, 'id': 98296915},
                        98408412: {'timestamp': 1437241080, 'id': 98408412},
                        98409373: {'timestamp': 1437760560, 'id': 98409373},
                        98364403: {'timestamp': 1422197700, 'id': 98364403}},
                   'ticker': 'IN-UN',
                   'id': 99005103}
            return val

    def get_corporation_ticker_from_id(self, corp_id):
        if int(corp_id) == 98292612:
            value = 'SVC-S'
        elif int(corp_id) == 98368686:
            value = 'GHBDT'
        elif int(corp_id) == 98384042:
            value = 'JMIAK'
        return value

    def check_api_is_type_account(self, api_id):
        if int(api_id) == 4641111:
            value = True
        else:
            value = False
        return value





