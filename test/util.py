# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _


from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client
from evelink.api import APIResult
from eveonline.models import Alliance
from eveportal import settings
from mixer.backend.django import mixer
from users.models import CustomUser


class APITestCase(TestCase):
    def setUp(self):
        super(APITestCase, self).setUp()
       # self.api = mock.MagicMock(spec=evelink_api.API)

    def api_valid_full(self):
        return {'api_id':  '4641111',
                'api_key': 'bmVVdUoBtamumPM6g9AevKDGDnxblLZI98O4ZqBI5ObdVuAxJZGhLtXvP5epAAI9'}

    def api_no_valid(self):
        return {'api_id':  '123456',
                'api_key': 'lolololoxoxloloosdflkdsjfsdhfsdhfsdhsdjkhfjhskjdhkjfhkjsdhkjh'}

    def api_valid_limit(self):
        return {'api_id':  '4658157',
                'api_key': 'y6e0npUT95xdIP9K54Fr3JCJm3Jr92IhdvcAo7j9DK887iSq6rf7Y5Rq2BqxjKNK'}

    def api_valid_corp(self):
        return {'api_id':  '4653308',
                'api_key': 'U4BM2ywvumJaUakh6T9gzZVDJrjeuYMUxVC4v7wIyBk5HUEsTkPv5Zep0Ffqz75Y'}



    def get_api_info(self, api_id):
         if int(api_id) == 4641111:
            var = {'access_mask': 1073741823, 'type': 'account', 'characters':
                {93840640: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                            'corp': {'id': 98292612, 'name': 'Stalin Corporation'},
                            'id': 93840640, 'name': 'Lili Cadelanne'},
                 95208193: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                            'corp': {'id': 98368686, 'name': 'Holding corporations'},
                            'id': 95208193, 'name': 'Carmen Ellecon'},
                 94541861: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                            'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'},
                            'id': 94541861, 'name': 'Avrora Titay'}
                 }, 'expire_ts': None}

            return var
         elif int(api_id) == 4653308:
             var = {'access_mask': 134217727, 'type': 'corp', 'characters':
                {93493316: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                            'corp': {'id': 98368686, 'name': 'Holding corporations'},
                            'id': 93493316, 'name': 'Olihera'}},
                                             'expire_ts': None}
             return var



    def get_characters_from_api(self, api_id):
        if int(api_id) == 4641111:
            var = {
                93840640: {'alliance':
                               {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98292612, 'name': 'Stalin Corporation'},
                           'id': 93840640, 'name': 'Lili Cadelanne'},
                95208193: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98368686, 'name': 'Holding corporations'},
                           'id': 95208193, 'name': 'Carmen Ellecon'},
                94541861: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'},
                           'id': 94541861, 'name': 'Avrora Titay'}}

            return var
        elif int(api_id) == 4653308:
            var = {
                93840640: {'alliance':
                               {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98292612, 'name': 'Stalin Corporation'},
                           'id': 93840640, 'name': 'Lili Cadelanne'},
                95208193: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98368686, 'name': 'Holding corporations'},
                           'id': 95208193, 'name': 'Carmen Ellecon'},
                94541861: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                           'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'},
                           'id': 94541861, 'name': 'Avrora Titay'}}
            return var


    def check_api_is_valid(self, api_id):
        if int(api_id) == 4641111:
            var = {'access_mask': 1073741823, 'type': 'account', 'characters':
            {93840640:
                 {'alliance':
                      {'id': 99005103, 'name': 'Independent Union'},
                  'corp': {'id': 98292612, 'name': 'Stalin Corporation'},
                  'id': 93840640, 'name': 'Lili Cadelanne'},
             95208193:
                 {'alliance':
                      {'id': 99005103, 'name': 'Independent Union'},
                  'corp': {'id': 98368686, 'name': 'Holding corporations'},
                  'id': 95208193, 'name': 'Carmen Ellecon'},
             94541861:
                 {'alliance':
                      {'id': 99005103, 'name': 'Independent Union'},
                  'corp': {'id': 98384042, 'name': 'Lucky Jmiaki Corporaki'},
                  'id': 94541861, 'name': 'Avrora Titay'}}, 'expire_ts': None}

            return var
        elif int(api_id) == 4653308:
            var = {'access_mask': 134217727, 'type': 'corp', 'characters':
                    {93493316: {'alliance': {'id': 99005103, 'name': 'Independent Union'},
                                'corp': {'id': 98368686, 'name': 'Holding corporations'},
                                'id': 93493316, 'name': 'Olihera'}}, 'expire_ts': None}
            return var

    def evelink_character_info_from_id(self, character_id):
        if int(character_id) == 93840640:

            val = APIResult(result={'alliance':
                                         {'timestamp': 1421130900, 'id': 99005103, 'name': 'Independent Union'},
                                     'sec_status': 5.00640606085186, 'skillpoints': None, 'corp':
                                         {'timestamp': 1429953480, 'id': 98292612, 'name': 'Stalin Corporation'},
                                     'ship': {'type_name': None, 'name': None, 'type_id': None},
                                     'id': 93840640, 'name': 'Lili Cadelanne', 'bloodline': 'Gallente',
                                     'isk': None,
                                     'race': 'Gallente',
                                     'location': None,
                                     'history': [
                                         {'corp_name': 'Stalin Corporation', 'start_ts': 1429953480, 'corp_id': 98292612},
                                         {'corp_name': 'The Scope', 'start_ts': 1429953480, 'corp_id': 1000107},
                                         {'corp_name': 'Holding corporations', 'start_ts': 1425212940, 'corp_id': 98368686},
                                         {'corp_name': 'The Scope', 'start_ts': 1425212940, 'corp_id': 1000107},
                                         {'corp_name': 'Stalin Corporation', 'start_ts': 1417879800, 'corp_id': 98292612},
                                         {'corp_name': 'The Scope', 'start_ts': 1417879740, 'corp_id': 1000107},
                                         {'corp_name': 'DIE-HARD PATRIOTS', 'start_ts': 1410921000, 'corp_id': 98075476},
                                         {'corp_name': 'The Scope', 'start_ts': 1410797880, 'corp_id': 1000107},
                                         {'corp_name': 'Shadows of the Day', 'start_ts': 1395467460, 'corp_id': 1662639041},
                                         {'corp_name': 'The Scope', 'start_ts': 1395415620, 'corp_id': 1000107},
                                         {'corp_name': 'Stalin Corporation', 'start_ts': 1392819000, 'corp_id': 98292612},
                                         {'corp_name': 'The Scope', 'start_ts': 1392817440, 'corp_id': 1000107},
                                         {'corp_name': 'Guards Assault Corps', 'start_ts': 1385562480, 'corp_id': 98238722},
                                         {'corp_name': 'The Scope', 'start_ts': 1385515500, 'corp_id': 1000107},
                                         {'corp_name': 'hoooot', 'start_ts': 1381412280, 'corp_id': 98254263},
                                         {'corp_name': 'Center for Advanced Studies', 'start_ts': 1379707860, 'corp_id': 1000169}]},
                             timestamp=1441826189,
                             expires=1441829162)
            return val
        elif int(character_id)  == 93959323:
            val =  APIResult(result={'alliance':
                                         {'timestamp': 1421130900, 'id': 99005103, 'name': 'Independent Union'},
                                     'sec_status': 5.00640606085186, 'skillpoints': None, 'corp':
                                         {'timestamp': 1429953480, 'id': 98292612, 'name': 'Stalin Corporation'},
                                     'ship': {'type_name': None, 'name': None, 'type_id': None},
                                     'id': 93959323, 'name': 'Stalin Vozmezdie', 'bloodline': 'Gallente',

                                     'isk': None,
                                     'race': 'Gallente',
                                     'location': None,
                                     'history': [{'corp_name': 'Stalin Corporation', 'start_ts': 1429953480, 'corp_id': 98292612}]},
                             timestamp=1441826189,
                             expires=1441829162)
            return val

    def get_corporation_information(self, corp_id):
        if int(corp_id)  == 98292612:
            var = {'alliance':
                                    {'id': 99005103, 'name': 'Independent Union'},
                                'description': u'',
                                'ceo': {'id': 93959323, 'name': 'Stalin Vozmezdie'},
                                'hq': {'id': 60014740, 'name': 'Balle VII - Moon 17 - Center for Advanced Studies School'},
                                'members': {'current': 46},
                                'logo': {'shapes': [{'color': 680, 'id': 565}, {'color': 0, 'id': 415}, {'color': 0, 'id': 415}],
                                         'graphic_id': 0},
                                'ticker': 'SVC-S', 'id': 98292612, 'tax_percent': 5.0, 'name': 'Stalin Corporation',
                                'url': 'https://forums.eveonline.com/default.aspx?g=posts&t=411958', 'shares': 1000}
            return var

    def get_alliance_information(self, alliance_id):
        if int(alliance_id) == 99005103:
            val = {'executor_id': 98368686,
                   'name': 'Independent Union',
                   'member_count': 100,
                   'timestamp': 1421041904,
                   'member_corps':
                       {98292612: {'timestamp': 1421130900, 'id': 98292612},
                        98391397: {'timestamp': 1429277700, 'id': 98391397},
                        98384042: {'timestamp': 1434356640, 'id': 98384042},
                        98368686: {'timestamp': 1421041920, 'id': 98368686},
                        98296915: {'timestamp': 1421182320, 'id': 98296915},
                        98408412: {'timestamp': 1437241080, 'id': 98408412},
                        98409373: {'timestamp': 1437760560, 'id': 98409373},
                        98364403: {'timestamp': 1422197700, 'id': 98364403}},
                   'ticker': 'IN-UN',
                   'id': 99005103}
            return val

    def get_corporation_ticker_from_id(self, corp_id):
        if int(corp_id) == 98292612:
            value = 'SVC-S'
        elif int(corp_id) == 98368686:
            value = 'GHBDT'
        elif int(corp_id) == 98384042:
            value = 'JMIAK'
        return value

    def check_api_is_type_account(self, api_id):
        if int(api_id) == 4641111:
            value = True
        else:
            value = False
        return value



class TestBase(APITestCase):
   # fixtures = ['fixtures/eveonline_fix.json']

    def setUp(self):
        super(TestBase, self).setUp()
        self.user = CustomUser.objects.create_user(username='user', password='user', email='user@user.com')
        self.moderator = CustomUser.objects.create_superuser(username='moder', password='moder', email='m@m.ru')
        self.bad_user = CustomUser.objects.create_user(username='rediska', email='rediska@dofigaumniy.ru', password='rediska')
        self.alliance = mixer.blend(Alliance, alliance_id = settings.ALLIANCE_ID, alliance_name = 'Independent Union')

        self.client = Client()
        assert self.client.login(username='user', password='user')

    def login_user(self):
        self.client.logout()
        self.client.login(username='user', password='user')
        self.url = reverse('add_api_key')
        data = self.api_valid_full()
        data['is_blue'] = False
        page = self.client.post(self.url, data)

    def became_anonymous(self):
        """
        Выполняет действия от имени анонимного пользователя
        """
        self.client.logout()

    # def login_as_moderator(self):
    #     """
    #     Меняет текущего пользователя на модератора
    #     """
    #     self.client.logout()
    #     self.client.login(username='moder', password='moder')

    def login_as_bad_user(self):
        """
        Меняет текущего пользователя на "не владельца объявления"
        """
        self.client.logout()
        self.client.login(username='rediska', password='rediska')



