# -*- coding: utf-8 -*-


from __future__ import absolute_import, division, print_function, unicode_literals
from django.utils.translation import ugettext as _

from haystack.views import SearchView

from search.views import MySearchView

try:
    from django.conf.urls import patterns, url
except ImportError:
    from django.conf.urls.defaults import patterns, url

urlpatterns = [

        # 'search.views',
        # url(r'^$', SearchView(), name='portal_search'),
    url(r'^$', MySearchView.as_view(), name='portal_search'),


]
