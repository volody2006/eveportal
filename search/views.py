# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _
from haystack.generic_views import SearchView
from eveonline.managers import EveManager
from haystack.models import SearchResult


class MySearchView(SearchView):
    """My custom search view."""


    def get_queryset(self):
        queryset = super(MySearchView, self).get_queryset()
        # further filter queryset based on some set of criteria

        return queryset.all() #filter(update__lte=datetime.now())

    def get_context_data(self, *args, **kwargs):
        # SearchResult
        context = super(MySearchView, self).get_context_data(*args, **kwargs)
        # do something
        # try:
        #     print (1, context['object_list'][0].__dict__)
        # except:
        #     pass

        if context['object_list'] == []:
            e = EveManager()
            name = context['query']
            obj_or_false = e.get_char_or_corp_is_name(name)
            if obj_or_false:
                # print(obj_or_false)

                s = SearchResult(app_label=obj_or_false._meta.app_label,
                                 model_name=obj_or_false._meta.model_name,
                                 pk=obj_or_false.pk,
                                 score=1,
                                 text=obj_or_false)
                # s.object = obj_or_false
                context['object_list'] = [s,]
                # print (2, context['object_list'][0])

        return context
