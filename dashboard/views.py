# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.template import RequestContext
from django.views.generic import TemplateView


@login_required
def dashboard_view(request):
    render_items = {}
    return render_to_response('dashboard/portal/dashboard.html', render_items, context_instance=RequestContext(request))


@login_required
def help_view(request):
    return render_to_response('dashboard/portal/help.html', None, context_instance=RequestContext(request))



# def view_404(request):
#     return render_to_response('public/index.html', None, context_instance=RequestContext(request))



class View_404(TemplateView):
    template_name = "template_page/page.html"